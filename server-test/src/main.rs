#![warn(clippy::all)]

//#[macro_use]
//extern crate lazy_static;
use std::cell::Cell;
use std::sync::atomic::{AtomicU32, Ordering};
use mio::net::TcpListener;
use mio::{Event, Evented, Events, Poll, PollOpt, Ready, Registration, SetReadiness, Token};
use std::net::{SocketAddr, ToSocketAddrs};
use std::sync::mpsc::{channel, Receiver, RecvError, Sender};

use std::str::FromStr;
use tntransition::Model;
use tntransition::{JsonQuery, QueryHandled};
use model::TNModel;
//use tnsqlite3::{Connection, OpenFlags, self};
use arena::Arena;

use tnserver::IOC;
use tnserver::{event_loop, DBConnection, Dispatcher, ListenConnection, ThreadPool, Transport};

pub(crate) const CONNCAPACITY: usize = 1000;

type QueryHandler<T> = Box<dyn QueryHandled<T> + Send + 'static>;

static qh_count: AtomicU32 = AtomicU32::new(0);

struct MockQueryHandler {
    id: u32,
    call_count: usize
}

impl MockQueryHandler {
 fn new() -> Self {
    let s = MockQueryHandler{id: qh_count.load(Ordering::SeqCst), call_count: 0 };
    qh_count.fetch_add(1, Ordering::SeqCst);
    s
}
}

impl<T> QueryHandled<T> for MockQueryHandler
where
    T: AsRef<[u8]> + 'static + Send,
{
    fn request(&mut self, chunks: Option<Vec<T>>) -> Result<Option<Vec<u8>>, tntransition::Error> {
        dbg!("request");
        self.call_count+=1;
        Ok(None)
    }
    fn try_clone(&self) -> Result<QueryHandler<T>, tntransition::Error> {
        Ok(Box::new(MockQueryHandler::new()))
    }
}
impl Drop for MockQueryHandler {
    fn drop(&mut self) {
        println!("Drop QueryHandler {}, was called {}", self.id, self.call_count)
}
}


fn main() {
    let addr = SocketAddr::from_str("127.0.0.1:8088").unwrap();

    let (registration, set_readiness) = Registration::new2();
    let handler_db_conn = Box::new(DBConnection::new(registration, set_readiness.clone()));

    let action_gen_old = move || {
        let st = set_readiness.clone();
        move |task: Box<dyn FnOnce() -> Transport + Send + 'static>,
              sender: &mut Sender<Transport>| {
            let mut tr = task();
            let res = tr.qh.request(tr.chunks.take());
            tr.message = res;
            tr.message = Ok(Some("Hello".into()));
            sender.send(tr).unwrap();
            st.set_readiness(Ready::readable()).unwrap();
        }
    };

    let threadpool = ThreadPool::new_ext(2, action_gen_old);
    let qh =  Box::new(MockQueryHandler::new());


    let arena = Arena::new(IOC::default(), 5120);
    let listen_conn = Box::new(ListenConnection::new(&addr, qh).unwrap());
    let  poll = Poll::new().unwrap();
    let mut events = Events::with_capacity(CONNCAPACITY);
    let mut dispatcher = Dispatcher::new(
        listen_conn,
        handler_db_conn,
        &poll,
        arena,
        threadpool,
        CONNCAPACITY,
    )
    .unwrap();

    event_loop(&poll, &mut events, &mut dispatcher).unwrap();


}
