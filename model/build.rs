use std::env;
use std::fs;
use std::path::Path;

//fn text_triggers(num: usize) -> String {
//    let r = (0..num).map(|i| text_trigger(i)).collect::<Vec<_>>();
//    r.join("")
//}

//fn text_trigger(count: usize) -> String {
//    let name = format!("text_col_{}", count.to_string());
//    let col = format!("column_{}", count.to_string());
//    let table = "text_table";
//    let column_num = count.to_string();

//    format!(
//        r#"
//    (
//        "{name}",
//    "CREATE TRIGGER {name} BEFORE UPDATE OF {col} ON text_table BEGIN
//    INSERT INTO log(task_id, timestamp, person_id, column, data)
//    SELECT tt.task_id, cdatetime(), getowner(), ec.name, tt.{col}
//    FROM (SELECT task_id, {col} FROM text_table WHERE task_id = OLD.task_id) AS tt,
//        (SELECT name FROM ecolumn WHERE id = {column_num}) AS ec;
//    END;"
//    ),"#,
//        name = name,
//        col = col,
//        column_num = column_num,
//    )
//}

const STATIC_FIELDS: i32 = 9;

fn trigger(col: &str, select: &str, colname: &str) -> String {
    let name = format!("log_{}", col,);
    let table = "task";
    format!(
        r#"
    (
        "{name}",
    "CREATE TRIGGER {name} BEFORE UPDATE OF {col} ON {table} BEGIN
    INSERT INTO log(task_id, timestamp, person_id, column, data)
    {select};
    END;"
    ),"#,
        name = name,
        col = col,
        table = table,
        select = select
    )
}

fn trigger_person(col: &str, colname: &str) -> String {
    let select = format!(
        "SELECT task.id, cdatetime(), getowner(), '{colname}',  p.lname || ' ' || p.fname || ' ' || p.mname
         FROM task
         INNER JOIN person AS p ON p.id = task.{col}
         WHERE task.id = OLD.id",
        col = col,
        colname = colname
    );
    trigger(col, &select, colname)
}

fn trigger_otm(col: &str, foreign_table: &str, foreign_col: &str, colname: &str) -> String {
    let select = format!(
        "SELECT task.id, cdatetime(), getowner(), '{colname}',  {foreign_table}.{foreign_col}
         FROM task
         INNER JOIN {foreign_table}  ON {foreign_table}.id = task.{col}
         WHERE task.id = OLD.id",
        col = col,
        colname = colname,
        foreign_table = foreign_table,
        foreign_col = foreign_col
    );
    trigger(col, &select, colname)
}

fn trigger_datetime(col: &str, colname: &str) -> String {
    let select = format!(
        "VALUES( OLD.id, cdatetime(), getowner(), '{colname}', fmtdatetime(OLD.{col}) )",
        col = col,
        colname = colname,
    );
    trigger(col, &select, colname)
}

fn trigger_direct(col: &str, colname: &str) -> String {
    let select = format!(
        "VALUES( OLD.id, cdatetime(), getowner(), '{colname}', OLD.{col} )",
        col = col,
        colname = colname,
    );
    trigger(col, &select, colname)
}

fn main() {
    let out_dir = env::var_os("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("triggers.rs");

    let tr = r#"pub(crate) const CREATE_TRIGGERS_STMT: &[(&str, &str)] = &[  "#.to_string()
        + &trigger_person("owner_id", "Owner")
        + &trigger_person("performer_id", "Performer")
        + &trigger_otm("status_id", "status", "name", "Status")
        + &trigger_datetime("start_time", "Start time")
        + &trigger_datetime("deadline", "Deadline")
        + &trigger_direct("pmask_id", "Priority")
        + "];";

//    let text_tr = r#"pub(crate) const CREATE_TEXT_TRIGGERS_STMT: &[(&str, &str)] = &[  "#
//        .to_string()
//        + &text_triggers(16)
//        + "];";

    let task_guard_stmt = format!(
     r#"pub const TASK_GUARD_STMT: &str = "WITH RECURSIVE
    cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
    UNION SELECT gr_1 FROM  person WHERE id=?
    UNION SELECT gr_2 FROM  person WHERE id=?
    UNION SELECT group_.id FROM cgroups, group_
    WHERE group_.parent=cgroups.id )
    SELECT IFNULL(bin_or_vec(perm, numtable.num + {static_filed_num}),
        zeroblob((SELECT COUNT(*) AS num  FROM ecolumn) + {static_filed_num}) )
    FROM perm_group AS pg
    INNER JOIN task ON pg.pmask_id = task.pmask_id,
    (SELECT COUNT(*) AS num  FROM ecolumn) AS numtable
    WHERE pg.group_id in cgroups
    AND task.id = ?";"#, static_filed_num=STATIC_FIELDS.to_string());

    let static_fielfs = r#"pub  const STATIC_FIELDS: i32 = 9;"#;

    let expr = format!(
        "{}
        {}
        {}",

        &tr, &task_guard_stmt, &static_fielfs
    );

    fs::write(&dest_path, expr).unwrap();
    println!("cargo:rerun-if-changed=bild.rs");
}
