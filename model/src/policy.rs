#![warn(clippy::all)]

use crate::abs::{Context, PersonPermission, PermLevel,  Error};
//use crate::sqlite3_aux::CONNECTION_MAP;
use crate::stypes::RowId;
use crate::{Method, Payload, TableKind};
use tnsqlite3::Connection;
use std::fmt;

pub type AFilter<FK> = Box<dyn Filter<Kind=FK> + Send + 'static>;

#[derive(Clone, Copy, Debug)]
pub enum FilterKind {
    ModeSelection = 0,
    Init,
    Unregister,
    User,
}

impl Default for FilterKind {
    fn default() -> Self {  FilterKind::ModeSelection }

}

pub trait Filter {
    type Kind;
    fn preprocess(&mut self, context: &mut Context, m: &Method) -> bool;
    fn postprocess(
        &mut self,
        context: &mut Context,
        reply: &Payload,
        conn: &mut Connection,
    ) -> Self::Kind;
}

#[derive(Default)]
struct ModeSelectionAF;

impl Filter for ModeSelectionAF {
    type Kind = FilterKind;
    fn preprocess(&mut self, context: &mut Context, m: &Method) -> bool {
        match m {
            Method::admin_count {  .. }  =>  true,
            _ => false,
        }
    }
    fn postprocess(
        &mut self,
        context: &mut Context,
        reply: &Payload,
        conn: &mut Connection,
    ) -> FilterKind {
        match reply {
            Payload::Id(c) => if c.0 == 0 {FilterKind::Init} else { FilterKind::Unregister },
            _ => FilterKind::ModeSelection,
        }
    }
}


#[derive(Default)]
struct InitDB {
    method: Option<Method>,
}

impl Filter for InitDB {
    type Kind = FilterKind;
    fn preprocess(&mut self, context: &mut Context, m: &Method) -> bool {
        self.method = Some(m.clone());
        match m {
            Method::add_person { params, .. }
                if  PersonPermission::from_i64( (params.5).0 ).level(PermLevel::Administration)    =>  true,
            Method::add_group {..} => true,
            Method::set_password {..} => true,
            Method::column_variants {params, ..} if params.0 == TableKind::Person => true,
            _ => false,
        }
    }
    fn postprocess(
        &mut self,
        context: &mut Context,
        reply: &Payload,
        conn: &mut Connection,
    ) -> FilterKind {
        if let Some(m) = self.method.take() {
            match m {
                Method::set_password { .. } => FilterKind::Unregister,
                _ => FilterKind::Init,
            }
        } else {
            FilterKind::Init
        }
    }
}

struct UnregisterAF;

impl Filter for UnregisterAF {
    type Kind = FilterKind;
    fn preprocess(&mut self, context: &mut Context, m: &Method) -> bool {
        match m {
            Method::authenticate { params, .. } => true,
            _ => false,
        }
    }

    fn postprocess(
        &mut self,
        context: &mut Context,
        reply: &Payload,
        conn: &mut Connection,
    ) -> FilterKind {
        match reply {
            Payload::Permission((id, perm)) => {
//                if let Some(person_id) = CONNECTION_MAP
//                    .lock()
//                    .expect("PoisonError mutex CONNECTION_MAP")
//                    .get_mut(&conn.handler())
//                {
//                    *person_id = context.person_id;
//                }
                context.person_id = id.0;
                context.perm = PersonPermission::from_i64(perm.0);
                dbg!(context);

                FilterKind::User
            }
            _ => FilterKind::Unregister,
        }
    }
}





struct UserAF;

impl Filter for UserAF {
    type Kind = FilterKind;
    fn preprocess(&mut self, context: &mut Context, m: &Method) -> bool {
        use Method::*;
        use TableKind::*;
        dbg!(context.perm.level(PermLevel::Ordinary) );
        let lev_1 = if context.perm.level(PermLevel::Ordinary) {
            match m {
                find {..
                }
                | fetch_data {..
                }
                | header {..
                }
                | column_variants {..
                }
               | column_type {.. }  => true,

                | update_str {
                    params: (tk, ..), ..
                }
                | update_int {
                    params: (tk, ..), ..
                }
                 if (*tk == Task) => true,
                add_task { .. } | prio_for_executor { .. } | update_prio { .. } => true,
                _ => false,
            }
        } else {
            false
        };
        if lev_1 {return true}
         dbg!(context.perm.level(PermLevel::PermMaskCreate) );
        let lev_2 = if context.perm.level(PermLevel::PermMaskCreate) {
            match m {
                 update_str {
                    params: (tk, ..), ..
                }
                | update_int {
                    params: (tk, ..), ..
                }
                  if (*tk == PermMask || *tk == PermGroup) => true,
                  add_perm_mask{..} | add_perm_group{..} => true,

                _ => false,
            }
        } else {
            false
        };
            dbg!(lev_2 );
         if lev_2 {return true}

         if context.perm.level(PermLevel::Administration) {
            match m {
                authenticate { .. }
                | admin_count { .. } => false,
                _ => true,
            }
        } else {
            false
        }

    }
    fn postprocess(
        &mut self,
        context: &mut Context,
        reply: &Payload,
        conn: &mut Connection,
    ) -> FilterKind {
        FilterKind::User
    }
}







struct PermissiveFilter;

pub struct PermissiveFilters(AFilter<FKDummy>);

impl Default for PermissiveFilters {
    fn default() -> Self {
        PermissiveFilters( Box::new(PermissiveFilter) )
    }
}

#[derive(Default, Clone, Copy, Debug)]
pub struct FKDummy;

impl Filter for PermissiveFilter {
type Kind = FKDummy;
    fn preprocess(&mut self, context: &mut Context, m: &Method) -> bool {
        true
    }
    fn postprocess(
        &mut self,
        context: &mut Context,
        reply: &Payload,
        conn: &mut Connection,
    ) -> Self::Kind {
        FKDummy
    }
}


impl FilterKit for PermissiveFilters {
    type Kind = FKDummy;
    fn get(&mut self, fk: Self::Kind) -> &mut AFilter<Self::Kind> {
        &mut self.0
    }
}



pub struct Filters([AFilter<FilterKind>; 4]);

impl Default for Filters {
    fn default() -> Self {
        Filters([
            Box::new(ModeSelectionAF),
            Box::new(InitDB::default()),
            Box::new(UnregisterAF),
            Box::new(UserAF),
        ])
    }
}




pub trait FilterKit: Default {
    type Kind: Default + Copy + fmt::Debug;
    fn get(&mut self, fk: Self::Kind) -> &mut AFilter<Self::Kind>;
}

impl FilterKit for Filters {
    type Kind = FilterKind;
    fn get(&mut self, fk: Self::Kind) -> &mut AFilter<Self::Kind> {
        &mut self.0[fk as usize]
    }
}


pub(crate) struct ContexManager<F: FilterKit> {
    filters: F,
    current: <F as FilterKit>::Kind,
}

impl<F> ContexManager<F>
where F: FilterKit
{

    pub(crate) fn new() -> ContexManager<F> {
        ContexManager {
            filters: F::default(),
            current: F::Kind::default(),
        }
    }


    pub(crate) fn preprocess(&mut self, ctx: &mut Context, m: &Method) -> bool {
        println!("preprocess {:?}", self.current);
        self.filters.get(self.current).preprocess(ctx, m)
    }

    pub(crate) fn postprocess(
        &mut self,
        ctx: &mut Context,
        reply: &Result<Payload, Error>,
        conn: &mut Connection,
    ) {
        println!("preprocess {:?}", self.current);
        if let Ok(payload) = reply {
            self.current = self.filters.get(self.current).postprocess(ctx, payload, conn);
        }
    }
}
