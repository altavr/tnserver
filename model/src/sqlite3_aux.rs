//use std::collections::BTreeMap;
use tnsqlite3::ffi;
//use tnsqlite3::{Sqlite3Handler};
use std::os::raw::*;
use std::sync::{Mutex};
use crate::abs::Context;

//use  lazy_static::lazy_static;

//lazy_static! {
//    pub(crate) static ref CONNECTION_MAP: Mutex<BTreeMap<Sqlite3Handler, i64>> =
//        Mutex::new(BTreeMap::new());
//}

pub extern "C" fn getowner(
    ctx: *mut ffi::sqlite3_context,
    _nargs: c_int,
    _values: *mut *mut ffi::sqlite3_value,
) {
    unsafe {
        if let Some(model_ctx) = (ffi::sqlite3_user_data(ctx) as *const Context).as_ref() {
            dbg!(&model_ctx);
            println!("address - {:p}", model_ctx);
            ffi::sqlite3_result_int64(ctx, model_ctx.person_id);
        } else {
            ffi::sqlite3_result_int64(ctx, 0);
        }
    }
}
