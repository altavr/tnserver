use tn_proc::Stringinize;
use tnsqlite3::{Connection, Insert, Mapped, Mapping, Sqlite3Ref, Values};

#[derive(Clone, Debug, PartialEq, Mapping, Values, Insert)]
#[table = "task"]
pub struct Task {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub owner_id: i64,
    pub performer_id: i64,
    pub status_id: i64,
    pub start_time: i64,
   pub create_time: i64,
    pub deadline: i64,
    pub prio: i64,
     pub pmask_id: i64,
}

impl Task {
    pub fn new(owner_id: i64, performer_id: i64, status_id: i64, prio: i64, pmask_id: i64) -> Self {
        Task {
            id: 0,
            owner_id,
            performer_id,
            status_id,
            start_time: 0,
            create_time: 0,
            deadline: 0,
            prio,
            pmask_id,
        }
    }
}


#[derive(Debug, PartialEq, Mapping, Values, Insert)]
#[table = "status"]
pub struct Status {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub name: String,
    pub prio_flag: i64,
}

impl Status {
pub fn new(name: &str, prio_flag: i64) -> Self{
    Status {id: 0, name: name.to_string(), prio_flag}

}

}

#[derive(Debug, PartialEq, Mapping, Values, Insert)]
#[table = "group_"]
pub struct Group {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub name: String,
    pub parent: Option<i64>
}

impl Group  {
   pub   fn new(name: &str, parent:  Option<i64>) -> Self {
        Group {id:0, name: name.to_string(), parent}
    }
}

#[derive(Debug, PartialEq, Clone, Mapping, Values, Insert)]
#[table = "person"]
pub struct Person {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub login: String,
    pub pass_hashsum: Option<String>,
    pub fname: String,
    pub lname: String,
    pub mname: String,
    pub gr_base: i64,
    pub gr_1: Option<i64>,
    pub gr_2: Option<i64>,
    pub perm: i64,
}

impl Person {
    pub fn new(login: &str, pass_hashsum: Option<&str>, gr_base: i64, perm: i64) -> Self {
        Person {
            id: 0,
            login: login.to_string(),
            pass_hashsum: pass_hashsum.map(|h| h.to_string()),
            fname: "f_".to_string() + login,
            lname: "l_".to_string() + login,
            mname: "m_".to_string() + login,
            gr_base,
            gr_1: None,
            gr_2: None,
            perm,
        }
    }
}



#[derive(Debug, PartialEq, Values, Insert)]
#[table = "ecolumn"]
pub struct EColumn {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub name: String,
}

#[derive(Debug, PartialEq, Mapping, Values, Insert)]
#[table = "log"]
pub struct Log {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub task_id: i64,
    pub timestamp: i64,
    pub person_id: i64,
    pub column: String,
    pub data: String,
}

#[derive(Debug, PartialEq, Mapping, Values, Insert)]
#[table = "perm_mask"]
pub struct PermMask {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub name: String,
    pub owner_id: i64,
    pub group_id: i64,
}


impl PermMask {
    pub fn new(name: &str, owner_id: i64, group_id: i64) -> Self {
        PermMask {
            id: 0,
            name: name.to_string(),
            owner_id,
            group_id,
        }
    }
}


#[derive(Debug, PartialEq, Mapping, Values, Insert)]
#[table = "perm_group"]
pub struct PermGroup {
    #[no_value]
    #[no_insert_column]
    pub id: i64,
    pub pmask_id: i64,
    pub group_id: i64,
    pub perm: Vec<u8>,
}
impl PermGroup {
    pub fn new(pmask_id: i64, group_id: i64, perm: Vec<u8>) -> Self {
        PermGroup {
            id: 0,
            pmask_id,
            group_id,
            perm,
        }
    }
}


//impl_property!(Group, "group_", "name");

#[cfg(test)]
mod tests {}
