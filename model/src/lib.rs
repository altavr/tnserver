#![allow(unused)]
#![warn(clippy::all)]

//#![macro_use]
mod abs;
pub mod entity;
mod policy;
mod sql;
mod sqlite3_aux;
mod stypes;
mod table;

use abs::*;
pub use abs::{
    CodeError, ColumnType, Condition, Context, Error, Finder, Model, PermLevel, Permission,
    PersonPermission, Privileges, TryClone, Where_,
};
use entity::*;
use generic_array::{arr, typenum, ArrayLength, GenericArray};
use policy::ContexManager;
pub use policy::{FilterKit, Filters, PermissiveFilters};
use serde::{Deserialize, Serialize};
use sql::*;
use sqlite3_aux::getowner;
use std::cmp::{Ord, Ordering};
use std::convert::{TryFrom, TryInto};
use std::marker::PhantomData;
use std::os::raw;
use std::pin;
use std::result;
use std::string::ToString;
use std::time::{SystemTime, UNIX_EPOCH};
pub use stypes::RowId;
use tn_proc::Stringinize;
use tnsqlite3::{
    params, select_tuple, vec_params, Connection, Identify, Insert, Mapped, Mapping, PStatement,
};
//use sha3::{Digest, Sha3_256};

pub const JSON_RPC_VER: &str = "2.0";

pub use crate::sql::STATIC_FIELDS;
pub const STATIC_FIELDS_RO: i32 = 2;

type Column = Box<dyn Columnar<Stringinizer> + Send + 'static>;
type View = Box<dyn TableModel<Stringinizer> + Send + 'static>;

impl<V, G, P> FetchData for Table<V, G, P>
where
    V: Mapping,
    <V as Mapping>::Target: Stringinize<<V as Mapping>::Target, Stringinizer = Stringinizer>
        + Identify
        + GetRowPermission,
    P: SqlApply<Self>,
{
    type Stringinizer = Stringinizer;
    fn fetch_data(
        &mut self,
        ids: Vec<RowId>,
        s: &Stringinizer,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        let mut res = Vec::new();
        let stmt = fetch_stmt(&self.fetch_stmt, &ids);
        let p_stmt = P::sql_apply(self, ids, ctx, conn)?;
        for r in p_stmt.query(V::mapping)? {
            let target = r?;
            let metacolumn: Vec<_> = V::Target::COLUMN_KIND
                .iter()
                .cloned()
                .zip(
                    target
                        .get_row_permission(ctx)
                        .iter()
                        .map(|p| Permission::from(*p)),
                )
                .map(|(k, p)| MetaColumn {
                    permission: p,
                    kind: k,
                })
                .collect();

            let meta = Meta {
                h: MetaRow::default(),
                c: metacolumn,
                s: None,
            };
            let mut columns = Vec::new();
            let id = RowId(target.identify());
            V::Target::stringinize(target, &mut columns, s);
            let row = Row {
                id,
                columns,
                meta,
            };

            res.push(row);
        }
        Ok(res)
    }
}

macro_rules! impl_GetRowPermission {
    ($t:ty, $num:expr) => {
        impl GetRowPermission for $t {
            fn get_row_permission(&self, ctx: &Context) -> &[u8] {
                if ctx.perm.level(PermLevel::Administration) {
                    &[Permission::RW as u8; $num]
                } else {
                    &[Permission::RO as u8; $num]
                }
            }
        }
    };
}

impl Mapping for RowId {
    type Target = RowId;
    fn mapping(row: &mut tnsqlite3::Row) -> result::Result<Self::Target, tnsqlite3::Error> {
        Ok(RowId(row.get(0)?.ok_or_else(|| {
            tnsqlite3::Error::InnerError(tnsqlite3::InnerError::new(tnsqlite3::IRR::UnexpectNull(
                0,
            )))
        })?))
    }
}

pub struct Stringinizer {}

impl Stringinizer {
    pub(crate) fn new() -> Self {
        Stringinizer {}
    }
    fn from_task_person(&self, v: task::Person) -> String {
        format!(
            "{} {}. {}.",
            v.lname,
            v.fname.chars().next().unwrap_or('_'),
            v.mname.chars().next().unwrap_or('_')
        )
    }

    //    fn from_datetime(&self, v: DateTime) -> String {
    //        let d = UNIX_EPOCH + Duration::from_secs(v.fe as u64);
    //        let date_time = chrono::DateTime::<chrono::offset::Utc>::from(d);
    //        date_time.format("%Y-%m-%d %H:%M:%S").to_string()
    //    }
    //    fn from_prio_flag(&self, v: status::PrioFlagView) -> String {
    //        match v.flag {
    //            1 => "Affect".to_string(),
    //            0 => "Does not affect".to_string(),
    //            _ => "Undefined flag".to_string(),
    //        }
    //    }
}

#[derive(Mapped, Debug)]
struct DateTime {
    fe: i64,
}

macro_rules! impl_column_kinded {
    ($t:ty, $kind:tt) => {
        impl ColumnKinded for $t {
            const COLUMN_KINDED: ColumnKind = ColumnKind::$kind;
        }
    };
}

impl_column_kinded!(DateTime, DateTime);
impl_column_kinded!(String, Text);
impl_column_kinded!(Option<String>, Text);
impl_column_kinded!(i64, Int);

impl Stringinized for DateTime {
    type Stringinizer = Stringinizer;
    fn stringinized(self, _: &Self::Stringinizer) -> String {
        self.fe.to_string()
    }
}

impl Stringinized for String {
    type Stringinizer = Stringinizer;
    fn stringinized(self, _: &Self::Stringinizer) -> String {
        self
    }
}

impl Stringinized for Option<String> {
    type Stringinizer = Stringinizer;
    fn stringinized(self, _: &Self::Stringinizer) -> String {
        self.unwrap_or_default()
    }
}

impl Stringinized for i64 {
    type Stringinizer = Stringinizer;
    fn stringinized(self, _: &Self::Stringinizer) -> String {
        self.to_string()
    }
}

struct Table<V, G, P> {
    table_name: String,
    fetch_stmt: String,
    find_stmt: String,
    view: PhantomData<V>,
    update_block: Vec<Column>,
    table_header: Vec<String>,
    columns: &'static [&'static str],
    guard: G,
    pstmt_maker: P,
}

impl<V, G, P> Table<V, G, P> {
    fn new(
        table_name: &str,
        fetch_stmt: String,
        find_stmt: String,
        up_block: Vec<Column>,
        table_header: Vec<String>,
        columns: &'static [&'static str],
        guard: G,
        pstmt_maker: P,
    ) -> Self {
        Table {
            table_name: table_name.to_string(),
            fetch_stmt,
            find_stmt,
            view: PhantomData,
            update_block: up_block,
            table_header,
            columns,
            guard,
            pstmt_maker,
        }
    }
}

impl<V, G, P> GetColumn<Stringinizer> for Table<V, G, P> {
    fn get_column(
        &mut self,
        column_idx: i32,
    ) -> Result<&mut (dyn Columnar<Stringinizer> + Send + 'static)> {
        //        Ok(  self.update_block[0].as_mut() )
        self.update_block
            .get_mut(column_idx as usize)
            .map(|c| c.as_mut())
            .ok_or_else(|| CodeError::IndexOutOfRange.into())
    }
}

impl<V, G, P> AddColumns for Table<V, G, P> {}
impl<V, G, P> DeleteColumn for Table<V, G, P> {}

impl<V, G, P> Header for Table<V, G, P> {
    fn header(&self, _: &mut Connection) -> Result<Vec<String>> {
        Ok(self.table_header.clone())
    }
}

impl<V, G, P> Find for Table<V, G, P> {
    fn find(&mut self, finder: &Finder, conn: &mut Connection) -> Result<Vec<RowId>> {
        let (where_clause, params) = finder.into_params(&self.table_name, self.columns)?;
        let p_stmt = conn.prepare(&(self.find_stmt.clone() + &where_clause), &params)?;
        Ok(p_stmt.query(RowId::mapping)?.fallible_collect()?)
    }
}

struct StringColumn {
    update_stmt: String,
}

impl StringColumn {
    fn new(table: &str, column: &str) -> Self {
        let update_stmt = format!("UPDATE {} SET {}=? WHERE id=?", table, column);
        StringColumn { update_stmt }
    }
}

impl UpdateInt<Stringinizer> for StringColumn {}
impl FetchData for StringColumn {
    type Stringinizer = Stringinizer;
}

impl GetColumnType<Stringinizer> for StringColumn {
    fn get_column_type(
        &mut self,
        id: RowId,
        _column: i32,
        ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<ColumnType> {
        Ok(ColumnType::Str)
    }
}

impl UpdateStr<Stringinizer> for StringColumn {
    fn update_str(
        &mut self,
        id: RowId,
        _column: i32,
        value: &str,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        conn.execute(&self.update_stmt, &[value.into(), id.0.into()])?;
        Ok(())
    }
}

struct IntColumn {
    update_stmt: String,
}

impl IntColumn {
    fn new(table: &str, column: &str) -> Self {
        let update_stmt = format!("UPDATE {} SET {}=? WHERE id=?", table, column);
        IntColumn { update_stmt }
    }
}

impl UpdateInt<Stringinizer> for IntColumn {
    fn update_int(
        &mut self,
        id: RowId,
        _: i32,
        value: RowId,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        conn.execute(&self.update_stmt, &[value.0.into(), id.0.into()])?;
        Ok(())
    }
}
impl FetchData for IntColumn {
    type Stringinizer = Stringinizer;
}

impl GetColumnType<Stringinizer> for IntColumn {
    fn get_column_type(
        &mut self,
        id: RowId,
        _: i32,
        ctx: &Context,
        _: &mut Connection,
    ) -> Result<ColumnType> {
        Ok(ColumnType::Int)
    }
}
impl UpdateStr<Stringinizer> for IntColumn {}

struct OTMView<V, N, P> {
    fetch_stmt: String,
    pstmt_maker: PhantomData<P>,
    view: PhantomData<V>,
    null: PhantomData<N>,
}

impl<V, N, P> OTMView<V, N, P>
where
    V: Mapping + Stringinize<V> + Identify,
{
    pub(crate) fn new(fetch_stmt: String) -> Self {
        OTMView {
            fetch_stmt,
            pstmt_maker: PhantomData,
            view: PhantomData,
            null: PhantomData,
        }
    }
}

impl<V, N> OTMView<V, N, WithoutParams>
where
    V: Mapping + Stringinize<V> + Identify,
{
    pub(crate) fn ordinary(table: &str, columns: &[&str]) -> Self {
        let columns = columns.join(", ");
        let fetch_stmt = format!("SELECT id, {} FROM {} ", columns, table);
        OTMView::new(fetch_stmt)
    }
}

struct WithoutParams;

impl<V, N> SqlApply<OTMView<V, N, WithoutParams>> for WithoutParams {
    fn sql_apply<'a>(
        src: &OTMView<V, N, WithoutParams>,
        _ids: Vec<RowId>,
        _ctx: &Context,
        conn: &'a mut Connection,
    ) -> result::Result<PStatement<'a>, tnsqlite3::Error> {
        conn.prepare(&src.fetch_stmt, &[])
    }
}

impl<V, G> SqlApply<Table<V, G, WithoutParams>> for WithoutParams {
    fn sql_apply<'a>(
        src: &Table<V, G, WithoutParams>,
        ids: Vec<RowId>,
        _ctx: &Context,
        conn: &'a mut Connection,
    ) -> result::Result<PStatement<'a>, tnsqlite3::Error> {
        let stmt = fetch_stmt(&src.fetch_stmt, &ids);
        conn.prepare(&stmt, &[])
    }
}

struct WithNullVar;
struct WithoutNullVar;

impl<V, P> FetchData for OTMView<V, WithoutNullVar, P>
where
    V: Mapping,
    <V as Mapping>::Target:
        Stringinize<<V as Mapping>::Target, Stringinizer = Stringinizer> + Identify,
    P: SqlApply<Self>,
{
    type Stringinizer = Stringinizer;
    fn fetch_data(
        &mut self,
        ids: Vec<RowId>,
        s: &Stringinizer,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        let p_stmt = P::sql_apply(self, ids, ctx, conn)?;
        let mut res = Vec::new();
        for r in p_stmt.query(V::mapping)? {
            let target = r?;
            let mut columns = Vec::new();
            let id = RowId(target.identify());
            V::Target::stringinize(target, &mut columns, s);
            let row_len = columns.len();
            let row = Row {
                id,
                columns,
                meta: Meta::default_from_num(row_len),
            };
            res.push(row);
        }
        Ok(res)
    }
}

impl<V, P> FetchData for OTMView<V, WithNullVar, P>
where
    V: Mapping,
    <V as Mapping>::Target:
        Stringinize<<V as Mapping>::Target, Stringinizer = Stringinizer> + Identify,
    P: SqlApply<Self>,
{
    type Stringinizer = Stringinizer;
    fn fetch_data(
        &mut self,
        ids: Vec<RowId>,
        s: &Stringinizer,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        let p_stmt = P::sql_apply(self, ids, ctx, conn)?;
        let mut res = Vec::new();
        for r in p_stmt.query(V::mapping)? {
            let target = r?;
            let mut columns = Vec::new();
            let id = RowId(target.identify());
            V::Target::stringinize(target, &mut columns, s);
            let row_len = columns.len();
            let row = Row {
                id,
                columns,
                meta: Meta::default_from_num(row_len),
            };
            res.push(row);
        }
        if !res.is_empty() {
            let row_len = res[0].columns.len();
            res.push(Row {
                id: RowId(0),
                columns: (0..row_len).map(|_| String::new()).collect(),
                meta: Meta::default_from_num(row_len),
            });
        }
        Ok(res)
    }
}

struct DefComplexUpdater<N> {
    update_stmt: String,
    null: PhantomData<N>,
}

impl<N> DefComplexUpdater<N> {
    fn new(src_table: &str, src_column: &str) -> Self {
        let update_stmt = format!("UPDATE {} SET {}=? WHERE id=?", src_table, src_column);
        DefComplexUpdater {
            update_stmt,
            null: PhantomData,
        }
    }
}

struct WithNullUp;
struct WithoutNullUp;

impl<N> UpdateStr<Stringinizer> for DefComplexUpdater<N> {}

impl UpdateInt<Stringinizer> for DefComplexUpdater<WithoutNullUp> {
    fn update_int(
        &mut self,
        id: RowId,
        _: i32,
        value: RowId,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        conn.execute(&self.update_stmt, &[value.0.into(), id.0.into()])?;
        Ok(())
    }
}

impl UpdateInt<Stringinizer> for DefComplexUpdater<WithNullUp> {
    fn update_int(
        &mut self,
        id: RowId,
        _: i32,
        value: RowId,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        if value.0 == 0 {
            conn.execute(
                &self.update_stmt,
                &[tnsqlite3::Sqlite3Ref::NULL, id.0.into()],
            )?;
        } else {
            conn.execute(&self.update_stmt, &[value.0.into(), id.0.into()])?;
        }
        Ok(())
    }
}

struct PermissiveGuard;

impl WriteAccess for PermissiveGuard {
    fn write_access_exists(
        &self,
        _rowid: i64,
        _column: i32,
        _ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<bool> {
        Ok(true)
    }
    fn write_access(
        &self,
        _rowid: i64,
        _column: i32,
        _ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<()> {
        Ok(())
    }
}

struct OneToManyColumn<T, U> {
    updater: U,
    ref_column: T,
}

impl<T, U> OneToManyColumn<T, U>
where
    T: FetchData<Stringinizer = Stringinizer>,
    U: UpdateInt<Stringinizer> + UpdateStr<Stringinizer>,
{
    fn new(updater: U, ref_column: T) -> Self {
        OneToManyColumn {
            updater,
            ref_column,
        }
    }
}

impl<T, U> GetColumnType<Stringinizer> for OneToManyColumn<T, U> {
    fn get_column_type(
        &mut self,
        id: RowId,
        column: i32,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<ColumnType> {
        Ok(ColumnType::Complex)
    }
}

impl<T, U> UpdateStr<Stringinizer> for OneToManyColumn<T, U>
where
    U: UpdateStr<Stringinizer>,
{
    fn update_str(
        &mut self,
        id: RowId,
        column: i32,
        value: &str,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        self.updater.update_str(id, column, value, ctx, conn)
    }
}

impl<T, U> UpdateInt<Stringinizer> for OneToManyColumn<T, U>
where
    U: UpdateInt<Stringinizer>,
{
    fn update_int(
        &mut self,
        id: RowId,
        column: i32,
        value: RowId,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        self.updater.update_int(id, column, value, ctx, conn)
    }
}

impl<T, U> FetchData for OneToManyColumn<T, U>
where
    T: FetchData<Stringinizer = Stringinizer>,
{
    type Stringinizer = Stringinizer;
    fn fetch_data(
        &mut self,
        _: Vec<RowId>,
        s: &Stringinizer,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        //        self.ref_column.find(&Finder::new(), conn)?;
        self.ref_column.fetch_data(Vec::new(), s, ctx, conn)
    }
}

#[derive(Mapping, Stringinize, Identify, Default)]
#[stringinizer = "Stringinizer"]
struct SingleColumnRow {
    #[stringinize_ignore]
    #[row_id]
    id: i64,
    c: String,
}

struct UnchangeableColumn;

impl GetColumnType<Stringinizer> for UnchangeableColumn {
    fn get_column_type(
        &mut self,
        id: RowId,
        _: i32,
        ctx: &Context,
        _: &mut Connection,
    ) -> Result<ColumnType> {
        Ok(ColumnType::Unchangeable)
    }
}

impl FetchData for UnchangeableColumn {
    type Stringinizer = Stringinizer;
}
impl UpdateInt<Stringinizer> for UnchangeableColumn {}
impl UpdateStr<Stringinizer> for UnchangeableColumn {}

struct FlagColumn<T> {
    update_stmt: String,
    c: Vec<T>,
    ctype: ColumnType,
}

impl<T> FlagColumn<T> {
    fn new(table: &str, column: &str, c: Vec<T>, ctype: ColumnType) -> Self {
        let update_stmt = format!("UPDATE {} SET {}=? WHERE id=?", table, column);
        FlagColumn {
            update_stmt,
            c,
            ctype,
        }
    }
}

impl<T> GetColumnType<Stringinizer> for FlagColumn<T> {
    fn get_column_type(
        &mut self,
        id: RowId,
        _column: i32,
        ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<ColumnType> {
        Ok(self.ctype)
    }
}

impl<T> FetchData for FlagColumn<T>
where
    T: ToString,
{
    type Stringinizer = Stringinizer;
    fn fetch_data(
        &mut self,
        _: Vec<RowId>,
        _s: &Self::Stringinizer,
        _ctx: &Context,
        _: &mut Connection,
    ) -> Result<Vec<Row>> {
        Ok(self
            .c
            .iter()
            .enumerate()
            .map(|(i, c)| Row {
                id: RowId(i as i64),
                columns: vec![c.to_string()],
                meta: Meta::default_from_num(1),
            })
            .collect::<Vec<_>>())
    }
}

impl<T> UpdateInt<Stringinizer> for FlagColumn<T>
where
    T: Into<i64> + Copy,
{
    fn update_int(
        &mut self,
        id: RowId,
        _: i32,
        value: RowId,
        _ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        if self.ctype == ColumnType::Unchangeable {
            return Err(Error::ModelError(CodeError::OpertionNotPermitten));
        }
        let value = (*self
            .c
            .get(value.0 as usize)
            .ok_or(Error::ModelError(CodeError::ValueNotCorrect))?)
        .into();
        conn.execute(&self.update_stmt, params!(value, id.0))?;
        Ok(())
    }
}

impl<T> UpdateStr<Stringinizer> for FlagColumn<T> {}

mod group {
    use super::*;

    #[derive(Stringinize, Mapping, Identify, Default)]
    #[stringinizer = "Stringinizer"]
    pub(crate) struct Group {
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        name: String,
        parent: Option<String>,
    }

    impl Table<Group, PermissiveGuard, WithoutParams> {
        pub(crate) fn group() -> Self {
            let columns = &["name", "parent"];
            let fetch_stmt = "SELECT group_.id, group_.name, pg.name
            FROM group_
            OUTER LEFT JOIN group_ AS pg ON group_.parent = pg.id
            WHERE group_.id in "
                .to_string();
            let find_stmt = "SELECT group_.id
            FROM group_
            OUTER LEFT JOIN group_ AS pg ON group_.parent = pg.id"
                .to_string();
            let up_block: Vec<Column> = vec![
                Box::new(StringColumn::new("group_", "name")),
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithNullUp>::new("group_", "parent"),
                    OTMView::<SingleColumnRow, WithNullVar, WithoutParams>::ordinary(
                        "group_",
                        &["name"],
                    ),
                )),
            ];
            let header = vec!["Name".into(), "Paret".into()];
            Self::new(
                "group_",
                fetch_stmt,
                find_stmt,
                up_block,
                header,
                columns,
                PermissiveGuard,
                WithoutParams,
            )
        }
    }

    //    impl_fetch_data!(Table<Group, G, WithoutParams>, Group);
    impl_GetRowPermission!(Group, Group::COLUMN_KIND.len());
}

mod status {
    use super::*;

    #[derive(Clone, Copy, Debug)]
    pub(crate) enum PrioFlagView {
        NoAffect = 0,
        Affect = 1,
    }

    impl Into<i64> for PrioFlagView {
        fn into(self) -> i64 {
            self as i64
        }
    }

    impl Mapped<PrioFlagView> for PrioFlagView {
        fn mapped(row: &mut tnsqlite3::Row, column: i32) -> result::Result<Self, tnsqlite3::Error> {
            match row.get(column)?.ok_or_else(|| {
                tnsqlite3::Error::InnerError(tnsqlite3::InnerError::new(
                    tnsqlite3::IRR::UnexpectNull(column),
                ))
            }) {
                Ok(1) => Ok(PrioFlagView::Affect),
                Ok(_) => Ok(PrioFlagView::NoAffect),
                Err(e) => Err(e),
            }
        }
    }

    impl ToString for PrioFlagView {
        fn to_string(&self) -> String {
            match self {
                PrioFlagView::Affect => "Affect".to_string(),
                PrioFlagView::NoAffect => "Does not affect".to_string(),
            }
        }
    }

    impl_column_kinded!(PrioFlagView, Text);

    impl Stringinized for PrioFlagView {
        type Stringinizer = Stringinizer;
        fn stringinized(self, _r: &Self::Stringinizer) -> String {
            self.to_string()
        }
    }

    #[derive(Stringinize, Mapping, Identify)]
    #[stringinizer = "Stringinizer"]
    pub(crate) struct Status {
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        name: String,
        prio_flag: PrioFlagView,
    }

    impl Table<Status, PermissiveGuard, WithoutParams> {
        pub(crate) fn status() -> Self {
            let columns = &["name", "prio_flag"];
            let fetch_stmt = gen_fetch_stmt("status", columns);
            let find_stmt = gen_find_stmt("status");
            let up_block: Vec<Column> = vec![
                Box::new(StringColumn::new("status", "name")),
                Box::new(FlagColumn::new(
                    "status",
                    "prio_flag",
                    vec![PrioFlagView::NoAffect, PrioFlagView::Affect],
                    ColumnType::Unchangeable,
                )),
            ];
            let header = vec!["Name".into(), "Priority flag".into()];
            Self::new(
                "status",
                fetch_stmt,
                find_stmt,
                up_block,
                header,
                columns,
                PermissiveGuard,
                WithoutParams,
            )
        }
    }

    //    impl_fetch_data!(Table<Status, G,WithoutParams >, Status);
    impl_GetRowPermission!(Status, Status::COLUMN_KIND.len());
}

mod person {
    use super::*;

    impl Mapped<Privileges> for Privileges {
        fn mapped(row: &mut tnsqlite3::Row, column: i32) -> result::Result<Self, tnsqlite3::Error> {
            use tnsqlite3::*;
            let value: i64 = row.get(column)?.ok_or_else(|| {
                tnsqlite3::Error::InnerError(InnerError::new(IRR::UnexpectNull(column)))
            })?;

            Privileges::try_from(value)
                .map_err(|_| Error::InnerError(tnsqlite3::InnerError::new(IRR::ConversionFailed)))
        }
    }

    impl_column_kinded!(Privileges, Text);

    impl Stringinized for Privileges {
        type Stringinizer = Stringinizer;
        fn stringinized(self, _r: &Self::Stringinizer) -> String {
            self.to_string()
        }
    }

    #[derive(Stringinize, Mapping, Identify)]
    #[stringinizer = "Stringinizer"]
    pub(crate) struct Person {
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        fname: String,
        lname: String,
        mname: String,
        group_base: String,
        base_1: Option<String>,
        base_2: Option<String>,
        login: String,
        privileges: Privileges,
    }

    impl Table<Person, PermissiveGuard, WithoutParams> {
        pub(crate) fn person() -> Self {
            let columns = &["fname", "lname", "mname", "gr_base", "gr_1", "gr_2"];
            let fetch_stmt = "SELECT person.id, fname, lname, mname, gb.name, g1.name, g2.name,
                login, perm
                   FROM  person
                   OUTER LEFT JOIN group_ AS gb ON gb.id = person.gr_base
                   OUTER LEFT JOIN group_ AS g1 ON g1.id = person.gr_1
                   OUTER LEFT JOIN group_ AS g2 ON g2.id = person.gr_2
                   WHERE person.id in "
                .to_string();
            let find_stmt = "SELECT person.id
                FROM  person
                OUTER LEFT JOIN group_ AS gb ON gb.id = person.gr_base
                OUTER LEFT JOIN group_ AS g1 ON g1.id = person.gr_1
                OUTER LEFT JOIN group_ AS g2 ON g2.id = person.gr_2"
                .to_string();
            let up_block: Vec<Column> = vec![
                Box::new(StringColumn::new("person", "fname")),
                Box::new(StringColumn::new("person", "lname")),
                Box::new(StringColumn::new("person", "mname")),
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithoutNullUp>::new("person", "gr_base"),
                    OTMView::<SingleColumnRow, WithoutNullVar, WithoutParams>::ordinary(
                        "group_",
                        &["name"],
                    ),
                )),
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithNullUp>::new("person", "gr_1"),
                    OTMView::<SingleColumnRow, WithNullVar, WithoutParams>::ordinary(
                        "group_",
                        &["name"],
                    ),
                )),
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithNullUp>::new("person", "gr_2"),
                    OTMView::<SingleColumnRow, WithNullVar, WithoutParams>::ordinary(
                        "group_",
                        &["name"],
                    ),
                )),
                Box::new(UnchangeableColumn),
                Box::new(FlagColumn::new(
                    "person",
                    "perm",
                    vec![
                        Privileges::Ordinary,
                        Privileges::PermMaskCreate,
                        Privileges::Administration,
                    ],
                    ColumnType::Complex,
                )),
            ];
            let header = [
                "First name",
                "Last name",
                "Middle name",
                "Base group",
                "Group 1",
                "Group 2",
                "Login",
                "Privileges",
            ]
            .iter()
            .map(|&s| s.to_string())
            .collect();
            Self::new(
                "person",
                fetch_stmt,
                find_stmt,
                up_block,
                header,
                columns,
                PermissiveGuard,
                WithoutParams,
            )
        }
    }

    //    impl_fetch_data!(Table<Person, G, WithoutParams >, Person);
    impl_GetRowPermission!(Person, Person::COLUMN_KIND.len());
}

pub(crate) mod task {
    use super::*;
    use crate::abs::*;

    use crate::sql::*;
    use crate::stypes::RowId;
    use crate::EColumn;
    use std::result;
    use tn_proc::Stringinize;
    use tnsqlite3::{Connection, Identify, Insert, Mapped, Mapping};

    impl_column_kinded!(Person, Text);

    impl Stringinized for Person {
        type Stringinizer = Stringinizer;
        fn stringinized(self, r: &Self::Stringinizer) -> String {
            r.from_task_person(self)
        }
    }

    #[derive(Mapped, Debug)]
    pub(crate) struct Person {
        pub(crate) fname: String,
        pub(crate) lname: String,
        pub(crate) mname: String,
    }

    impl Mapped<DataColumns> for DataColumns {
        fn mapped(
            row: &mut tnsqlite3::Row,
            column: i32,
        ) -> result::Result<DataColumns, tnsqlite3::Error> {
            let mut columns: Vec<Option<String>> = Vec::new();
            for i in column..row.count() {
                columns.push(row.get(i)?)
            }
            Ok(DataColumns(Some(columns)))
        }
    }

    #[derive(Debug)]
    struct DataColumns(Option<Vec<Option<String>>>);


    const STATIC_FIELDS_INC: i32 = STATIC_FIELDS - 1;
    pub(crate) const MAX_LEN_TASK_ROW: i32 = 24;

    #[derive(Stringinize, Mapping, Debug)]
    #[stringinizer = "Stringinizer"]
    struct Task {
        #[stringinize_ignore]
        perm: Vec<u8>,
        #[stringinize_ignore]
        id: i64,
        #[stringinize_ignore]
        performer_id: i64,
        #[mapped_length = 3]
        owner: Person,
        #[mapped_length = 3]
        performer: Person,
        status: String,
        start_time: DateTime,
        deadline: DateTime,
        prio: i64,
        pmask: String,
        create_time: DateTime,
        group: String,
        #[stringinize_ignore]
        data: DataColumns,
    }

    impl GetRowPermission for Task {
        fn get_row_permission(&self, ctx: &Context) -> &[u8] {
            &self.perm
        }
    }

    pub(crate) struct MultiColumn;

    impl MultiColumn {
        pub(crate) fn count(conn: &mut Connection) -> Result<i32> {
            let count = conn
                .prepare("SELECT COUNT(*) FROM ecolumn", &[])?
                .query(i64::mapping)?
                .next()
                .ok_or_else(|| {
                    CodeError::GeneralError(
                        "SELECT COUNT(*) FROM ecolumn not return row".to_string(),
                    )
                })??;
            Ok(count as i32)
        }
        pub(crate) fn names(conn: &mut Connection) -> Result<Vec<String>> {
            let stmt = "SELECT name FROM ecolumn ORDER BY id";
            //            conn.prepare(stmt, &[])?;
            Ok(conn
                .prepare(stmt, &[])?
                .query(String::mapping)?
                .fallible_collect()?)
        }
        fn update_names(columns: Vec<String>, conn: &Connection) -> Result<()> {
            conn.execute("DELETE FROM ecolumn", &[])?;
            for i in columns {
                let col = EColumn { id: 0, name: i };
                col.insert(conn)?;
            }
            Ok(())
        }
    }

    impl FetchData for MultiColumn {
        type Stringinizer = Stringinizer;
    }
    impl UpdateInt<Stringinizer> for MultiColumn {}
    impl GetColumnType<Stringinizer> for MultiColumn {
        fn get_column_type(
            &mut self,
            id: RowId,
            _: i32,
            ctx: &Context,
            _: &mut Connection,
        ) -> Result<ColumnType> {
            Ok(ColumnType::Str)
        }
    }

    impl UpdateStr<Stringinizer> for MultiColumn {
        fn update_str(
            &mut self,
            id: RowId,
            column: i32,
            value: &str,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<()> {
            let stmt = format!(
                "INSERT INTO text_table (task_id, column_{0}) VALUES (?, ?)
                    ON CONFLICT (task_id) DO UPDATE SET column_{0} = excluded.column_{0}",
                column.to_string()
            );
            conn.execute(&stmt, &[id.0.into(), value.into()])?;
            Ok(())
        }
    }

    #[derive(Mapping, Identify, Stringinize)]
    #[stringinizer = "Stringinizer"]
    struct PersonVariant {
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        #[mapped_length = 3]
        person: Person,
    }

    #[derive(Mapping, Identify, Stringinize)]
    #[stringinizer = "Stringinizer"]
    struct StatusVariant {
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        name: String,
    }

    pub(crate) const STATIC_HEADER: &[&str] = &[
        "Owner",
        "Executor",
        "Status",
        "Start time",
        "Deadline",
        "Priority",
        "Access mask",
        "Creation time",
        "Group",
    ];

  pub(crate)  struct TaskGuard;

    #[cfg(not(feature = "column_permission_disable"))]
    impl WriteAccess for TaskGuard {
        fn write_access_exists(
            &self,
            rowid: i64,
            column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<bool> {

            let p_stmt = conn.prepare(
                TASK_GUARD_STMT,
                params!(ctx.person_id, ctx.person_id, ctx.person_id, rowid),
            )?;
            for p in p_stmt.query(select_tuple!(Vec<u8>))? {
                dbg!(&p);
                if Permission::from(*p?.get(column as usize).ok_or(CodeError::IndexOutOfRange)?)
                    == Permission::RW
                {
                    return Ok(true);
                }
            }
            Ok(false)
        }
    }

    #[cfg(feature = "column_permission_disable")]
    impl WriteAccess for TaskGuard {
        fn write_access_exists(
            &self,
            rowid: i64,
            column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<bool> {
            Ok(true)
        }
    }

    pub(crate) struct ExtTable {
        static_header: Vec<String>,
        up_block: Vec<Column>,
        ext_up_block: MultiColumn,
        static_columns: &'static [&'static str],
    }

    impl GetColumnType<Stringinizer> for ExtTable {
        fn get_column_type(
            &mut self,
            id: RowId,
            column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<ColumnType> {
            if !TaskGuard::write_access_exists(&TaskGuard, id.0, column, ctx, conn)? {
                return Ok(ColumnType::Unchangeable);
            }

            match column {
                0..=STATIC_FIELDS_INC => {
                    self.up_block[column as usize].get_column_type(id, column, ctx, conn)
                }
                _ => self
                    .ext_up_block
                    .get_column_type(id, column - STATIC_FIELDS, ctx, conn),
            }
        }
    }

    impl UpdateInt<Stringinizer> for ExtTable {
        fn update_int(
            &mut self,
            id: RowId,
            column: i32,
            value: RowId,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<()> {
            TaskGuard::write_access(&TaskGuard, id.0, column, ctx, conn)?;
            match column {
                0..=STATIC_FIELDS_INC => {
                    self.up_block[column as usize].update_int(id, column, value, ctx, conn)
                }
                _ => self
                    .ext_up_block
                    .update_int(id, column - STATIC_FIELDS, value, ctx, conn),
            }
        }
    }

    impl UpdateStr<Stringinizer> for ExtTable {
        fn update_str(
            &mut self,
            id: RowId,
            column: i32,
            value: &str,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<()> {
            TaskGuard::write_access(&TaskGuard, id.0, column, ctx, conn)?;
            match column {
                0..=STATIC_FIELDS_INC => {
                    self.up_block[column as usize].update_str(id, column, value, ctx, conn)
                }
                _ => self
                    .ext_up_block
                    .update_str(id, column - STATIC_FIELDS, value, ctx, conn),
            }
        }
    }
    impl ColumnVariants<Stringinizer> for ExtTable {
        fn column_variants(
            &mut self,
            column: i32,
            s: &Stringinizer,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<Vec<Row>> {
            match column {
                0..=STATIC_FIELDS_INC => {
                    self.up_block[column as usize].fetch_data(Vec::new(), s, ctx, conn)
                }
                _ => self.ext_up_block.fetch_data(Vec::new(), s, ctx, conn),
            }
        }
    }

    impl ExtTable {
        pub(crate) fn new() -> Self {
            let up_block: Vec<Column> = vec![
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithoutNullUp>::new("task", "owner_id"),
                    OTMView::<PersonVariant, WithoutNullVar, WithoutParams>::ordinary(
                        "person",
                        &["fname", "lname", "mname"],
                    ),
                )),
                Box::new(OneToManyColumn::new(
                    ExecutorUpdater,
                    OTMView::<PersonVariant, WithoutNullVar, WithoutParams>::ordinary(
                        "person",
                        &["fname", "lname", "mname"],
                    ),
                )),
                Box::new(OneToManyColumn::new(
                    StatusUpdater,
                    OTMView::<StatusVariant, WithoutNullVar, WithoutParams>::ordinary(
                        "status",
                        &["name"],
                    ),
                )),
                Box::new(IntColumn::new("task", "start_time")),
                Box::new(IntColumn::new("task", "deadline")),
                Box::new(UnchangeableColumn),
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithoutNullUp>::new("task", "pmask_id"),
                    OTMView::<SingleColumnRow, WithoutNullVar, WithoutParams>::ordinary(
                        "perm_mask",
                        &["name"],
                    ),
                )),
                Box::new(UnchangeableColumn),
                Box::new(UnchangeableColumn),
            ];
            let static_header = STATIC_HEADER.iter().map(|&h| h.to_string()).collect();
            let static_columns = &[
                "task.owner_id",
                "task.performer_id",
                "task.status_id",
                "task.start_time",
                "task.deadline",
                "task.prio",
                "task.pmask_id",
                "task.create_time",
                "gr.name",
            ];
            ExtTable {
                static_header,
                up_block,
                ext_up_block: MultiColumn,
                static_columns,
            }
        }

        fn create_text_table(
            &self,
            name: &str,
            column_count: usize,
            conn: &Connection,
        ) -> Result<()> {
            let columns_expr = (0..column_count)
                .map(|i| format!("column_{}  TEXT,", i.to_string()))
                .collect::<Vec<_>>()
                .join("\n");

            let stmt = format!(
                "CREATE TABLE  {} (
                                    task_id     INTEGER NOT NULL UNIQUE,
                                    {}
                                    FOREIGN KEY (task_id) REFERENCES task(id))",
                name, columns_expr
            );
            conn.execute(&stmt, &[])?;
            Ok(())
        }

        fn table_is_exists(&self, name: &str, conn: &Connection) -> Result<bool> {
            let stmt = "SELECT name FROM sqlite_master  WHERE type='table' ";
            let p_stmt = conn.prepare(stmt, &[])?;
            for i in p_stmt.query(String::mapping)? {
                let i = i?;
                dbg!(&i);
                if i == name {
                    return Ok(true);
                }
            }
            Ok(false)
        }
    }

    fn text_column_expr(count: i32) -> Vec<String> {
        (0..count)
            .map(|i| format!(", column_{}", i.to_string()))
            .collect::<Vec<_>>()
    }

    fn select_text_columns(count: i32) -> Vec<String> {
        (0..count)
            .map(|i| format!(", tt.column_{}", i.to_string()))
            .collect::<Vec<_>>()
    }
    fn select_text_column_expr(count: i32) -> String {
        (0..count)
            .map(|i| format!(", tt.column_{}", i.to_string()))
            .collect::<Vec<_>>()
            .join("")
    }

    impl Find for ExtTable {
        fn find(&mut self, finder: &Finder, conn: &mut Connection) -> Result<Vec<RowId>> {
            let (where_clause, params) = finder.into_params("task", self.static_columns)?;

            let stmt = to_find_task_stmt(&where_clause);
            Ok(conn
                .prepare(&stmt, &params)?
                .query(RowId::mapping)?
                .fallible_collect()?)
        }
    }

    impl FetchData for ExtTable {
        type Stringinizer = Stringinizer;
        fn fetch_data(
            &mut self,
            ids: Vec<RowId>,
            s: &Stringinizer,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<Vec<Row>> {
            let mut res = Vec::new();
            let stmt = to_fetch_task_stmt(
                &select_text_column_expr(MultiColumn::count(conn)?),
                &gen_ids_str(&ids),
                STATIC_FIELDS,
            );
            let usr_id = ctx.person_id;
            let p_stmt = conn.prepare(&stmt, params!(usr_id, usr_id, usr_id))?;
            for r in p_stmt.query(Task::mapping)? {
                let mut target = r?;
                let mut columns = Vec::new();
                let id = RowId(target.id);

                let performer_id = target.performer_id;
                let data_rows = target.data.0.take().unwrap_or_default();
                //                let column_perm = target.row_access(ctx);
                dbg!(&target);
                let metacolumn: Vec<_> = <Task as Stringinize<Task>>::COLUMN_KIND
                    .iter()
                    .cloned()
                    .chain([ColumnKind::Text].iter().cloned().cycle())
                    .zip(
                        target
                            .get_row_permission(ctx)
                            .iter()
                            .map(|p| Permission::from(*p)),
                    )
                    .map(|(k, p)| MetaColumn {
                        permission: p,
                        kind: k,
                    })
                    .collect();
                let meta = Meta {
                    h: MetaRow::default(),
                    c: metacolumn,
                    s: Some(RowId(performer_id)),
                };
                <Task as Stringinize<Task>>::stringinize(target, &mut columns, s);
                data_rows
                    .into_iter()
                    .for_each(|r| columns.push(r.unwrap_or_default()));

                let row = Row { id, columns, meta };
                res.push(row);
            }
            Ok(res)
        }
    }

    impl AddColumns for ExtTable {
        fn add_columns(&self, column_list: Vec<String>, conn: &mut Connection) -> Result<()> {
            if !self.table_is_exists("text_table", conn)? {
                let tx = conn.transaction()?;
                self.create_text_table("text_table", column_list.len(), conn)?;
                MultiColumn::update_names(column_list, conn)?;
                tx.commit()?;
                return Ok(());
            }

            if column_list.is_empty() {
                return Ok(());
            }
            let mut columns = MultiColumn::names(conn)?;

            let stmt = format!(
                "INSERT INTO text_table_temp (task_id {0})
                    SELECT task_id {0}  FROM  text_table",
                text_column_expr(columns.len() as i32).join(""),
            );

            let old_len = column_list.len();
            columns.extend(column_list);
            let new_len = columns.len();
            conn.execute("PRAGMA foreign_keys=OFF", &[])?;
            {
                let tx = conn.transaction()?;
                self.create_text_table("text_table_temp", columns.len(), conn)?;
                conn.execute(&stmt, &[])?;

                MultiColumn::update_names(columns, conn)?;
                conn.execute("DROP TABLE text_table", &[])?;

                conn.execute(
                    "ALTER TABLE text_table_temp RENAME TO 'text_table';
                   PRAGMA foreign_key_check ",
                    &[],
                )?;

                conn.execute(" PRAGMA foreign_key_check ", &[])?;

                //синхронизация с таблицей прав доступа
                let permvec = conn
                    .prepare("SELECT id, perm FROM perm_group", &[])?
                    .query(select_tuple!(i64, Vec<u8>))?
                    .fallible_collect()?;
                for (id, mut perm) in permvec {
                    perm.extend(vec![Permission::default() as u8; old_len]);
                    conn.execute(
                        "UPDATE perm_group SET perm = ?
                                  WHERE id=?",
                        params!(&perm, id),
                    )?;
                }

                if self.table_is_exists("fts", conn)? {
                    conn.execute(" DROP   TABLE  fts ;", &[])?;
                }

                for t in create_fts_table_stmt(new_len).iter() {
                    conn.execute(t, &[])?;
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //TODO Add to fts old data

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                tx.commit()?;
            }
            conn.execute("PRAGMA foreign_keys=ON", &[])?;
            for t in text_triggers_stmt(new_len).iter() {
                conn.execute(t, &[])?;
            }

            Ok(())
        }
    }

    impl DeleteColumn for ExtTable {
        fn delete_column(&self, idx: i32, conn: &mut Connection) -> Result<()> {
            dbg!("DeleteColumn");
            if !self.table_is_exists("text_table", conn)? {
                return Err(CodeError::OpertionNotPermitten.into());
            }
            let count = MultiColumn::count(conn)?;
            if (idx >= count + STATIC_FIELDS) || idx < STATIC_FIELDS {
                return Err(CodeError::IndexOutOfRange.into());
            }
            if count <= 1 {
                return Err(CodeError::OpertionNotPermitten.into());
            }
            let idx_offset = idx - STATIC_FIELDS;
            let mut column_names = MultiColumn::names(conn)?;
            column_names.remove(idx_offset as usize);
            let mut columns_old = text_column_expr(count);
            columns_old.remove(idx_offset as usize);
            let columns_new = text_column_expr(count - 1);

            let stmt = format!(
                "INSERT INTO text_table_temp (task_id {})
                    SELECT task_id {}  FROM  text_table",
                columns_new.join(""),
                columns_old.join(""),
            );
            conn.execute("PRAGMA foreign_keys=OFF", &[])?;
            {
                let tx = conn.transaction()?;
                self.create_text_table("text_table_temp", columns_new.len(), conn)?;
                conn.execute(&stmt, &[])?;
                MultiColumn::update_names(column_names, conn)?;
                conn.execute("DROP TABLE text_table", &[])?;
                conn.execute(
                    "ALTER TABLE text_table_temp RENAME TO 'text_table';
                   PRAGMA foreign_key_check ",
                    &[],
                )?;

                conn.execute(" PRAGMA foreign_key_check ", &[])?;

                //синхронизация с таблицей прав доступа
                let permvec = conn
                    .prepare("SELECT id, perm FROM perm_group", &[])?
                    .query(select_tuple!(i64, Vec<u8>))?
                    .fallible_collect()?;
                for (id, mut perm) in permvec {
                    perm.remove(idx as usize);
                    perm.push(Permission::default() as u8);
                    conn.execute(
                        "UPDATE perm_group SET perm = ?
                                  WHERE id=?",
                        params!(&perm, id),
                    )?;
                }

                if self.table_is_exists("fts", conn)? {
                    conn.execute("  DROP   TABLE  fts ", &[])?;
                }

                for t in create_fts_table_stmt(columns_new.len()).iter() {
                    conn.execute(t, &[])?;
                }
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                //TODO Add to fts old data

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                tx.commit()?;
            }
            //recreate triggers for log
            conn.execute("PRAGMA foreign_keys=ON", &[])?;
            for t in text_triggers_stmt(columns_new.len()).iter() {
                conn.execute(t, &[])?;
            }
            Ok(())
        }
    }

    impl Header for ExtTable {
        fn header(&self, conn: &mut Connection) -> Result<Vec<String>> {
            let mut header = self.static_header.clone();
            header.extend(MultiColumn::names(conn)?);
            Ok(header)
        }
    }

    struct ExecutorUpdater;

    impl UpdateStr<Stringinizer> for ExecutorUpdater {}

    impl UpdateInt<Stringinizer> for ExecutorUpdater {
        fn update_int(
            &mut self,
            id: RowId,
            _: i32,
            value: RowId,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<()> {
            let (old_exe_id, old_prio, status_flag) = check_status_flag(id.0, conn)?;

            if status_flag == 1 {
                if value.0 == old_exe_id {
                    return Err(Error::ModelError(CodeError::ValueNotCorrect));
                }
                if let Some((stmt, params)) = shift_performer_prio(old_exe_id, Some(old_prio), None)
                {
                    conn.execute(stmt, vec_params!(params))?;
                }
                let new_prio = max_prioriry_for_executor(value.0, conn)?;
                let stmt = "UPDATE task SET performer_id=?, prio=? WHERE id=?";
                conn.execute(stmt, params!(value.0, new_prio, id.0))?;
            } else {
                let stmt = "UPDATE task SET performer_id=? WHERE id=?";
                conn.execute(stmt, params!(value.0, id.0))?;
            }
            Ok(())
        }
    }

    struct StatusUpdater;

    impl UpdateStr<Stringinizer> for StatusUpdater {}

    impl UpdateInt<Stringinizer> for StatusUpdater {
        fn update_int(
            &mut self,
            id: RowId,
            _: i32,
            value: RowId,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<()> {
            let (exe_id, prio, old_status_flag) = check_status_flag(id.0, conn)?;
            let new_status_flag = status_prio_flag(value.0, conn)?;

            match new_status_flag.cmp(&old_status_flag) {
                Ordering::Less => {
                    if let Some((stmt, params)) = shift_performer_prio(exe_id, Some(prio), None) {
                        conn.execute(stmt, vec_params!(params))?;
                    }
                }
                Ordering::Greater => {
                    if let Some((stmt, params)) = shift_performer_prio(exe_id, None, Some(prio)) {
                        conn.execute(stmt, vec_params!(params))?;
                    }
                }
                Ordering::Equal => (),
            };

            let stmt = "UPDATE task SET status_id=? WHERE id=?";
            conn.execute(stmt, params!(value.0, id.0))?;

            Ok(())
        }
    }
}



mod permission_mask {
    use super::*;
    use task::Person;
    use tnsqlite3::{Connection, Identify, Insert, Mapped, Mapping};

    #[derive(Stringinize, Mapping, Identify, Debug)]
    #[stringinizer = "Stringinizer"]
    pub(crate) struct PermMask {
        #[stringinize_ignore]
        perm: Option< i64>,
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        name: String,
        #[mapped_length = 3]
        owner: Person,
        group: String,
    }

    impl Table<PermMask, PermMaskGuard, PermMaskQuery> {
        pub(crate) fn perm_mask() -> Self {
            let columns = &["name", "owner", "group"];
            let fetch_stmt = "SELECT  permission.id,  perm_mask.id, perm_mask.name,
                owner.fname, owner.lname, owner.mname, gr.name
                FROM perm_mask
                INNER JOIN person AS owner ON owner.id = perm_mask.owner_id
                INNER JOIN group_ AS gr ON gr.id = perm_mask.group_id
                OUTER LEFT JOIN
                    (WITH RECURSIVE
                    cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
                    UNION SELECT gr_1 FROM  person WHERE id=?
                    UNION SELECT gr_2 FROM  person WHERE id=?
                    UNION SELECT group_.id FROM cgroups, group_
                    WHERE group_.parent=cgroups.id )
                    SELECT id
                        FROM perm_mask AS pm
                        WHERE pm.group_id in cgroups) as permission ON permission.id = perm_mask.id
                WHERE perm_mask.id in "
                .to_string();
            let find_stmt = "SELECT perm_mask.id
                FROM perm_mask
                INNER JOIN person AS owner ON owner.id = perm_mask.owner_id
                INNER JOIN group_ AS gr ON gr.id = perm_mask.group_id "
                .to_string();
            let up_block: Vec<Column> = vec![
                Box::new(StringColumn::new("perm_mask", "name")),
                Box::new(UnchangeableColumn),
                Box::new(OneToManyColumn::new(
                    DefComplexUpdater::<WithoutNullUp>::new("perm_mask", "group_id"),
                    OTMView::<SingleColumnRow, WithoutNullVar, permission::PersonGroupVariant>::new(
                        String::new(),
                    ),
                )),
            ];
            let header = vec!["Name".into(), "Owner".into(), "Group".into()];
            Self::new(
                "perm_mask",
                fetch_stmt,
                find_stmt,
                up_block,
                header,
                columns,
                PermMaskGuard,
                PermMaskQuery,
            )
        }
    }


    pub(crate) struct PermMaskQuery;

    impl<G> SqlApply<Table<PermMask, G, PermMaskQuery>> for PermMaskQuery {
        fn sql_apply<'a>(
            src: &Table<PermMask, G, PermMaskQuery>,
            ids: Vec<RowId>,
            ctx: &Context,
            conn: &'a mut Connection,
        ) -> result::Result<PStatement<'a>, tnsqlite3::Error> {
            let usr_id = ctx.person_id;
            let stmt = fetch_stmt(&src.fetch_stmt, &ids);
            conn.prepare(&stmt, params!(usr_id, usr_id, usr_id))
        }
    }

    impl GetRowPermission for PermMask {
        fn get_row_permission(&self, _ctx: &Context) -> &[u8] {
            dbg!(self);
            if self.perm.is_some() {
                &[Permission::RW as u8; PermMask::COLUMN_KIND.len()]
            } else {
                &[Permission::RO as u8; PermMask::COLUMN_KIND.len()]
            }
        }
    }

    //        impl_fetch_data!(Table<PermMask, G, PermMaskQuery>, PermMask);

    pub(crate) struct PermMaskGuard;

    #[cfg(not(feature = "column_permission_disable"))]
    impl WriteAccess for PermMaskGuard {
        fn write_access_exists(
            &self,
            rowid: i64,
            _column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<bool> {
            let stmt = "WITH RECURSIVE
            cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
            UNION SELECT gr_1 FROM  person WHERE id=?
            UNION SELECT gr_2 FROM  person WHERE id=?
            UNION SELECT group_.id FROM cgroups, group_
            WHERE group_.parent=cgroups.id )
            SELECT count(*)
            FROM perm_mask
            WHERE  group_id in cgroups AND  id = ? ";
            let count = conn
                .prepare(
                    stmt,
                    params!(ctx.person_id, ctx.person_id, ctx.person_id, rowid),
                )?
                .query(select_tuple!(i64))?
                .next()
                .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??;
            Ok(count != 0)
        }
    }

    #[cfg(feature = "column_permission_disable")]
    impl WriteAccess for PermMaskGuard {
        fn write_access_exists(
            &self,
            rowid: i64,
            column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<bool> {
            Ok(true)
        }
    }
}

mod permission {
    use super::*;
    use crate::abs::*;
    use crate::stypes::RowId;
    pub(crate) use crate::table::ExtTable;
    use crate::task::MultiColumn;
    use tnsqlite3::{Connection, Identify, Mapped, Mapping};

    #[derive(Mapping, Identify, Debug)]
    struct PermRow {
        permvec: Option<i64>,
        #[row_id]
        id: i64,
        group: String,
        perm: Vec<u8>,
    }

    //    const PERM_COLUMN_OFFSET: i32 = 1;




    pub(crate) struct PermGroupGuard;

    #[cfg(not(feature = "column_permission_disable"))]
    impl WriteAccess for PermGroupGuard {
        fn write_access_exists(
            &self,
            rowid: i64,
            _column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<bool> {
            let stmt = "WITH RECURSIVE
                        cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
                          UNION SELECT gr_1 FROM  person WHERE id=?
                          UNION SELECT gr_2 FROM  person WHERE id=?
                         UNION SELECT group_.id FROM cgroups, group_
                         WHERE group_.parent=cgroups.id )
                         SELECT count(*)
                         FROM perm_mask AS pm
                         INNER JOIN perm_group AS pg ON pg.pmask_id = pm.id
                         WHERE pg.id=?
                         AND pm.group_id in cgroups";
            let count = conn
                .prepare(
                    stmt,
                    params!(ctx.person_id, ctx.person_id, ctx.person_id, rowid),
                )?
                .query(select_tuple!(i64))?
                .next()
                .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??;
            Ok(count != 0)
        }
    }

    #[cfg(feature = "column_permission_disable")]
    impl WriteAccess for PermGroupGuard {
        fn write_access_exists(
            &self,
            rowid: i64,
            column: i32,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<bool> {
            Ok(true)
        }
    }

    pub(crate) struct PermUpdater;

    impl FetchData for PermUpdater {
        type Stringinizer = Stringinizer;
        fn fetch_data(
            &mut self,
            _ids: Vec<RowId>,
            _s: &Stringinizer,
            ctx: &Context,
            _conn: &mut Connection,
        ) -> Result<Vec<Row>> {
            use abs::Permission::*;
            Ok(vec![
                Row {
                    id: RowId(RO as i64),
                    columns: vec![RO.to_string()],
                    meta: Meta::default_from_num(1),
                },
                Row {
                    id: RowId(RW as i64),
                    columns: vec![RW.to_string()],
                    meta: Meta::default_from_num(1),
                },
            ])
        }
    }

    impl ColumnVariants<Stringinizer> for PermUpdater {
        fn column_variants(
            &mut self,
            _column: i32,
            s: &Stringinizer,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<Vec<Row>> {
            self.fetch_data(Vec::new(), s, ctx, conn)
        }
    }

    impl UpdateInt<Stringinizer> for PermUpdater {
        fn update_int(
            &mut self,
            id: RowId,
            column: i32,
            value: RowId,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<()> {
            let perm = Permission::try_from(value)?;
            let row_task_len = task::MultiColumn::count(conn)? + STATIC_FIELDS;
            let mut permvec = conn
                .prepare("SELECT perm FROM perm_group WHERE id=?", params!(id.0))?
                .query(select_tuple!(Vec<u8>))?
                .next()
                .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??;
            dbg!(column, row_task_len);
            if column >= row_task_len {
                return Err(Error::ModelError(CodeError::ValueNotCorrect));
            }
            dbg!("");
            permvec[(column) as usize] = perm as u8;
            let stmt = "UPDATE perm_group SET perm=? WHERE id=?";
            conn.execute(&stmt, params!(&permvec, id.0))?;
            Ok(())
        }
    }
    impl UpdateStr<Stringinizer> for PermUpdater {}

    impl GetColumnType<Stringinizer> for PermUpdater {
        fn get_column_type(
            &mut self,
            id: RowId,
            column: i32,
            ctx: &Context,
            _: &mut Connection,
        ) -> Result<ColumnType> {
            Ok(ColumnType::Complex)
        }
    }




    const CHILDREN_GROUPS_STMT: &str = "WITH RECURSIVE
            cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
                UNION SELECT gr_1 FROM  person WHERE id=?
                UNION SELECT gr_2 FROM  person WHERE id=?
               UNION SELECT group_.id FROM cgroups, group_
               WHERE group_.parent=cgroups.id )
               SELECT id, name FROM group_ WHERE id in cgroups";

    pub(crate) struct PersonGroupVariant;

    impl<V, N> SqlApply<OTMView<V, N, PersonGroupVariant>> for PersonGroupVariant {
        fn sql_apply<'a>(
            _src: &OTMView<V, N, PersonGroupVariant>,
            _ids: Vec<RowId>,
            ctx: &Context,
            conn: &'a mut Connection,
        ) -> result::Result<PStatement<'a>, tnsqlite3::Error> {
            conn.prepare(
                CHILDREN_GROUPS_STMT,
                params!(ctx.person_id, ctx.person_id, ctx.person_id),
            )
        }
    }

    impl ExtTable<PermGroupView, PermUpdater, typenum::U1, PermGroupGuard> {
        pub(crate) fn permission_group() -> Self {
            let static_header = &[
                "Group",
                "Owner",
                "Executor",
                "Status",
                "Start time",
                "Deadline",
                "Priority",
                "Access mask",
                "Creation time",
                "Group",
            ];
            let up_block = arr![Column; Box::new(UnchangeableColumn) ];

            ExtTable {
                static_header,
                up_block,
                ext_up_block: PermUpdater,
                guard: PermGroupGuard,
                view: PhantomData,
            }
        }
    }

    impl Header for PermUpdater {
        fn header(&self, conn: &mut Connection) -> Result<Vec<String>> {
            Ok(MultiColumn::names(conn)?)
        }
    }

    impl AddColumns for PermUpdater {}
    impl DeleteColumn for PermUpdater {}

    pub(crate) struct PermGroupView;

    impl<M, N, G> Find for ExtTable<PermGroupView, M, N, G>
    where
        N: ArrayLength<Column>,
    {
        fn find(&mut self, finder: &Finder, conn: &mut Connection) -> Result<Vec<RowId>> {
            let (where_clause, params) = finder.into_params("perm_group", &[])?;
            let stmt = "SELECT id
            FROM perm_group "
                .to_string()
                + &where_clause;
            if finder.0.len() != 1 {
                return Err(Error::ModelError(CodeError::ValueNotCorrect));
            }
            Ok(conn
                .prepare(&stmt, &params)?
                .query(RowId::mapping)?
                .fallible_collect()?)
        }
    }

    impl<M, N, G> FetchData for ExtTable<PermGroupView, M, N, G>
    where
        N: ArrayLength<Column>,
    {
        type Stringinizer = Stringinizer;
        fn fetch_data(
            &mut self,
            ids: Vec<RowId>,
            _s: &Stringinizer,
            ctx: &Context,
            conn: &mut Connection,
        ) -> Result<Vec<Row>> {
            let row_len = (MultiColumn::count(conn)? + STATIC_FIELDS) as usize;

            let mut res = Vec::new();
            let stmt = format!(
        "
        SELECT permission.id,  pg.id,  gr.name,  pg.perm
            FROM perm_group AS pg
            INNER JOIN group_ AS gr ON pg.group_id = gr.id
            OUTER LEFT JOIN
                (WITH RECURSIVE
                cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
                UNION SELECT gr_1 FROM  person WHERE id=?
                UNION SELECT gr_2 FROM  person WHERE id=?
                UNION SELECT group_.id FROM cgroups, group_
                WHERE group_.parent=cgroups.id )
                SELECT id
                    FROM perm_mask AS pm
                    WHERE pm.group_id in cgroups) as permission ON permission.id = pg.pmask_id

            WHERE pg.id in ({})",
                &gen_ids_str(&ids)
            );
            let usr_id = ctx.person_id;
            let p_stmt = conn.prepare(&stmt, params!(usr_id, usr_id, usr_id))?;

            for r in p_stmt.query(PermRow::mapping)? {
                let target = r?;
                let mut columns = Vec::with_capacity(row_len + 1);
                columns.push(target.group);
                target
                    .perm
                    .into_iter()
                    .take(row_len)
                    .try_for_each(|b| -> Result<()> {
                        Ok(columns.push(Permission::from(b).to_string()))
                    })?;

                let metacolumn: Vec<_> = [ColumnKind::Text]
                    .iter()
                    .cloned()
                    .cycle()
                    .zip(
                        [Permission::RO].iter().cloned().chain(
                            (if target.permvec.is_some() {
                                [Permission::RW]
                            } else {
                                [Permission::RO]
                            })
                            .iter()
                            .cloned()
                            .cycle()
                            .take(row_len)
                        ),
                    )
                    .map(|(k, p)| MetaColumn {
                        permission: p,
                        kind: k,
                    })
                    .collect();

                    let meta = Meta {
                        h: MetaRow::default(),
                        c: metacolumn,
                        s: None,
                    };
                res.push(Row {
                    id: RowId(target.id),
                    columns,
                    meta,
                });
            }
            Ok(res)
        }
    }

}

pub(crate) mod log {

    use crate::abs::*;
    use crate::stypes::RowId;
    use crate::{
        fetch_stmt, task::Person, Column, DateTime, PermissiveGuard, Stringinize, Stringinizer,
        Table, UnchangeableColumn, WithoutParams,
    };
    use tnsqlite3::{Connection, Identify, Mapped, Mapping};

    #[derive(Stringinize, Mapping, Identify)]
    #[stringinizer = "Stringinizer"]
    pub(crate) struct Log {
        #[stringinize_ignore]
        #[row_id]
        id: i64,
        timestamp: DateTime,
        #[mapped_length = 3]
        person: Person,
        column: String,
        data: String,
    }

    impl GetRowPermission for Log {
        fn get_row_permission(&self, ctx: &Context) -> &[u8] {
            &[Permission::RO as u8; Log::COLUMN_KIND.len()]
        }
    }

    impl Table<Log, PermissiveGuard, WithoutParams> {
        pub(crate) fn log() -> Self {
            let columns = &["timestamp", "person_id", "column", "data"];
            let fetch_stmt = "SELECT log.id, timestamp, p.fname, p.lname, p.mname, column, data
            FROM  log
            INNER JOIN person AS p ON p.id = log.person_id
            WHERE log.id in "
                .to_string();
            let find_stmt = "SELECT log.id
             FROM  log
             INNER JOIN person AS p ON p.id = log.person_id"
                .to_string();
            let up_block: Vec<Column> = vec![
                Box::new(UnchangeableColumn),
                Box::new(UnchangeableColumn),
                Box::new(UnchangeableColumn),
                Box::new(UnchangeableColumn),
            ];

            let header = ["Time", "Person", "Column", "Content"]
                .iter()
                .map(|&s| s.to_string())
                .collect();

            Self::new(
                "log",
                fetch_stmt,
                find_stmt,
                up_block,
                header,
                columns,
                PermissiveGuard,
                WithoutParams,
            )
        }
    }

    //    impl_fetch_data!(Table<Log, G, WithoutParams>, Log);
}

fn gen_fetch_stmt(name: &str, columns: &[&str]) -> String {
    let columns = columns.join(", ");
    format!("SELECT id, {} FROM {} WHERE id in ", columns, name)
}

fn gen_ids_str(ids: &[RowId]) -> String {
    ids.iter()
        .map(|i| i.0.to_string())
        .collect::<Vec<_>>()
        .join(",")
}

fn fetch_stmt(base_stmt: &str, ids: &[RowId]) -> String {
    base_stmt.to_string() + &format!(" ({})", &gen_ids_str(ids))
}

fn gen_find_stmt(name: &str) -> String {
    format!("SELECT id FROM {}", name)
}

pub fn create_tables(conn: &mut Connection) -> Result<()> {
    for (_, table) in CREATE_DB_EXPR {
        conn.execute(table, &[])?;
    }
    for (_, trigger) in CREATE_TRIGGERS_STMT {
        conn.execute(trigger, &[])?;
    }
    //    for (_, text_trigger) in CREATE_TEXT_TRIGGERS_STMT {
    //        conn.execute(text_trigger, &[])?;
    //    }
    Ok(())
}

impl<F: FilterKit> TryClone<Self, Error> for TNModel<F> {
    fn try_clone(&self) -> Result<Self> {
        TNModel::<F>::new(&self.db_file, self.gen_db_conn)
    }
}

pub struct TNModel<F: FilterKit> {
    con_man: ContexManager<F>,
    context: pin::Pin<Box<Context>>,
    conn: Connection,
    gen_db_conn: fn(&str) -> result::Result<Connection, tnsqlite3::Error>,
    tables: Vec<View>,
    st_er: Stringinizer,
    db_file: String,
}

fn debug_request(m: &Method) {
    println!("{:?}", m);
}

impl<F> TNModel<F>
where
    F: FilterKit,
{
    pub fn new(
        db_file: &str,
        gen_db_conn: fn(&str) -> result::Result<Connection, tnsqlite3::Error>,
    ) -> Result<Self> {
        let tables: Vec<View> = vec![
            Box::new(Table::group()),
            Box::new(Table::person()),
            Box::new(Table::status()),
            Box::new(task::ExtTable::new()),
            Box::new(Table::perm_mask()),
            Box::new(permission::ExtTable::permission_group()),
            Box::new(Table::log()),
        ];
        let mut context = Box::pin(Context::new());
        let mut conn = gen_db_conn(db_file)?;
        let ctx = pin::Pin::get_mut(context.as_mut());
        conn.create_function(
            "getowner",
            0,
            getowner,
            tnsqlite3::FunctionFlag::NonFlag,
            ctx as *mut Context as *mut raw::c_void,
        )?;

        Ok(TNModel {
            con_man: ContexManager::<F>::new(),
            context,
            conn,
            gen_db_conn,
            tables,
            st_er: Stringinizer::new(),
            db_file: db_file.to_string(),
        })
    }

    pub fn with_context(
        db_file: &str,
        gen_db_conn: fn(&str) -> result::Result<Connection, tnsqlite3::Error>,
        mut context: Context,
    ) -> Result<Self> {
        let mut model = TNModel::new(db_file, gen_db_conn)?;
        let mut context = Box::pin(context);
        let conn = model.db_connection();
        let ctx = pin::Pin::get_mut(context.as_mut());
        conn.create_function(
            "getowner",
            0,
            getowner,
            tnsqlite3::FunctionFlag::NonFlag,
            ctx as *mut Context as *mut raw::c_void,
        )?;

        model.context = context;
        Ok(model)
    }

    fn request_inner(&mut self, method: Method) -> Result<Payload> {
        debug_request(&method);

        use Method::*;
        match method {
            find { params, .. } => self.find(params.0, params.1),
            fetch_data { params, .. } => self.fetch_data(params.0, params.1),
            header { params, .. } => self.header(params.0),
            column_type { params, .. } => self.column_type(params.0, params.1, params.2),
            update_str { params, .. } => self.update_str(params.0, params.1, params.2, params.3),
            update_int { params, .. } => self.update_int(params.0, params.1, params.2, params.3),
            column_variants { params, .. } => self.column_variants(params.0, params.1),
            add_columns { params, .. } => self.add_columns(params.0, params.1),
            delete_column { params, .. } => self.delete_column(params.0, params.1),
            add_group { params, .. } => self.add_group(params.0, params.1),
            add_status { params, .. } => self.add_status(params.0, params.1),
            add_perm_mask { params, .. } => self.add_perm_mask(params.0, params.1),
            add_perm_group { params, .. } => self.add_perm_group(params.0, params.1),
            add_person { params, .. } => {
                self.add_person(params.0, params.1, params.2, params.3, params.4, params.5)
            }
            add_task { params, .. } => {
                self.add_task(params.0, params.1, params.2, params.3, params.4, params.5)
            }
            prio_for_executor { params, .. } => self.prio_for_executor(params.0),
            update_prio { params, .. } => self.update_prio(params.0, params.1),
            authenticate { params, .. } => self.authenticate(params.0, params.1),
            change_password { params, .. } => self.change_password(params.0, params.1, params.2),
            set_password { params, .. } => self.set_password(params.0, params.1),
            admin_count { .. } => self.admin_count(),
        }
    }

    fn find(&mut self, kind: TableKind, cond: Finder) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        Ok(Payload::Finded(table.find(&cond, &mut self.conn)?))
    }

    fn fetch_data(&mut self, kind: TableKind, ids: Vec<RowId>) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        Ok(Payload::Rows(table.fetch_data(
            ids,
            &self.st_er,
            &self.context,
            &mut self.conn,
        )?))
    }

    fn header(&mut self, kind: TableKind) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        Ok(Payload::Header(table.header(&mut self.conn)?))
    }
    fn column_type(&mut self, kind: TableKind, id: RowId, column: i32) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        Ok(Payload::ColumnType(table.get_column_type(
            id,
            column,
            &self.context,
            &mut self.conn,
        )?))
    }
    fn update_str(
        &mut self,
        kind: TableKind,
        id: RowId,
        column: i32,
        value: String,
    ) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        table.update_str(id, column, &value, &self.context, &mut self.conn)?;
        Ok(Payload::Ok)
    }
    fn update_int(
        &mut self,
        kind: TableKind,
        id: RowId,
        column: i32,
        value: RowId,
    ) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        table.update_int(id, column, value, &self.context, &mut self.conn)?;
        Ok(Payload::Ok)
    }

    fn column_variants(&mut self, kind: TableKind, num: i32) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        Ok(Payload::Rows(table.column_variants(
            num,
            &self.st_er,
            &self.context,
            &mut self.conn,
        )?))
    }
    fn add_columns(&mut self, kind: TableKind, columns: Vec<String>) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        table.add_columns(columns, &mut self.conn)?;
        Ok(Payload::Ok)
    }
    fn delete_column(&mut self, kind: TableKind, idx: i32) -> Result<Payload> {
        let table = &mut self.tables[kind as usize];
        table.delete_column(idx, &mut self.conn)?;
        Ok(Payload::Ok)
    }

    fn add_group(&mut self, name: String, parent: RowId) -> Result<Payload> {
        let parent = if parent.0 == 0 { None } else { Some(parent.0) };
        let group = Group {
            id: 0,
            name,
            parent,
        };
        Ok(Payload::Id(RowId(group.insert(&self.conn)?)))
    }
    fn add_status(&mut self, name: String, prio_flag: i32) -> Result<Payload> {
        let status = Status {
            id: 0,
            name,
            prio_flag: prio_flag as i64,
        };
        Ok(Payload::Id(RowId(status.insert(&self.conn)?)))
    }

    fn add_perm_mask(&mut self, name: String, group: RowId) -> Result<Payload> {
        let mask = PermMask {
            id: 0,
            name,
            owner_id: self.context.person_id,
            group_id: group.0,
        };
        Ok(Payload::Id(RowId(mask.insert(&self.conn)?)))
    }

    fn add_perm_group(&mut self, pmask: RowId, group: RowId) -> Result<Payload> {
        let column_num = task::MultiColumn::count(&mut self.conn)?;
        let mut perm = vec![Permission::default() as u8; (STATIC_FIELDS - STATIC_FIELDS_RO) as usize];
        perm.extend(&vec![Permission::RO as u8; STATIC_FIELDS_RO as usize]);
        perm.extend(&vec![Permission::default() as u8; column_num as usize]);
        let pg = PermGroup {
            id: 0,
            pmask_id: pmask.0,
            group_id: group.0,
            perm,
        };
        Ok(Payload::Id(RowId(pg.insert(&self.conn)?)))
    }

    fn add_person(
        &mut self,
        login: String,
        fname: String,
        lname: String,
        mname: String,
        gr_base: RowId,
        perm: RowId,
    ) -> Result<Payload> {
        if PersonPermission::try_from(perm).is_err() {
            return Err(Error::ModelError(CodeError::ValueNotCorrect));
        }
        let person = Person {
            id: 0,
            login,
            pass_hashsum: None,
            fname,
            lname,
            mname,
            gr_base: gr_base.0,
            gr_1: None,
            gr_2: None,
            perm: perm.0,
        };

        Ok(Payload::Id(RowId(person.insert(&self.conn)?)))
    }

    fn add_task(
        &mut self,
        performer_id: RowId,
        status_id: RowId,
        start_time: RowId,
        deadline: RowId,
        prio: i32,
        pmask: RowId,
    ) -> Result<Payload> {
        let ts = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("SystemTime before UNIX EPOCH!")
            .as_secs() as i64;
        let task = Task {
            id: 0,
            owner_id: self.context.person_id,
            performer_id: performer_id.0,
            status_id: status_id.0,
            start_time: start_time.0,
            create_time: ts,
            deadline: deadline.0,
            prio: prio as i64,
            pmask_id: pmask.0,
        };

        let max_prio = max_prioriry_for_executor(performer_id.0, &mut self.conn)?;

        if status_prio_flag(status_id.0, &mut self.conn)? != 0 {
            if prio > max_prio as i32 {
                return Err(Error::ModelError(CodeError::ValueNotCorrect));
            }

            if prio < max_prio as i32 {
                if let Some((stmt, params)) =
                    shift_performer_prio(performer_id.0, None, Some(prio as i64))
                {
                    self.conn.execute(stmt, vec_params!(params))?;
                }
            }
        }

        Ok(Payload::Id(RowId(task.insert(&self.conn)?)))
    }

    fn prio_for_executor(&mut self, exe_id: RowId) -> Result<Payload> {
        let max_prio = max_prioriry_for_executor(exe_id.0, &mut self.conn)?;
        Ok(Payload::Priority((0..max_prio as i32).collect()))
    }

    fn update_prio(&mut self, task_id: RowId, prio: i32) -> Result<Payload> {
        let (exe_id, old_prio, status_flag) = check_status_flag(task_id.0, &mut self.conn)?;

        if status_flag != 1 {
            return Err(Error::ModelError(CodeError::ValueNotCorrect));
        }

        let max_prio = max_prioriry_for_executor(exe_id, &mut self.conn)?;

        if prio > max_prio as i32 {
            return Err(Error::ModelError(CodeError::ValueNotCorrect));
        }
        if prio == max_prio as i32 {
            return Ok(Payload::Ok);
        }
        if let Some((stmt, params)) =
            shift_performer_prio(exe_id, Some(old_prio), Some(prio as i64))
        {
            self.conn.execute(stmt, vec_params!(params))?;
        }
        let stmt = "UPDATE task SET prio=? WHERE id=?";
        self.conn.execute(stmt, params!(prio, task_id.0))?;
        Ok(Payload::Ok)
    }

    fn authenticate(&mut self, login: String, hashsum: String) -> Result<Payload> {
        let (id, perm) = self
            .conn
            .prepare(
                "SELECT id, perm FROM person WHERE login=? AND pass_hashsum=?",
                params!(&login, &hashsum),
            )?
            .query(select_tuple!(i64, i64))?
            .next()
            .ok_or(Error::ModelError(CodeError::AuthenticationFailed))??;
        Ok(Payload::Permission((RowId(id), RowId(perm))))
    }

    fn change_password(
        &mut self,
        login: String,
        old_hashsum: String,
        new_hashsum: String,
    ) -> Result<Payload> {
        self.authenticate(login.clone(), old_hashsum)?;
        self.set_password(login, new_hashsum)
    }

    fn set_password(&mut self, login: String, hashsum: String) -> Result<Payload> {
        let count = self
            .conn
            .prepare("SELECT count(*) FROM person WHERE login=?", params!(&login))?
            .query(select_tuple!(i64))?
            .next()
            .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??;
        if count != 1 {
            return Err(Error::ModelError(CodeError::ValueNotCorrect));
        }

        let stmt = "UPDATE person SET pass_hashsum=? WHERE login=?";
        self.conn.execute(stmt, params!(&hashsum, &login))?;
        Ok(Payload::Ok)
    }

    fn admin_count(&mut self) -> Result<Payload> {
        let stmt = "SELECT count(*) FROM person WHERE (perm & ?) = ?";
        let count = self
            .conn
            .prepare(
                stmt,
                params!(
                    PermLevel::Administration as i64,
                    PermLevel::Administration as i64
                ),
            )?
            .query(select_tuple!(i64))?
            .next()
            .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??;
        Ok(Payload::Id(RowId(count)))
    }
}

fn check_status_flag(task_id: i64, conn: &mut Connection) -> Result<(i64, i64, i64)> {
    let stmt = "SELECT task.performer_id, task.prio, s.prio_flag FROM status AS s
                INNER JOIN task ON  s.id = task.status_id
                WHERE task.id = ?";
    Ok(conn
        .prepare(stmt, params!(task_id))?
        .query(select_tuple!(i64, i64, i64))?
        .next()
        .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??)
}

fn status_prio_flag(status_id: i64, conn: &mut Connection) -> Result<i64> {
    let stmt = "SELECT prio_flag FROM status WHERE id=?";
    Ok(conn
        .prepare(stmt, params!(status_id))?
        .query(select_tuple!(i64))?
        .next()
        .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??)
}

fn max_prioriry_for_executor(exe_id: i64, conn: &mut Connection) -> Result<i64> {
    let stmt = "SELECT MAX(task.prio) FROM task
                INNER JOIN status AS s ON s.id = task.status_id
                WHERE  (s.prio_flag = 1) AND performer_id = ?";

    let mut query = conn
        .prepare(stmt, params!(exe_id))?
        .query(select_tuple!(Option<i64>))?;
    Ok(
        if let Some(prio) = query
            .next()
            .ok_or(Error::ModelError(CodeError::ValueNotCorrect))??
        {
            prio + 1
        } else {
            0
        },
    )
}

fn shift_performer_prio(
    exe_id: i64,
    old_prio: Option<i64>,
    new_prio: Option<i64>,
) -> Option<(&'static str, Vec<i64>)> {
    match (old_prio, new_prio) {
        (Some(old_prio), Some(new_prio)) => match new_prio.cmp(&old_prio) {
            Ordering::Less => Some((
                "UPDATE task  SET prio = prio + 1
            WHERE id IN (
            SELECT task.id AS id FROM task
            INNER JOIN status AS s ON s.id = task.status_id
            WHERE s.prio_flag = 1 AND performer_id = ? AND
            prio < ? AND prio >= ?)",
                vec![exe_id, old_prio, new_prio],
            )),
            Ordering::Greater => Some((
                "UPDATE task  SET prio = prio - 1
            WHERE id IN (
            SELECT task.id AS id FROM task
            INNER JOIN status AS s ON s.id = task.status_id
            WHERE s.prio_flag = 1 AND performer_id = ? AND
            prio > ? AND prio <= ?)",
                vec![exe_id, old_prio, new_prio],
            )),
            Ordering::Equal => None,
        },
        (Some(old_prio), None) => Some((
            "UPDATE task  SET prio = prio - 1
            WHERE id IN (
            SELECT task.id AS id FROM task
            INNER JOIN status AS s ON s.id = task.status_id
            WHERE s.prio_flag = 1 AND performer_id = ? AND
            prio > ?)",
            vec![exe_id, old_prio],
        )),
        (None, Some(new_prio)) => Some((
            "UPDATE task  SET prio = prio + 1
            WHERE id IN (
            SELECT task.id AS id FROM task
            INNER JOIN status AS s ON s.id = task.status_id
            WHERE s.prio_flag = 1 AND performer_id = ? AND
            prio >= ?)",
            vec![exe_id, new_prio],
        )),
        (None, None) => None,
    }
}

impl<F: FilterKit> Model for TNModel<F> {
    type Connection = tnsqlite3::Connection;
    type Output = OutObject;
    type Proc = Method;

    fn request(&mut self, method: Self::Proc) -> Result<Self::Output> {
        let id = id_method(&method).to_string();
        if !self.con_man.preprocess(&mut self.context, &method) {
            println!("FiltredError>>>>>>>>>>>>>>>>>>>.");
            return Ok(OutObject::Error(ErrObject::new(
                CodeError::FiltredError,
                "".to_string(),
                id,
            )));
        }
        let reply = self.request_inner(method);
        self.con_man
            .postprocess(&mut self.context, &reply, &mut self.conn);

        match reply {
            Ok(r) => {
                println!("{:?}", &r);
                Ok(OutObject::Response(Response::new(r, id)))
            }
            Err(Error::ModelError(e)) => {
                println!("{:?}", &e);
                Ok(OutObject::Error(ErrObject::new(e, "".to_string(), id)))
            }
            Err(e) => Err(e),
        }
    }
    fn init_db(&mut self) -> Result<()> {
        //        self.create_tables()
        Ok(())
    }
    fn db_connection(&mut self) -> &mut Self::Connection {
        &mut self.conn
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "name")]
#[allow(non_camel_case_types)]
pub enum Method {
    find {
        jsonrpc: String,
        id: String,
        params: (TableKind, Finder),
    },
    fetch_data {
        jsonrpc: String,
        id: String,
        params: (TableKind, Vec<RowId>),
    },
    header {
        jsonrpc: String,
        id: String,
        params: (TableKind,),
    },
    column_type {
        jsonrpc: String,
        id: String,
        params: (TableKind, RowId, i32),
    },
    update_str {
        jsonrpc: String,
        id: String,
        params: (TableKind, RowId, i32, String),
    },
    update_int {
        jsonrpc: String,
        id: String,
        params: (TableKind, RowId, i32, RowId),
    },
    column_variants {
        jsonrpc: String,
        id: String,
        params: (TableKind, i32),
    },
    add_columns {
        jsonrpc: String,
        id: String,
        params: (TableKind, Vec<String>),
    },
    delete_column {
        jsonrpc: String,
        id: String,
        params: (TableKind, i32),
    },
    add_group {
        jsonrpc: String,
        id: String,
        params: (String, RowId),
    },

    add_status {
        jsonrpc: String,
        id: String,
        params: (String, i32),
    },
    add_perm_mask {
        jsonrpc: String,
        id: String,
        params: (String, RowId),
    },
    add_perm_group {
        jsonrpc: String,
        id: String,
        params: (RowId, RowId),
    },
    add_person {
        jsonrpc: String,
        id: String,
        params: (String, String, String, String, RowId, RowId),
    },
    add_task {
        jsonrpc: String,
        id: String,
        params: (RowId, RowId, RowId, RowId, i32, RowId),
    },
    prio_for_executor {
        jsonrpc: String,
        id: String,
        params: (RowId,),
    },

    update_prio {
        jsonrpc: String,
        id: String,
        params: (RowId, i32),
    },

    authenticate {
        jsonrpc: String,
        id: String,
        params: (String, String),
    },
    change_password {
        jsonrpc: String,
        id: String,
        params: (String, String, String),
    },
    set_password {
        jsonrpc: String,
        id: String,
        params: (String, String),
    },
    admin_count {
        jsonrpc: String,
        id: String,
        #[serde(skip)]
        params: (),
    },
}

macro_rules! variant_method_to_id {
    ($m:expr, $($var:tt),*) => {
     match $m {
        $( Method::$var {id, ..} => id, )*
         }
    }
}

fn id_method(method: &Method) -> &str {
    variant_method_to_id!(
        method,
        find,
        fetch_data,
        header,
        column_type,
        update_str,
        update_int,
        column_variants,
        add_columns,
        delete_column,
        add_group,
        add_status,
        add_perm_mask,
        add_perm_group,
        add_person,
        add_task,
        prio_for_executor,
        update_prio,
        authenticate,
        change_password,
        set_password,
        admin_count
    )
}

#[macro_export]
macro_rules! method {
    ($name:tt, $params:expr ) => {
        Method::$name {
            jsonrpc: JSON_RPC_VER.to_string(),
            id: "43".to_string(),
            params: $params,
        }
    };
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum TableKind {
    Group,
    Person,
    Status,
    Task,
    PermMask,
    PermGroup,
    Log,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum Payload {
    Rows(Vec<Row>),
    Header(Vec<String>),
    Id(RowId),
    ColumnType(ColumnType),
    Finded(Vec<RowId>),
    Priority(Vec<i32>),
    Permission((RowId, RowId)),
    Ok,
}

#[serde(untagged)]
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum OutObject {
    Response(Response),
    Error(ErrObject),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Response {
    pub jsonrpc: String,
    pub result: Payload,
    pub id: String,
}

impl Response {
    pub fn new(data: Payload, id: String) -> Self {
        Response {
            jsonrpc: JSON_RPC_VER.to_string(),
            result: data,
            id,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ErrObject {
    pub jsonrpc: String,
    pub error: ComError,
    pub id: String,
}

impl ErrObject {
    fn new(code: CodeError, message: String, id: String) -> Self {
        let error = ComError { code, message };
        ErrObject {
            jsonrpc: JSON_RPC_VER.to_string(),
            error,
            id,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ComError {
    pub code: CodeError,
    message: String,
}

#[cfg(test)]
mod tests {

    use super::*;
    use serde_json;

    macro_rules! serdeser {
        ($obj:expr, $t:ty) => {
            let p = serde_json::to_string(&$obj).unwrap();
            println!("{:?}", &p);
            let res: $t = serde_json::from_str(&p).unwrap();
        };
    }

    macro_rules! insert {
        ($conn:ident, $($ent:expr),+) => {
        $($ent.insert(&mut $conn).unwrap();)+
        }
    }
    macro_rules! rowid_vec {
        [$($el:expr),+] => {
        vec![ $(RowId($el),)+]
        }
    }

    pub fn create_conn() -> result::Result<Connection, tnsqlite3::Error> {
       let mut conn =   Connection::new(":memory:")?;

       conn.register_cdatetime_func()?;
       create_tables(&mut conn).expect("create_tables");
       Ok(conn)
    }

    fn meta2permission_vec(m: &Meta) -> Vec<Permission> {
        m.c.iter().map(|c| c.permission ).collect()

    }

    #[test]
    fn PermMaskQuery_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);
    insert!( conn, group1, group2, user1, user2, pmask1, pmask2);
    let mut pmtable = Table::perm_mask();
    let rows = pmtable.fetch_data(rowid_vec![1,2], &ser, &ctx, &mut conn).expect("");
    assert_eq!(meta2permission_vec(&rows[0].meta), vec![RW, RW, RW]  );
    assert_eq!(meta2permission_vec(&rows[1].meta), vec![RO, RO, RO]  );
}


    #[test]
    fn PermGroupView_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);

    let pg1 = PermGroup::new(1, 1, vec![0, 0]);
    let pg2 = PermGroup::new(1, 2, vec![0, 0]);
    let pg3 = PermGroup::new(2, 2, vec![0, 0]);

    insert!( conn, group1, group2, user1, user2, pmask1, pmask2, pg1, pg2, pg3);
    let mut pgtable = permission::ExtTable::permission_group();
    let rows = pgtable.fetch_data(rowid_vec![1,2,3], &ser, &ctx, &mut conn).expect("");
    dbg!(&rows);
    let mut v0 = vec![RO];
      v0.extend(&  vec![RW; STATIC_FIELDS as usize] );
     let mut v2 = vec![RO];
      v2.extend(&  vec![RO; STATIC_FIELDS as usize] );
    assert_eq!(meta2permission_vec(&rows[0].meta), v0 );
    assert_eq!(meta2permission_vec(&rows[1].meta), v0);
    assert_eq!(meta2permission_vec(&rows[2].meta), v2 );
    }

    #[test]
    fn Group_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let ctx_admin = Context::from_permission(PersonPermission::from(Privileges::Administration ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);

    insert!( conn, group1, group2);
    let mut grtable = Table::group();
    let rows = grtable.fetch_data(rowid_vec![1,2], &ser, &ctx, &mut conn).expect("");
    assert_eq!(meta2permission_vec(&rows[0].meta), vec![RO;2] );
    assert_eq!(meta2permission_vec(&rows[1].meta), vec![RO;2] );

    let rows = grtable.fetch_data(rowid_vec![1,2], &ser, &ctx_admin, &mut conn).expect("");
    assert_eq!(meta2permission_vec(&rows[0].meta), vec![RW;2] );
    assert_eq!(meta2permission_vec(&rows[1].meta), vec![RW;2] );
    }

    #[test]
    fn PermMaskGuard_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);
    insert!( conn, group1, group2, user1, user2, pmask1, pmask2);

    assert!(permission_mask::PermMaskGuard.write_access_exists(
            1, 0, &ctx, &mut conn).expect("") );
    assert!(!permission_mask::PermMaskGuard.write_access_exists(
            2, 0, &ctx, &mut conn).expect("") );
    let group1_1 = Group::new("gr1_1", Some(1));
    let pmask1_1 = PermMask::new("pmask1_1", 1, 3);
     insert!( conn, group1_1, pmask1_1);
    assert!(permission_mask::PermMaskGuard.write_access_exists(
            3, 0, &ctx, &mut conn).expect("") );
    }

    #[test]
    fn PermGroupGuard_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let group1_1 = Group::new("gr1_1", Some(1));
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);
    let pg1 = PermGroup::new(1, 1, vec![0]);
    let pg2 = PermGroup::new(2, 2, vec![0]);
    let pg1_1 = PermGroup::new(1, 3, vec![0]);
    insert!( conn, group1, group2, group1_1,  user1, user2, pmask1, pmask2, pg1, pg2, pg1_1);

    let mut table = Table::perm_mask();
    let rows = table.column_variants(2, &ser, &ctx, &mut conn).expect("");
    assert_eq!(rows.len(), 2);
    assert_eq!(rows[0].id.0, 1);
    assert_eq!(rows[1].id.0, 3);
    }


    #[test]
    fn PersonGroupVariant_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);
    insert!( conn, group1, group2, user1, user2, pmask1, pmask2);

    assert!(permission_mask::PermMaskGuard.write_access_exists(
            1, 0, &ctx, &mut conn).expect("") );
    assert!(!permission_mask::PermMaskGuard.write_access_exists(
            2, 0, &ctx, &mut conn).expect("") );
    let group1_1 = Group::new("gr1_1", Some(1));
    let pmask1_1 = PermMask::new("pmask1_1", 1, 3);
     insert!( conn, group1_1, pmask1_1);
    assert!(permission_mask::PermMaskGuard.write_access_exists(
            3, 0, &ctx, &mut conn).expect("") );
    }


    #[test]
    fn TaskView_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let group1_1 = Group::new("gr1_1", Some(1));
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);
    let pmask3 = PermMask::new("pmask3", 1, 1);
    let pg1 = PermGroup::new(1, 1, vec![0]);
    let pg2 = PermGroup::new(2, 2, vec![0]);
    let pg1_1 = PermGroup::new(1, 3, vec![0]);
    let status1 = Status::new("stat1", 0);
    let task1 = Task::new(1, 1, 1, 0, 3);

    insert!( conn, group1, group2, group1_1,  user1, user2, pmask1, pmask2,pmask3,  pg1, pg2, pg1_1,
    status1, task1);

    let mut table = task::ExtTable::new();

    let rows = table.fetch_data(rowid_vec![1], &ser, &ctx, &mut conn).expect("");
    assert_eq!(rows.len(), 1);
    println!("{:?}", &rows);

    }

    #[test]
    fn TaskGuard_test() {
    use Permission::*;
    let mut conn = create_conn().expect("create_conn");
    let ser = Stringinizer::new();
    let ctx = Context::from_permission(PersonPermission::from(Privileges::Ordinary ));
    let group1 = Group::new("gr1", None);
    let group2 = Group::new("gr2", None);
    let group1_1 = Group::new("gr1_1", Some(1));
    let user1  = Person::new("user1", None, 1, 1);
    let user2 = Person::new("user2", None, 2, 1);
    let pmask1 = PermMask::new("pmask1", 1, 1);
    let pmask2 = PermMask::new("pmask2", 2, 2);
    let pmask3 = PermMask::new("pmask3", 1, 1);
    let pg1 = PermGroup::new(1, 1, vec![0]);
    let pg2 = PermGroup::new(2, 2, vec![0]);
    let pg1_1 = PermGroup::new(1, 3, vec![0]);
    let status1 = Status::new("stat1", 0);
    let task1 = Task::new(1, 1, 1, 0, 3);

    insert!( conn, group1, group2, group1_1,  user1, user2, pmask1, pmask2,pmask3,  pg1, pg2, pg1_1,
    status1, task1);

    assert!(!task::TaskGuard.write_access_exists(
            1, 0, &ctx, &mut conn).expect("") );

    }


    #[test]
    fn deser_method_rowid_test() {
        let m = Method::find {
            jsonrpc: JSON_RPC_VER.to_string(),
            id: "45".to_string(),
            params: (TableKind::Group, find!()),
        };
        serdeser!(m, Method);
    }

    #[test]
    fn deser_finder_rowid_test() {
        let f = find!((RowId(2),));
        let meth = method!(find, (TableKind::Group, f));
        serdeser!(meth, Method);

        let s = "{\"id\":\"d4cec5741b7abc0b\",\"jsonrpc\":\"2.0\",\"name\":\"find\",\"params\":[\"Group\",[{\"RowId\":[\"5\"]}]]}";
        let res: Method = serde_json::from_str(&s).unwrap();
        println!("{:?}", &res);
    }

    #[test]
    #[ignore]
    fn deser_outobject_test() {
        let f = find!((4, "hello from QT".to_string(), Condition::Equal,));
        println!("{:?}", &f);
        let pa_ser = "
            [\"Group\",[{\"Str\":[4,\"hello from QT\",\"Equal\"]},{\"EntId\":[2,[\"1\",\"2\",\"3\"]]}]]";

        let pa_de: Finder = serde_json::from_str(&pa_ser).unwrap();
        println!("{:?}", &pa_de);
    }

    #[test]
    #[ignore]
    fn method_deser_find_test() {
        let w = find!(
            (4, "def".to_string(), Condition::Equal),
            (2, vec![RowId(1), RowId(2), RowId(3)])
        );
        let meth = method!(find, (TableKind::Group, w));
        let m_str = serde_json::to_string(&meth).unwrap();
        let ss = r#"{"jsonrpc":"2.0","name":"find","params":["Group",[{"Str":[4,"def","Equal"]},{"EntId":[2,["1","2","3"]]}]],"id":"42"}"#;
        assert_eq!(&m_str, ss);
    }

    #[test]
    //    #[ignore]
    fn serialize_test() {
        let rows = vec![Row {
            id: RowId(1),
            columns: vec!["name".into(), "group".into()],
            meta: Meta::default_from_num(2),
        }];
        let pl = Payload::Rows(rows);
        let resp = Response::new(pl, "90".to_string());
        let out = OutObject::Response(resp);
        let s = serde_json::to_string(&out).unwrap();
        //    println!("{}", &s);
        let ref_str = r#"{"jsonrpc":"2.0","result":{"Rows":[{"id":"1","columns":["name","group"],"meta":{"h":1,"c":[1,1],"s":null}}]},"id":"90"}"#;
        assert_eq!(ref_str, &s);

        let pl = Payload::Id(RowId(44));
        let resp = Response::new(pl, "90".to_string());
        let out = OutObject::Response(resp);
        let s = serde_json::to_string(&out).unwrap();
        println!("{}", &s);
    }
}
