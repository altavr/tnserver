#[allow(dead_code)]


include!(concat!(env!("OUT_DIR"), "/triggers.rs"));

pub(crate) const CREATE_DB_EXPR: &[(&str, &str)] = &[
    (
        "task",
        "CREATE TABLE task (
           id               INTEGER PRIMARY KEY,
           owner_id           INTEGER NOT NULL,
           performer_id     INTEGER NOT NULL,
           status_id        INTEGER NOT NULL,
           start_time       INTEGER NOT NULL,
           create_time       INTEGER NOT NULL,
           deadline         INTEGER NOT NULL,
           prio             INTEGER NOT NULL,
           pmask_id        INTEGER NOT NULL,
           FOREIGN KEY (owner_id) REFERENCES person(id),
           FOREIGN KEY (performer_id) REFERENCES person(id),
           FOREIGN KEY (status_id) REFERENCES status(id),
           FOREIGN KEY (pmask_id) REFERENCES perm_mask(id))",
    ),
    (
        "status",
        "CREATE TABLE status (
            id  INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            prio_flag   INTEGER NOT NULL)",
    ),
    (
        "group",
        "CREATE TABLE group_ (
                 id              INTEGER PRIMARY KEY,
                 name            TEXT NOT NULL,
                 parent             TEXT,
                 FOREIGN KEY (parent) REFERENCES group_(id))",
    ),
    (
        "person",
        "CREATE TABLE person (
                 id               INTEGER PRIMARY KEY,
                 login            TEXT NOT NULL UNIQUE,
                 pass_hashsum     TEXT,
                 fname            TEXT NOT NULL,
                 lname            TEXT NOT NULL,
                 mname            TEXT NOT NULL,
                 gr_base          INTEGER NOT NULL,
                 gr_1             INTEGER,
                 gr_2             INTEGER,
                 perm               INTEGER NOT NULL,
                 FOREIGN KEY (gr_base) REFERENCES group_(id),
                 FOREIGN KEY (gr_1 ) REFERENCES group_(id),
                 FOREIGN KEY (gr_2) REFERENCES group_(id)) ",
    ),
    (
        "ecolumn",
        "CREATE TABLE ecolumn (
           id           INTEGER  PRIMARY KEY AUTOINCREMENT,
           name         TEXT NOT NULL
       );",
    ),
    (
        "text_table",
        "CREATE TABLE           text_table (
        task_id                 INTEGER NOT NULL UNIQUE,
        FOREIGN KEY (task_id)   REFERENCES task(id)
            )"      ,
    ),
    (
        "perm_mask",
        "CREATE TABLE           perm_mask (
        id                      INTEGER PRIMARY KEY,
        name                    TEXT NOT NULL,
        owner_id                   INTEGER NOT NULL,
        group_id                   INTEGER NOT NULL,
        FOREIGN KEY (owner_id)   REFERENCES person(id),
        FOREIGN KEY (group_id)   REFERENCES group_(id))" ,
    ),
    (
        "perm_group",
        "CREATE TABLE           perm_group (
        id                      INTEGER PRIMARY KEY,
        pmask_id                INTEGER NOT NULL,
        group_id                INTEGER NOT NULL,
        perm                    BINARY NOT NULL,
        FOREIGN KEY (pmask_id)   REFERENCES perm_mask(id),
        FOREIGN KEY (group_id)   REFERENCES group_(id),
        UNIQUE (pmask_id, group_id))"       ,
    ),
    (
        "log",
        "CREATE TABLE           log (
        id                      INTEGER PRIMARY KEY,
        task_id                INTEGER NOT NULL,
        timestamp               INTEGER NOT NULL,
        person_id                  INTEGER NOT NULL,
        column                  text NOT NULL,
        data                    text,
         FOREIGN KEY (person_id)   REFERENCES person(id),
       FOREIGN KEY (task_id)   REFERENCES task(id))"
    ),

];
//TODO add primary key to text_table


pub(crate) fn to_fetch_task_stmt(ecol_expr: &str, ids_str: &str, static_filed_num: i32) -> String {
    format!(
" SELECT IFNULL(permission.perm, zeroblob(numtable.num + {static_filed_num}) ),
        task.id, task.performer_id, own.fname, own.lname,  own.mname,
        perf.fname, perf.lname,  perf.mname, status.name, task.start_time, task.deadline,
        task.prio, pmask.name, task.create_time, gr.name
        {ecol_expr}
FROM task
OUTER LEFT JOIN text_table AS tt ON tt.task_id = task.id
JOIN person AS own ON task.owner_id = own.id
JOIN person AS perf ON task.performer_id = perf.id
JOIN  perm_mask AS pmask ON task.pmask_id = pmask.id
JOIN status ON status.id = task.status_id
JOIN group_ AS gr ON gr.id = perf.gr_base
OUTER LEFT JOIN
    (WITH RECURSIVE
    cgroups(id) AS ( SELECT gr_base FROM  person WHERE id=?
    UNION SELECT gr_1 FROM  person WHERE id=?
    UNION SELECT gr_2 FROM  person WHERE id=?
    UNION SELECT group_.id FROM cgroups, group_
    WHERE group_.parent=cgroups.id )
    SELECT bin_or_vec(perm, numtable.num + {static_filed_num}) AS perm, task.id AS task_id
    FROM perm_group AS pg
    INNER JOIN task ON pg.pmask_id = task.pmask_id,
    (SELECT COUNT(*) AS num  FROM ecolumn) AS numtable
    WHERE pg.group_id in cgroups
    GROUP BY task.id  ) AS permission ON task.id = permission.task_id,
(SELECT COUNT(*) AS num  FROM ecolumn) AS numtable
WHERE  task.id IN ({ids_str} )  ",
        ecol_expr = ecol_expr,
        ids_str = ids_str,
        static_filed_num = static_filed_num
    )
}


pub(crate) fn to_find_task_stmt ( where_clause: &str) -> String {
    format!("SELECT task.id
    FROM task
    OUTER LEFT JOIN text_table AS tt ON tt.task_id = task.id
      JOIN person AS own ON task.owner_id = own.id
     JOIN person AS perf ON task.performer_id = perf.id
     JOIN perm_mask AS pmask ON task.pmask_id = pmask.id
   JOIN status ON status.id = task.status_id
    JOIN group_ AS gr ON gr.id = perf.gr_base {}
    ",  where_clause)
}

pub(crate) fn create_fts_table_stmt(num: usize) -> Vec<String> {
dbg!(num);
    let columns = (0..num)
        .map(|i| format!("column_{}", i.to_string()))
        .collect::<Vec<_>>()
        .join(", ");
    let columns_new = (0..num)
        .map(|i| format!("new.column_{}", i.to_string()))
        .collect::<Vec<_>>()
        .join(", ");
    let columns_old = (0..num)
        .map(|i| format!("old.column_{}", i.to_string()))
        .collect::<Vec<_>>()
        .join(", ");
    let create = format!(
        "CREATE VIRTUAL TABLE fts USING fts5( {}, content='text_table', content_rowid='task_id')",
        &columns
    );
    let trigger_insert = format!(
        "CREATE TRIGGER text_table_ainsert AFTER INSERT ON text_table BEGIN
                INSERT INTO fts(rowid, {}) VALUES (new.task_id, {});
                END;",
        &columns, &columns_new
    );
    let trigger_delete = format!(
        "CREATE TRIGGER text_table_ad AFTER DELETE ON text_table BEGIN
    INSERT INTO fts(fts, rowid, {}) VALUES('delete', old.task_id, {});
    END;",
        &columns, columns_old
    );
    let trigger_update = format!(
        "CREATE TRIGGER text_table_au AFTER UPDATE ON text_table BEGIN
    INSERT INTO fts(fts, rowid, {0}) VALUES('delete', old.task_id, {1});
    INSERT INTO fts(rowid, {0}) VALUES (new.task_id, {2});
    END;",
        &columns, &columns_old, &columns_new
    );
    vec![create, trigger_insert, trigger_delete, trigger_update]
}

pub(crate) fn text_triggers_stmt(num: usize) ->  Vec<String> {
    (0..num).map(|i| text_trigger_stmt(i)).collect::<Vec<_>>()

}

pub(crate) fn text_trigger_stmt(count: usize) -> String {
    let name = format!("text_col_{}", count.to_string());
    let col = format!("column_{}", count.to_string());
    let table = "text_table";
    let column_num = count.to_string();

    format!(
  " CREATE TRIGGER {name} BEFORE UPDATE OF {col} ON text_table BEGIN
    INSERT INTO log(task_id, timestamp, person_id, column, data)
    SELECT tt.task_id, cdatetime(), getowner(), ec.name, tt.{col}
    FROM (SELECT task_id, {col} FROM text_table WHERE task_id = OLD.task_id) AS tt,
        (SELECT name FROM ecolumn WHERE id = {column_num}) AS ec;
    END;",
        name = name,
        col = col,
        column_num = column_num,
    )
}
