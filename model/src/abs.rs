#![macro_use]
#![warn(clippy::all)]

use crate::stypes::*;
use serde::{Deserialize, Serialize};
use serde_json;
use std::convert::TryFrom;
use std::str::FromStr;
use std::string::ToString;
use std::{error, fmt, result};
use tnsqlite3::{Connection, Mapped, Mapping, PStatement, Sqlite3Ref};

pub(crate) const MAX_VARIANTS: i32 = 500;

pub(crate) type Result<T> = result::Result<T, Error>;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum CodeError {
    GeneralError(String),
    IndexOutOfRange,
    TypeNotSupport,
    OperationNotSupported,
    OpertionNotPermitten,
    EmptyFTSQuery,
    ValueNotCorrect,
    FiltredError,
    PasswordTooWeak,
    AuthenticationFailed,
    ConversionFailed,
    WriteAccessForbid,
}

#[derive(Debug)]
pub enum Error {
    ModelError(CodeError),
    TNSqlite3(tnsqlite3::Error),
    Serde(serde_json::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({:?})", self)
    }
}

impl error::Error for Error {}

macro_rules! impl_from_error {
    ($t:ty, $var:ident) => {
        impl From<$t> for Error {
            fn from(v: $t) -> Error {
                Error::$var(v)
            }
        }
    };
}

impl_from_error!(CodeError, ModelError);
impl_from_error!(tnsqlite3::Error, TNSqlite3);
impl_from_error!(serde_json::Error, Serde);

#[derive(PartialEq, Debug, Serialize, Deserialize, Clone, Copy)]
pub enum ColumnType {
    Complex,
    Int,
    Str,
    Unchangeable,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum Condition {
    Equal,
    Less,
    More,
}

impl ToString for Condition {
    fn to_string(&self) -> String {
        match self {
        Condition::Equal => "Equal".to_string(),
        Condition::Less => "Less".to_string(),
        Condition::More => "More".to_string(),

        }
    }

}

macro_rules! impl_from_where {
    ($t:ty, $variant:tt) => {
        impl From<$t> for Where_ {
            fn from(v: $t) -> Where_ {
                Where_::$variant(v)
            }
        }
    };
}

impl_from_where!((i32, RowId, Condition), Int);
impl_from_where!((i32, String, Condition), Str);
impl_from_where!((i32, Vec<RowId>), EntId);
impl_from_where!((RowId,), RowId);
impl_from_where!((String,), FTS);

impl From<(i32, Vec<i64>)> for Where_ {
    fn from(v: (i32, Vec<i64>)) -> Where_ {
        Where_::EntId( (v.0, v.1.iter().map(| i | RowId(*i) ).collect()) )
    }

}

impl From<(i32, &'_ str, Condition)> for Where_ {
    fn from(v: (i32, &'_ str, Condition)) -> Where_ {
        Where_::Str( (v.0, v.1.to_string(), v.2 ) )
    }

}


impl From<i64> for Where_ {
    fn from(v: i64) -> Where_ {
        Where_::RowId((RowId(v),))
    }
}

#[macro_export]
macro_rules! find {
    ($($cond:expr),+) => {
       Finder::where_( vec![ $($cond.into(),)+ ] )
    };
    () => {
        Finder::new()
    };
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum Where_ {
    Int((i32, RowId, Condition)),
    Str((i32, String, Condition)),
    EntId((i32, Vec<RowId>)),
    RowId((RowId,)),
    FTS((String,)),
    PermGroup((RowId,)),
    Log((RowId,)),
}

impl Where_ {
    pub(crate) fn into_params(
        &self,
        table: &str,
        columns: &[&str],
    ) -> Result<(String, Option<Sqlite3Ref>)> {
        Ok(match self {
            Where_::RowId((id,)) => (
                format!("({}.id = ?)", table),
                Some(Sqlite3Ref::INTEGER(id.0)),
            ),
            Where_::FTS((query,)) => {
                if query.trim().is_empty() {
                    return Err(Error::ModelError(CodeError::EmptyFTSQuery));
                }
                (
                    format!(
                        "tt.task_id in (SELECT rowid FROM fts WHERE fts MATCH '{}')",
                        &query
                    ),
                    None,
                )
            }
            Where_::EntId((column, ids)) => {
                let ids_str = ids
                    .iter()
                    .map(|i| i.to_string())
                    .collect::<Vec<_>>()
                    .join(",");
                (
                    format!("({} in ({}))", columns[*column as usize], &ids_str),
                    None,
                )
            }
            Where_::Int((column, id, cond)) => {
                                (
                    format!("({} {} ?)", columns[*column as usize], &cond.to_string() ),
                    Some(Sqlite3Ref::INTEGER(id.0)),
                )
            }
            Where_::PermGroup((pmask_id,)) => ("(pmask_id = ?)".to_string(),
                                        Some(Sqlite3Ref::INTEGER(pmask_id.0))),
            Where_::Log((task_id,)) => ("(log.task_id = ?)".to_string(),
                                        Some(Sqlite3Ref::INTEGER(task_id.0))),
            _ => unimplemented!(),
        })
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
#[serde(transparent)]
pub struct Finder(pub(crate) Vec<Where_>);

impl Finder {
    pub fn new() -> Self {
        Finder(Vec::new())
    }
    pub fn where_(v: Vec<Where_>) -> Self {
        Finder(v)
    }

    pub(crate) fn into_params(
        &self,
        table: &str,
        columns: &[&str],
    ) -> Result<(String, Vec<Sqlite3Ref>)> {
        let mut pair = (Vec::new(), Vec::new());
        for w in self.0.iter() {
            let (expr, param) = w.into_params(table, columns)?;
            pair.0.push(expr);
            if let Some(p) = param {
                pair.1.push(p);
            }
        }
        Ok(if self.0.is_empty() {
            (String::new(), Vec::new())
        } else {
            (" WHERE ".to_string() + &pair.0.join(" AND "), pair.1)
        })
    }
}

pub enum PermLevel {
    Ordinary = 1,
    PermMaskCreate = 1 << 1,
    Administration = 1 << 2,
}

#[derive(Clone, Copy)]
pub enum Privileges {
    Ordinary = PermLevel::Ordinary as isize,
    PermMaskCreate = PermLevel::Ordinary as isize | PermLevel::PermMaskCreate as isize,
    Administration = PermLevel::Ordinary as isize
        | PermLevel::PermMaskCreate as isize
        | PermLevel::Administration as isize,
}

impl Into<i64> for Privileges {
    fn into(self) -> i64 {
        self as i64
    }
}

impl ToString for Privileges {
    fn to_string(&self) -> String {
        use Privileges::*;
        match self{
            Ordinary => "User".to_string(),
            PermMaskCreate => "Create pmask".to_string(),
            Administration => "Admin".to_string(),
        }
    }
}

impl TryFrom<i64> for Privileges {
    type Error = Error;

    fn try_from(p: i64) -> result::Result<Self, Self::Error> {
        use Privileges::*;
        match p {
        1 => Ok(Ordinary),
        0b11 => Ok(PermMaskCreate),
        0b111 => Ok(Administration),
        _ => Err(Error::ModelError(CodeError::ConversionFailed)),
        }
    }
}


impl From<Privileges> for PersonPermission {
    fn from(v: Privileges) -> Self {
        PersonPermission(v.into())
    }

}

#[derive(Clone, Copy, Debug)]
pub struct PersonPermission(i64);


impl Default for PersonPermission {
    fn default() -> Self {
        PersonPermission(0)
    }
}

impl PersonPermission {
    pub fn from_i64(v: i64) -> Self {
        PersonPermission(v)
    }

    pub(crate) fn level(self, lev: PermLevel) -> bool {
        self.0 & (lev as i64) != 0
       }
}

impl TryFrom<RowId> for PersonPermission {
    type Error = Error;

    fn try_from(p: RowId) -> result::Result<Self, Self::Error> {
        if p.0 == 0 || p.0 == 1 || p.0 == 0b11 || p.0 == 0b111 {
            Ok(PersonPermission(p.0))
        } else {
            Err(Error::ModelError(CodeError::ConversionFailed))
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Row {
    pub(crate) id: RowId,
    pub columns: Vec<String>,
    pub meta: Meta,
}

#[derive(PartialEq, Clone, Debug)]
pub struct MetaColumn {
    pub permission: Permission,
    pub kind: ColumnKind,
}

impl Default for MetaColumn {
    fn default() -> Self {
        MetaColumn {
            permission: Permission::RW,
            kind: ColumnKind::Text,
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct MetaRow {
    pub permission: Permission,
}

impl Default for MetaRow {
    fn default() -> Self {
        MetaRow {
            permission: Permission::RW,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Permission {
    RO = 0,
    RW = 1,
}

impl ToString for Permission {
    fn to_string(&self) -> String {
        use Permission::*;
        match self {
            RO => "RO".to_string(),
            RW => "RW".to_string(),
        }
    }
}

impl Default for Permission {
    fn default() -> Self {
        Permission::RW
    }
}

//impl From<u8> for Permission {
//    fn from(v: u8) -> Self {
//        match v {
//            1 => Permission::RW,
//            _ => Permission::RO
//    }
//}}

macro_rules! impl_from_for_Permission {
    ($t:ty) => {
    impl From<$t> for Permission {
        fn from(v: $t) -> Self {
            match v {
                    1 => Permission::RW,
                    _ => Permission::RO,
                }
            }
        }
    };
}
impl_from_for_Permission!(u8);
impl_from_for_Permission!(i64);

impl TryFrom<RowId> for Permission {
    type Error = Error;

    fn try_from(value: RowId) -> result::Result<Self, Self::Error> {
        match value.0 {
            0 => Ok(Permission::RO),
            1 => Ok(Permission::RW),
            _ => Err(Error::ModelError(CodeError::ConversionFailed)),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ColumnKind {
    Text = 0,
    DateTime,
    Int,
}

impl Default for ColumnKind {
    fn default() -> Self {
        ColumnKind::Text
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct Meta {
    pub h: MetaRow,
    pub c: Vec<MetaColumn>,
    pub s: Option<RowId>,
}

impl Meta {
    pub(crate) fn default_from_num(num: usize) -> Self {
        Meta {
            h: MetaRow::default(),
            c: [MetaColumn::default()]
                .iter()
                .cycle()
                .cloned()
                .take(num)
                .collect(),
            s: None,
        }
    }
}

//pub(crate) struct RawMeta(Vec<u8>);

#[derive( Debug)]
pub struct Context {
    pub(crate) person_id: i64,
    pub(crate) perm: PersonPermission,
}

impl Context {
    pub(crate) fn new() -> Self {
        Context {
            person_id: 0,
            perm: PersonPermission::default(),
        }
    }

    pub fn from_permission(perm: PersonPermission)-> Self  {
        Context{person_id: 1,  perm }
    }
}


pub(crate) trait Columnar<S>:
    UpdateInt<S> + UpdateStr<S> + FetchData<Stringinizer = S> + GetColumnType<S>
{
}

impl<T, S> Columnar<S> for T where
    T: UpdateInt<S> + UpdateStr<S> + FetchData<Stringinizer = S> + GetColumnType<S>
{
}

pub(crate) trait GetColumn<S> {
    fn get_column(&mut self, column_idx: i32)
        -> Result<&mut (dyn Columnar<S> + Send + 'static)>;
}

impl<T, S> UpdateInt<S> for T
where
    T: GetColumn<S>,
{
    fn update_int(
        &mut self,
        id: RowId,
        column: i32,
        value: RowId,
         ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        self.get_column(column)?.update_int(id, column, value, ctx, conn)
    }
}

impl<T, S> UpdateStr<S> for T
where
    T: GetColumn<S>,
{
    fn update_str(
        &mut self,
        id: RowId,
        column: i32,
        value: &str,
         ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        self.get_column(column)?.update_str(id, column, value, ctx, conn)
    }
}

impl<T, S> ColumnVariants<S> for T
where
    T: GetColumn<S>,
{
    fn column_variants(
        &mut self,
        column: i32,
        s: &S,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        self.get_column(column)?
            .fetch_data(Vec::new(), s, ctx, conn)
    }
}

impl<T, S> GetColumnType<S> for T
where
    T: GetColumn<S>,
{
    fn get_column_type(&mut self, id: RowId, column: i32,   ctx: &Context, conn: &mut Connection) -> Result<ColumnType> {
        self.get_column(column)?.get_column_type(id, column, ctx,  conn)
    }
}

pub(crate) trait TableModel<S>:
    ColumnVariants<S>
    + FetchData<Stringinizer = S>
    + DeleteColumn
    + UpdateInt<S>
    + UpdateStr<S>
    + ColumnVariants<S>
    + GetColumnType<S>
    + Find
    + Header
    + AddColumns
{
}

impl<T, S> TableModel<S> for T where
    T: ColumnVariants<S>
        + FetchData<Stringinizer = S>
        + DeleteColumn
        + UpdateInt<S>
        + UpdateStr<S>
        + GetColumnType<S>
        + Find
        + FetchData<Stringinizer = S>
        + Header
        + AddColumns
{
}

pub(crate) trait Header {
    fn header(&self, _conn: &mut Connection) -> Result<Vec<String>> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait AddColumns {
    fn add_columns(&self, _column_list: Vec<String>,  _conn: &mut Connection) -> Result<()> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait DeleteColumn {
    fn delete_column(&self, _idx: i32,  _conn: &mut Connection) -> Result<()> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait UpdateInt<S> {
    fn update_int(
        &mut self,
        _id: RowId,
        _column: i32,
        _value: RowId,
        _ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<()> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait UpdateStr<S> {
    fn update_str(
        &mut self,
        _id: RowId,
        _column: i32,
        _value: &str,
        _ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<()> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait ColumnVariants<S> {
    fn column_variants(
        &mut self,
        _column: i32,
        _s: &S,
        _ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait GetColumnType<S> {
    fn get_column_type(&mut self, _id: RowId, _column: i32, _ctx: &Context, _conn: &mut Connection) -> Result<ColumnType> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait FetchData {
    type Stringinizer;
    fn fetch_data(
        &mut self,
        _ids: Vec<RowId>,
        _s: &Self::Stringinizer,
        _ctx: &Context,
        _conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait Find {
    fn find(&mut self, _params: &Finder, _conn: &mut Connection) -> Result<Vec<RowId>> {
        Err(Error::ModelError(CodeError::OperationNotSupported))
    }
}

pub(crate) trait SqlApply<V> {
    fn sql_apply<'a>(
        src: &V,
        ids: Vec<RowId>,
        ctx: &Context,
        conn: &'a mut Connection,
    ) -> result::Result<PStatement<'a>, tnsqlite3::Error>;
}

pub(crate) trait WriteAccess {
    fn write_access_exists(&self, rowid: i64, column: i32, ctx: &Context,
    conn: &mut Connection) -> Result< bool>;
    fn write_access(&self, rowid: i64,  column: i32, ctx: &Context,
        conn: &mut Connection) -> Result<()> {
        if self.write_access_exists(rowid, column, ctx, conn)? {
            Ok(())
        } else {
            Err(Error::ModelError(CodeError::WriteAccessForbid))
        }
    }
}

pub(crate) trait GetRowPermission {
    fn get_row_permission(&self, ctx: &Context) -> &[u8];
}



pub trait Procedure {
    fn id(&self) -> &str;
}

pub trait TryClone<T, E> {
    fn try_clone(&self) -> result::Result<T, E>;
}

pub trait Model {
    type Connection;
    type Output;
    type Proc;
    fn request(&mut self, method: Self::Proc) -> Result<Self::Output>;
    fn init_db(&mut self) -> Result<()>;
    fn db_connection(&mut self) -> &mut Self::Connection;
}

pub trait Stringinize<T> {
    type Stringinizer;
    const COLUMN_KIND: &'static [ColumnKind];
    fn stringinize(src: T, row: &mut Vec<String>, r: &Self::Stringinizer);
}

pub trait Stringinized: ColumnKinded {
    type Stringinizer;
    fn stringinized(self, r: &Self::Stringinizer) -> String;
}

pub trait ColumnKinded {
    const COLUMN_KINDED: ColumnKind;
}
