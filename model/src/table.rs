use super::*;
use crate::abs::*;

use crate::sql::*;
use crate::stypes::RowId;
use crate::EColumn;
use generic_array::{ArrayLength, GenericArray};
use std::marker::PhantomData;
use std::result;
use tn_proc::Stringinize;
use tnsqlite3::{Connection, Identify, Insert, Mapped, Mapping};

pub(crate) struct ExtTable<V, M, N, G>
where
    N: ArrayLength<Column>,
{
    pub(crate) static_header: &'static [&'static str],
    pub(crate) up_block: GenericArray<Column, N>,
    pub(crate) ext_up_block: M,
    pub(crate) guard: G,
    pub(crate) view: PhantomData<V>,
}

impl<V, M, N, G> GetColumnType<Stringinizer> for ExtTable<V, M, N, G>
where
    M: GetColumnType<Stringinizer>,
    N: ArrayLength<Column>,
    G: WriteAccess,
{
    fn get_column_type(
        &mut self,
        id: RowId,
        column: i32,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<ColumnType> {
        if !self.guard.write_access_exists(id.0, column, ctx, conn)? {
            return Ok(ColumnType::Unchangeable);
        }

        if column >= 0 && column < N::to_i32() {
            self.up_block[column as usize].get_column_type(id, column, ctx, conn)
        } else {
            self.ext_up_block
                .get_column_type(id, column - N::to_i32(), ctx, conn)
        }
    }
}

impl<V, M, N, G> UpdateInt<Stringinizer> for ExtTable<V, M, N, G>
where
    M: UpdateInt<Stringinizer>,
    N: ArrayLength<Column>,
    G: WriteAccess,
{
    fn update_int(
        &mut self,
        id: RowId,
        column: i32,
        value: RowId,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        dbg!(column);

        self.guard.write_access(id.0, column, ctx, conn)?;
        if column >= 0 && column < N::to_i32() {
            self.up_block[column as usize].update_int(id, column, value, ctx, conn)
        } else {
            self.ext_up_block
                .update_int(id, column - N::to_i32(), value, ctx, conn)
        }
    }
}

impl<V, M, N, G> UpdateStr<Stringinizer> for ExtTable<V, M, N, G>
where
    M: UpdateStr<Stringinizer>,
    N: ArrayLength<Column>,
    G: WriteAccess,
{
    fn update_str(
        &mut self,
        id: RowId,
        column: i32,
        value: &str,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<()> {
        self.guard.write_access(id.0, column, ctx, conn)?;
        if column >= 0 && column < N::to_i32() {
            self.up_block[column as usize].update_str(id, column, value, ctx, conn)
        } else {
            self.ext_up_block
                .update_str(id, column - N::to_i32(), value, ctx, conn)
        }
    }
}

impl<V, M, N, G> ColumnVariants<Stringinizer> for ExtTable<V, M, N, G>
where
    M: ColumnVariants<Stringinizer>,
    N: ArrayLength<Column>,
{
    fn column_variants(
        &mut self,
        column: i32,
        s: &Stringinizer,
        ctx: &Context,
        conn: &mut Connection,
    ) -> Result<Vec<Row>> {
        if column >= 0 && column < N::to_i32() {
            self.up_block[column as usize].fetch_data(Vec::new(), s, ctx, conn)
        } else {
            self.ext_up_block
                .column_variants(column - N::to_i32(), s, ctx, conn)
        }
    }
}

impl<V, M, N, G> AddColumns for ExtTable<V, M, N, G>
where
    M: AddColumns,
    N: ArrayLength<Column>,
{
    fn add_columns(&self, column_list: Vec<String>, conn: &mut Connection) -> Result<()> {
        self.ext_up_block.add_columns(column_list, conn)
    }
}

impl<V, M, N, G> DeleteColumn for ExtTable<V, M, N, G>
where
    M: DeleteColumn,
    N: ArrayLength<Column>,
{
    fn delete_column(&self, idx: i32, conn: &mut Connection) -> Result<()> {
        if idx < N::to_i32() {
            Err(Error::ModelError(CodeError::ValueNotCorrect))
        } else {
            self.ext_up_block.delete_column(idx - N::to_i32(), conn)
        }
    }
}

impl<V, M, N, G> Header for ExtTable<V, M, N, G>
where
    M: Header,
    N: ArrayLength<Column>,
{
    fn header(&self, conn: &mut Connection) -> Result<Vec<String>> {
        let mut header = self
            .static_header
            .iter()
            .map(|n| n.to_string())
            .collect::<Vec<_>>();
        header.extend(self.ext_up_block.header(conn)?);
        Ok(header)
    }
}
