#![warn(clippy::all)]

use crate::abs::{ColumnKind, Meta, MetaColumn, MetaRow, Permission};
use crate::Method;
use serde::{de, de::MapAccess, Deserializer, Serializer};
use serde::{Deserialize, Serialize};
use std::mem;
use std::str::FromStr;
use std::{fmt, num};

impl Serialize for MetaRow {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let v = self.permission as u32;

        serializer.serialize_u32(v)
    }
}

impl<'de> Deserialize<'de> for MetaRow {
    fn deserialize<D>(deserializer: D) -> Result<MetaRow, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct MetaRowVisitor;

        impl<'de> de::Visitor<'de> for MetaRowVisitor {
            type Value = MetaRow;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                write!(formatter, "expects to receive struct MetaRow")
            }

            fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                let permission = match v as u8 {
                    0 => Permission::RO,
                    1 => Permission::RW,
                    _ => return Err(E::custom("Permission not 0 or 1 ")),
                };

                Ok(MetaRow { permission })
            }
        }

        deserializer.deserialize_u64(MetaRowVisitor)
    }
}

impl Serialize for MetaColumn {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let v = ((self.permission as u16) | (self.kind as u16) << 8) as u32;

        serializer.serialize_u32(v)
    }
}

impl<'de> Deserialize<'de> for MetaColumn {
    fn deserialize<D>(deserializer: D) -> Result<MetaColumn, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct MetaColumnVisitor;

        impl<'de> de::Visitor<'de> for MetaColumnVisitor {
            type Value = MetaColumn;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                write!(
                    formatter,
                    " expects to receive struct MetaColumn"
                )
            }

            fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                let permission = match v & 0xFF {
                    0 => Permission::RO,
                    1 => Permission::RW,
                    _ => return Err(E::custom("Permission not 0 or 1 ")),
                };
                let kind = match (v >> 8) & 0xFF {
                    0 => ColumnKind::Text,
                    1 => ColumnKind::DateTime,
                    _ => return Err(E::custom("ColumnKind not 0 or 1 ")),
                };
                Ok(MetaColumn { permission, kind })
            }
        }
        deserializer.deserialize_u64(MetaColumnVisitor)
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct RowId(pub i64);

impl FromStr for RowId {
    type Err = num::ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(RowId(s.parse()?))
    }
}

impl From<i64> for RowId {
    fn from(v: i64) -> Self {
        RowId(v)
    }
}

impl fmt::Display for RowId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        write!(f, "{}", self.0)
    }
}

impl Serialize for RowId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de> Deserialize<'de> for RowId {
    fn deserialize<D>(deserializer: D) -> Result<RowId, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct RowIdVisitor;

        impl<'de> de::Visitor<'de> for RowIdVisitor {
            type Value = RowId;
            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                write!(formatter, "his Visitor expects to receive struct RowId")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                RowId::from_str(v).map_err(|e| E::custom(e))
            }
        }

        deserializer.deserialize_str(RowIdVisitor)
    }
}



#[cfg(test)]
mod tests {

    use super::*;
    use serde::{Deserialize, Serialize};
    use serde_json;

    #[derive(Serialize, Deserialize, Debug)]
    struct Ro {
        id: RowId,
    }

    macro_rules! serdeser {
        ($obj:expr, $t:ty) => {
            let p = serde_json::to_string(&$obj).unwrap();
            println!("{:?}", &p);
            let res: $t = serde_json::from_str(&p).unwrap();
        };
    }



    #[test]
    fn rowid_serde_test() {
        let rid = RowId(-23646854789846);
        let s = serde_json::to_string(&rid).unwrap();
        println!("{:?}", &s);

        let r: RowId = serde_json::from_str(&s).unwrap();
        println!("{:?}", &r);
    }

    #[test]
    fn struct_rowid_serde_test() {
        let rid = Ro {
            id: RowId(-23646854789846),
        };
        let s = serde_json::to_string(&rid).unwrap();
        assert_eq!("{\"id\":\"-23646854789846\"}", &s);
        let r: Ro = serde_json::from_str(&s).unwrap();
        println!("{:?}", &r);
    }

    #[test]
    fn metacolumn_serde_test() {
        let mc = vec![ MetaColumn{ permission: Permission::RW, kind: ColumnKind::DateTime }];
        serdeser!(mc, Vec<MetaColumn>);
    }    #[test]

    fn metarow_serde_test() {
        let mc =  MetaRow{ permission: Permission::RW, };
        serdeser!(mc, MetaRow);
    }

}
