mod common;
use common::*;



#[test]
fn task_view_permission_test() {
    init_db!(tnmod, new_status!("high status"), new_status!("very high status"), new_group!("group1"),
    new_group!("group2"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    insert!(tnmod, new_person!("a1", "b2", "c", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_perm_mask!("mask2", 1, 2));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    add_perm_group_method!(1, 1, 1, tnmod);
    update_int_perm_group_and_task_check_method!(1, 1, 0, Permission::RO as i64, "RO", tnmod);
update_int_perm_group_and_task_check_method!(1, 1, 5, Permission::RO as i64, "RO", tnmod);

}



#[test]
fn log_task_test() {
    init_db!(tnmod, new_status!("high status"), new_status!("very high status"), new_group!("group1"),
    new_group!("group2"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    insert!(tnmod, new_person!("a1", "b2", "c", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_perm_mask!("mask2", 1, 2));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    update_int_method!(Task, 1, 0, 2, "b2 a. c.", tnmod);
    update_int_method!(Task, 1, 2, 2, "very high status", tnmod);

    find_wf_method!(Log, vec![1 ,2], find!(Where_::Log((RowId(1), ))), tnmod);
    fetch_method!(Log, vec![1, 2], 2, tnmod);

}

#[test]
fn columns_variants_perm_group_recursive_case_2_test() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group!("group 2"),
        new_group_w_parent!("gen 1 gr 1", 1),
        new_group_w_parent!("gen 1 gr 1 gr 1", 3),
        new_group!("group 3"),
        new_group_w_parent!("gen 1 gr 3", 5),
        new_group_w_parent!("gen 1 gr 3 gr 1", 6)
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    update_int_method!(Person, 1, 4, 6, "gen 1 gr 3", tnmod);
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    add_perm_group_method!(1, 1, 1, tnmod);
//    column_variants_method!(PermGroup, 0, 5, tnmod);
}


#[test]
fn columns_variants_perm_group_recursive_case_1_test() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group!("group 2"),
        new_group_w_parent!("gen 1 gr 1", 1),
        new_group_w_parent!("gen 1 gr 1 gr 1", 3),
        new_group!("group 3"),
        new_group_w_parent!("gen 1 gr 3", 5),
        new_group_w_parent!("gen 1 gr 3 gr 1", 6)
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    update_int_method!(Person, 1, 4, 5, "group 3", tnmod);
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    add_perm_group_method!(1, 1, 1, tnmod);
//    column_variants_method!(PermGroup, 0, 6, tnmod);
}


#[test]
fn columns_variants_perm_group_recursive_test() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group_w_parent!("gen 1 gr 1", 1),
        new_group_w_parent!("gen 1 gr 2", 1),
        new_group_w_parent!("gen 1 gr 1 gr 1", 2)
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    add_perm_group_method!(1, 1, 1, tnmod);
    column_variants_method!(PermGroup, 1, 2, tnmod);
}


#[test]
fn update_int_perm_group() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group!("group 2")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    add_perm_group_method!(1, 1, 1, tnmod);
    update_int_perm_group_method!(PermGroup, 1, 2, 1, "RW", tnmod);
    update_int_perm_group_method!(PermGroup, 1, 2, 0, "RO", tnmod);
}


#[test]
fn columns_variants_perm_group_test() {

    init_db!(
        tnmod,
        new_group!("group 1")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    add_perm_group_method!(1, 1, 1, tnmod);
    column_variants_method!(PermGroup, 1, 2, tnmod);
}


#[test]
fn perm_group_find_and_fetch_test() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group!("group 2")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    find_wf_method!(PermGroup, vec![], find!(1), tnmod);
    fetch_method!(PermGroup, vec![], 0, tnmod);

    add_perm_group_method!(1, 1, 1, tnmod);
    find_wf_method!(PermGroup,  vec![1], find!(1), tnmod);
    fetch_method!(PermGroup, vec![1], 1, tnmod);

}


#[test]
fn perm_group_add_test() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group!("group 2"),
        new_status!("status 1")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    add_perm_group_method!(1, 1, 1,  tnmod);
    add_columns_method!(Task, tnmod, "column 0");
    add_columns_method!(Task, tnmod, "column 1");
    add_perm_group_method!(1, 2, 2,  tnmod);
    delete_column_method!(Task, tnmod, STATIC_FIELDS);
}


#[test]
fn update_int_perm_mask_test() {
    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group!("group 2")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    update_int_method!(PermMask, 1, 2, 2, "group 2", tnmod);
}

#[test]
fn columns_variants_perm_mask_test() {

    init_db!(
        tnmod,
        new_group!("group 1"),
        new_group_w_parent!("group 2", 1)
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    column_variants_method!(PermMask, 2, 2, tnmod);
}


#[test]
fn perm_mask_find_and_fetch_test() {
    init_db!(
        tnmod,
        new_group!("group 1")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));

    find_method!(PermMask, vec![], tnmod);
    fetch_method!(PermMask, vec![], 0, tnmod);

    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    find_method!(PermMask, vec![1], tnmod);
    fetch_method!(PermMask, vec![1], 1, tnmod);
    insert!(tnmod, new_perm_mask!("mask1", 2, 1));
    find_method!(PermMask, vec![1, 2], tnmod);
    fetch_method!(PermMask, vec![1, 2], 2, tnmod);

}


#[test]
fn admin_count_test() {
    init_db!(tnmod, new_group!("high status"));
    admin_count_method!(0, tnmod);
    insert!(tnmod, new_person_perm!("Dow", "John", "AA", PermLevel::Ordinary as i64 , 1));
    admin_count_method!(0, tnmod);
    insert!(tnmod, new_person_perm!("Dow1", "John1", "AA1", PermLevel::Administration as i64 , 1));
    admin_count_method!(1, tnmod);
}

#[test]
fn change_password_test() {
    init_db!(tnmod, new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    change_password_method!("Dow" , "111", "222", tnmod);

}

#[test]
fn set_password_test() {
    init_db!(tnmod, new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    set_password_method!("Dow" , "111", tnmod);

}


#[test]
fn authenticate_test() {
    init_db!(tnmod, new_group!("primary"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    authenticate_method!("Dow" , "111", 1, 1 , tnmod);

}

#[test]
fn update_status_column_task_test() {
    init_db!(
        tnmod,
        new_status_wprio!("high status", 0),
        new_status_wprio!("high status1", 1),
        new_status_wprio!("high status2", 1),
        new_group!("high status")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 1, tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 1, 1, 2,  tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 3,  tnmod);

    add_task_method!(1, 2, 2, 14124, 12355, 0, 1, 4,   tnmod);
    add_task_method!(1, 2, 1, 14124, 12355, 1, 1, 5,  tnmod);
    add_task_method!(1, 2, 2, 14124, 12355, 0, 1, 6,  tnmod);
    add_task_method!(1, 2, 2, 14124, 12355, 1, 1, 7, tnmod);

    update_status_column_task_method!(1,  2,  tnmod);
    update_status_column_task_method!(1,  1,  tnmod);
    update_status_column_task_method!(1,  2,  tnmod);
    update_status_column_task_method!(4,  3,  tnmod);
    update_status_column_task_method!(5,  1,  tnmod);
    update_status_column_task_method!(6,  2,  tnmod);
     update_status_column_task_method!(7,  1,  tnmod);
     update_status_column_task_method!(1,  1,  tnmod);

    update_status_column_task_method!(3,  2,  tnmod);
    update_status_column_task_method!(5,  1,  tnmod);
    update_status_column_task_method!(6,  3,  tnmod);
     update_status_column_task_method!(7,  2,  tnmod);
     update_status_column_task_method!(2,  3,  tnmod);
     update_status_column_task_method!(1,  2,  tnmod);
     update_status_column_task_method!(4,  2,  tnmod);


}


#[test]
fn update_performer_column_task_test() {
    init_db!(
        tnmod,
        new_status_wprio!("high status", 0),
        new_status_wprio!("high status1", 1),
        new_group!("high status")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_task_method!(1, 1, 2, 14124, 12355, 0, 1,  1, tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 1, 1,  2, tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 0, 1,  3, tnmod);

    add_task_method!(1, 2, 2, 14124, 12355, 0, 1,  4, tnmod);
    add_task_method!(1, 2, 1, 14124, 12355, 1, 1,  5, tnmod);
    add_task_method!(1, 2, 2, 14124, 12355, 0, 1,  6, tnmod);
    add_task_method!(1, 2, 2, 14124, 12355, 1, 1,  7, tnmod);

    update_performer_column_task_method!(1,  2,  tnmod);
    update_performer_column_task_method!(1,  1,  tnmod);
    update_performer_column_task_method!(1,  2,  tnmod);
    update_performer_column_task_method!(4,  1,  tnmod);
    update_performer_column_task_method!(5,  1,  tnmod);
    update_performer_column_task_method!(6,  1,  tnmod);
     update_performer_column_task_method!(7,  1,  tnmod);
     update_performer_column_task_method!(1,  1,  tnmod);

    update_performer_column_task_method!(3,  2,  tnmod);
    update_performer_column_task_method!(5,  2,  tnmod);
    update_performer_column_task_method!(6,  2,  tnmod);
     update_performer_column_task_method!(7,  2,  tnmod);
     update_performer_column_task_method!(2,  2,  tnmod);
     update_performer_column_task_method!(1,  2,  tnmod);
     update_performer_column_task_method!(4,  2,  tnmod);


}


#[test]
fn update_prio_method_test() {
    init_db!(
        tnmod,
        new_status_wprio!("high status", 0),
        new_status_wprio!("high status1", 1),
        new_group!("high status")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 1,  tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 1, 1, 2,   tnmod);
    add_task_method!(1, 1, 1, 14124, 12355, 1, 1, 3,  tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 4,  tnmod);
     add_task_method!(1, 1, 2, 14124, 12355, 2, 1, 5,  tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 4, 1, 6,  tnmod);

    update_prio_method!(1, 0, tnmod);
    update_prio_method!(2, 1, tnmod);
    update_prio_method!(4, 2, tnmod);
    update_prio_method!(5, 3, tnmod);
    update_prio_method!(6, 4, tnmod);

}


#[test]
fn add_task_method_test() {
    init_db!(
        tnmod,
        new_status_wprio!("high status", 0),
        new_status_wprio!("high status1", 1),
        new_group!("high status")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 1, tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 2, tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 1, 1, 3,  tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 1, 1, 4,  tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 1, 1, 5, tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 0, 1, 6,   tnmod);
    add_task_method!(1, 1, 2, 14124, 12355, 4, 1, 7, tnmod);

}


#[test]
fn prio_for_executor_test() {
    init_db!(
        tnmod,
        new_status_wprio!("high status", 0),
        new_status_wprio!("high status1", 1),
        new_group!("high status")
    );
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    prio_for_executor_method!(1, None, tnmod);

    insert!(tnmod, new_task_wprio!(1, 1, 2, 14124, 12355, 0, 1));
    prio_for_executor_method!(1, Some(0), tnmod);

    insert!(tnmod, new_task_wprio!(1, 1, 2, 221242, 235235, 1, 1));
    prio_for_executor_method!(1, Some(1), tnmod);

    insert!(tnmod, new_task_wprio!(1, 1, 2, 34142, 323523, 2, 1));
    prio_for_executor_method!(1, Some(2), tnmod);

}



#[test]
fn update_ext_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_columns_method!(Task, tnmod, "column 0");
    add_columns_method!(Task, tnmod, "column 1");
    add_columns_method!(Task, tnmod, "column 2");
    add_columns_method!(Task, tnmod, "column 3");
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    update_str_method!(Task, 1,  STATIC_FIELDS, "def", tnmod);
    update_str_method!(Task, 1,  STATIC_FIELDS + 2, "def2", tnmod);
    find_method!(Task, vec![1], tnmod);
    fetch_method!(Task, vec![1], 1, tnmod);

}

#[test]
fn find_fts_and_fetch_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_columns_method!(Task, tnmod, "column 0");
    add_columns_method!(Task, tnmod, "column 1");
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    update_str_method!(Task, 1,  STATIC_FIELDS, "def", tnmod);
    update_str_method!(Task, 2,  STATIC_FIELDS, "def", tnmod);

    find_wf_method!(Task, vec![1,2], find!((" def".to_string(),)), tnmod);
    fetch_method!(Task, vec![1,2], 2, tnmod);

    find_wf_method!(Task, vec![1], find!((" def".to_string(),), 1), tnmod);
    fetch_method!(Task, vec![1], 1, tnmod);
}


#[test]
fn find_and_fetch_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    add_columns_method!(Task, tnmod, "column 0");
    add_columns_method!(Task, tnmod, "column 1");
    add_columns_method!(Task, tnmod, "column 2");
    add_columns_method!(Task, tnmod, "column 3");
    find_method!(Task, vec![], tnmod);
//    fetch_method!(Task, vec![], 0, tnmod);
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    find_method!(Task, vec![1,2], tnmod);
    fetch_method!(Task, vec![1,2], 2, tnmod);
}

#[test]
fn find_wf_and_fetch_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    find_wf_method!(Task, vec![2], find!(2), tnmod);
    fetch_method!(Task, vec![2], 1, tnmod);

}

#[test]
fn delete_column_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));

    add_columns_method!(Task, tnmod, "column 0");
    add_columns_method!(Task, tnmod, "column 1");
    add_columns_method!(Task, tnmod, "column 2");
    add_columns_method!(Task, tnmod, "column 3");

    delete_column_method!(Task, tnmod, 1 + STATIC_FIELDS);
    delete_column_method!(Task, tnmod, STATIC_FIELDS);
    delete_column_method!(Task, tnmod, 1 + STATIC_FIELDS);
}


#[test]
fn add_columns_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));

    add_columns_method!(Task, tnmod, "column 0");
    add_columns_method!(Task, tnmod, "column 1");
    add_columns_method!(Task, tnmod, "column 2");
    add_columns_method!(Task, tnmod, "column 3");

}

#[test]
fn update_int_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("group1"),
    new_group!("group2"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    insert!(tnmod, new_person!("a1", "b", "c", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_perm_mask!("mask2", 1, 2));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    update_int_method!(Task, 1, 6, 2, "mask2", tnmod);

}

#[test]
fn column_variant_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    insert!(tnmod, new_person!("a1", "b", "c", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));

    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    column_type_method!(Task, 1, 0, ColumnType::Complex, tnmod);
    column_type_method!(Task,1, 1, ColumnType::Complex, tnmod);
    column_type_method!(Task, 1,2, ColumnType::Complex, tnmod);
    column_type_method!(Task, 1,3, ColumnType::Int, tnmod);
    column_type_method!(Task,1, 4, ColumnType::Int, tnmod);

    column_variants_method!(Task, 0, 2, tnmod);
    column_variants_method!(Task, 2, 1, tnmod);
}

#[test]
fn column_variants_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

    column_variants_method!(Task, 0, 1, tnmod);
    column_variants_method!(Task, 1, 1, tnmod);
}

#[test]
fn column_variants_task_case_1_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("Dow", "John", "AA", 1));
    insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));
    column_variants_method!(Task, 6, 1, tnmod);
}


#[test]
fn add_task_test() {
    init_db!(tnmod, new_status!("high status"), new_group!("high status"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    insert!(tnmod, new_perm_mask!("mask1", 1, 1));
    insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

}


#[test]
fn columns_variants_status_test() {

    init_db!(tnmod,);
    init_db!(tnmod, new_status!("high status"));
    column_variants_method!(Status, 1, 2, tnmod);
}


#[test]
fn update_str_status_test() {
    init_db!(tnmod,);
    insert!(tnmod, new_status!("high status"));
    update_str_method!(Status, 1, 0, "new_a", tnmod);

}


#[test]
fn column_type_status_test() {
    init_db!(tnmod,);
    column_type_method!(Status, 1,0, ColumnType::Str, tnmod);

}

#[test]
fn find_and_fetch_status_test() {
    init_db!(tnmod,);
    find_method!(Status, vec![], tnmod);
    fetch_method!(Status, vec![], 0, tnmod);
    insert!(tnmod, new_status!("high status"));
    find_method!(Status, vec![1], tnmod);
    insert!(tnmod, new_status!("high status"));
    find_method!(Status, vec![1,2], tnmod);
    fetch_method!(Status, vec![1,2], 2, tnmod);

}



#[test]
fn add_status_test() {
    init_db!(tnmod,);
    insert!(tnmod, new_status!("high status"));
}

#[test]
fn update_int_persons_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    insert!(tnmod, new_person!("a", "b", "c", 1));

    update_int_method!(Person, 1, 3, 2, "second", tnmod);

}

#[test]
fn update_str_persons_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    insert!(tnmod, new_person!("a", "b", "c", 1));

    update_str_method!(Person, 1, 0, "new_a", tnmod);

}

#[test]
fn column_variants_persons_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    column_variants_method!(Person, 3, 2, tnmod);
    column_variants_method!(Person, 4, 3, tnmod);
}


#[test]
fn column_type_persons_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    column_type_method!(Person,1, 0, ColumnType::Str, tnmod);
    column_type_method!(Person, 1,3, ColumnType::Complex, tnmod);
}


#[test]
fn find_and_fetch_persons_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    insert!(tnmod, new_person!("a", "b", "c", 1));
    find_wf_method!(Person, vec![1], find!(1), tnmod);
    fetch_method!(Person, vec![1], 1 , tnmod);
}

#[test]
fn find_finder_and_fetch_group1_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    find_method!(Group, vec![1, 2], tnmod);
    insert!(tnmod, new_group!("a"));
    find_wf_method!(Group, vec![1], find!(1), tnmod);
    fetch_method!(Group, vec![1], 1 , tnmod);
}

#[test]
fn add_person_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));

    let met = to_method!(
        add_person,
        "first".into(),
        "first".into(),
        "last".into(),
        "middle".into(),
        RowId(1),
        RowId(0)
    );
    macro_call_meth!(tnmod, met, last_rowid!(tnmod));
}


#[test]
fn update_str_group_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));

    update_str_method!(Group, 1, 0, "new_a", tnmod);

}


#[test]
fn update_int_group_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));

    update_int_method!(Group, 1, 1, 2, "second", tnmod);
}

#[test]
fn find_and_fetch_group_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    find_method!(Group, vec![1, 2], tnmod);
    insert!(tnmod, new_group!("a"));
    find_method!(Group, vec![1, 2, 3], tnmod);
    fetch_method!(Group, vec![1, 2, 3], 3 , tnmod);
}

#[test]
fn find_finder_and_fetch_group_test() {
    init_db!(tnmod, new_group!("primary"), new_group!("second"));
    find_method!(Group, vec![1, 2], tnmod);
    insert!(tnmod, new_group!("a"));
    find_wf_method!(Group, vec![1], find!(1), tnmod);
    fetch_method!(Group, vec![2], 1 , tnmod);
}
