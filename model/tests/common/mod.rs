pub use model::entity::*;
pub  use model::*;
pub  use tnsqlite3::{Connection, Insert, Mapping, Mapped, params, select_tuple };
//use model::stypes::RowId;
pub  use std::{error, fmt, result};
pub  use serde_json;



pub fn text_column_count<F: FilterKit>(model: &mut TNModel<F>) -> Vec<String> {
    let conn = model.db_connection();
    let stmt = "SELECT sql FROM sqlite_master  WHERE type='table' AND name='text_table'";
//    conn.execute(stmt, &[]).expect("SELECT sql FROM sqlite_master  WHERE type='table' AND name='text_table");
    let sql: String = conn
        .prepare(stmt, &[]).expect("SELECT sql FROM sqlite_master  WHERE type='table' AND name='text_table")
        .query(String::mapping)
        .expect("text_column_count, zero lenght vector")
        .fallible_collect()
        .expect("text_column_count, fallible_collect").pop().unwrap_or_default();
    let mut columns = Vec::new();
    let mut old = "";
    for w in sql.split_whitespace() {

    if w.len() >= 4 &&  &w[..4] == "TEXT" {
        columns.push( old.to_string() )
    }
    old = w;

    }
    columns
}

pub fn ecolumn_row_count<F: FilterKit>(model: &mut TNModel<F>) -> i64 {
    let conn = model.db_connection();
    let count: i64 = conn
        .prepare("SELECT COUNT(*) FROM ecolumn", &[]).expect("SELECT COUNT(*) FROM ecolumn")
        .query(i64::mapping)
        .expect("query_map, zero lenght vector")
        .fallible_collect()
        .expect("ecolumn_row_count, zero lenght vector").pop().unwrap_or_default();
    count
}

#[derive(Debug)]
struct RRow {
 task_id: i64,
 col: Vec<Option<String>>
}

pub fn print_table<F: FilterKit>(model: &mut TNModel<F>) {
    let conn = model.db_connection();
    let stmt = "SELECT * FROM  text_table";
    let p_stmt = conn.prepare(stmt, &[]).unwrap();
    let mut qi = p_stmt.query_row().unwrap();
    println!("^^^^^^^^^^^^^^^^^^^^^^^^^");
    while let Some(x) = qi.next_row() {
        x.unwrap();
        let mut rrow = RRow {task_id: qi.get(0).unwrap().unwrap(), col: Vec::new()};
        for i in 1..qi.count() {
            let s  = qi.get(i).unwrap();
            rrow.col.push(s);
        }
        println!("{:?}", &rrow);

}}

pub fn print_map_table<T, F: FilterKit>(model: &mut TNModel<F>)
where T: Mapping,
 <T as Mapping>::Target: fmt::Debug
{
    let conn = model.db_connection();
    let stmt = "SELECT * FROM  person";
    let p_stmt =  conn.prepare(stmt, &[]).unwrap();
    for r in p_stmt.query(T::mapping).unwrap()  {
        println!("{:?}", r.unwrap());
    }

}


pub fn print_task<T, F: FilterKit>(model: &mut TNModel<F>)
where T: Mapping,
 <T as Mapping>::Target: fmt::Debug
{
    let conn = model.db_connection();
    let stmt = "SELECT * FROM  task";
    let p_stmt =  conn.prepare(stmt, &[]).unwrap();
    for r in p_stmt.query(T::mapping).unwrap()  {
        println!("{:?}", r.unwrap());
    }

}

pub fn count_perm_group<F: FilterKit>(pmask_id: i64, group_id: i64, model: &mut TNModel<F>)
 -> Vec<u8>
{
    let conn = model.db_connection();
    let stmt = "SELECT perm FROM  perm_group WHERE pmask_id=? AND group_id=?";
    let p_stmt =  conn.prepare(stmt, params!(pmask_id, group_id)).unwrap();
    let mut  v =  p_stmt.query(select_tuple!(Vec<u8>))
    .expect("count_perm_group p_stmt.query")
    .fallible_collect().expect("fallible_collect");
        v.remove(0)
}


pub fn prio_seq<F: FilterKit>(exe_id: i64, model: &mut TNModel<F>) -> Vec<(i64, i64)> {
    let conn = model.db_connection();
    let stmt = "SELECT task.id, task.prio FROM task
                INNER JOIN status AS s ON s.id = task.status_id
                WHERE  (s.prio_flag = 1) AND performer_id = ?
                ORDER BY task.prio";
    let p_stmt =  conn.prepare(stmt, params!(exe_id)).expect("prio_seq  execute");
    let mut query = p_stmt.query( select_tuple!(i64, i64) ).expect("prio_seq query_map");
    query.fallible_collect().expect("prio_seq fallible_collect " )
}

pub fn check_prio_seq_and_prio(task_id: i64, task_prio: i64, seq: &[(i64, i64)]) {
    let mut prev_prio = -1;
    for  &( id, prio) in seq {
        assert_eq!(prio, prev_prio+1);
        prev_prio += 1;
        if task_id == id {
          assert_eq!(task_prio, prio) }
    }
}

pub fn check_prio_seq(seq: &[(i64, i64)]) {
    println!("{:?}", seq);
    let mut prev_prio = -1;
    for  &( id, prio) in seq {
        assert_eq!(prio, prev_prio+1);
        prev_prio += 1;
    }
}


pub fn exe_id_from_task_id<F: FilterKit>(task_id: i64, model: &mut TNModel<F>) -> i64 {
    let conn = model.db_connection();
    let stmt = "SELECT performer_id FROM task WHERE id = ?";
    let p_stmt =  conn.prepare(stmt, params!(task_id)).expect("exe_id_from_task_id  execute");
    let mut query = p_stmt.query( select_tuple!(i64) ).expect("exe_id_from_task_id query_map");

    query.next().expect(" exe_id_from_task_id next").expect("exe_id_from_task_id, expect 2 ")

}

pub fn create_conn(_: &str) -> result::Result<Connection, tnsqlite3::Error> {
   let mut conn =   Connection::new(":memory:")?;
   conn.register_cdatetime_func()?;
   Ok(conn)
}

#[macro_export]
macro_rules! init_db {
    ($model:ident,  $($ent:expr),*) => {
        let context = Context::from_permission( PersonPermission::from(Privileges::Ordinary) );
        let mut $model = TNModel::<PermissiveFilters>::with_context("", create_conn, context).unwrap();
//        $model.create_tables().unwrap();

        let db = $model.db_connection();
        create_tables( db ).unwrap();
        $($ent.insert(db).unwrap();)*
}

}

#[macro_export]
macro_rules! init_db_admin {
    ($model:ident,  $($ent:expr),*) => {
        let context = Context::from_permission( PersonPermission::from(Privileges::Administration) );
        let mut $model = TNModel::<PermissiveFilters>::with_context("", create_conn, context).unwrap();
//        $model.create_tables().unwrap();

        let db = $model.db_connection();
        create_tables( db ).unwrap();
        $($ent.insert(db).unwrap();)*
}

}

#[macro_export]
macro_rules! init_db_context {
    ($model:ident,  $($ent:expr),*) => {
        let mut $model = TNModel::<Filters>::new( create_conn).unwrap();
        $model.create_tables().unwrap();
        let db = $model.db_connection();
        create_tables(  ).unwrap();
        $($ent.insert(db).unwrap();)*
}

}

#[macro_export]
macro_rules! insert {
    ($model:ident, $($ent:expr),+) => {
    let  db = $model.db_connection();
    $($ent.insert(db).unwrap();)+
    }
}

#[macro_export]
macro_rules! to_method {
    ($method:tt,  $($param:expr),+) => {
        method!( $method,  ( $($param),+,  ))
//       Method::new(MethodKind::$method,
//                ParamsKind::$paramkind(( $($param),+,  )) )
    }
}

#[macro_export]
macro_rules! err {
    ($e:expr) => {err($e)};
}


#[macro_export]
macro_rules! macro_call_meth {
    ($model:ident, $meth:expr, $expect:expr) => {
        let res = $model.request($meth).unwrap();
        match res {
            OutObject::Response(Response { result, .. }) => { assert_eq!(result, $expect);
                }
            OutObject::Error(e) => panic!(format!("{:?}", e)),
        }
    };

    ($model:ident, $meth:expr, err  $expect:expr) => {
        let res = $model.request($meth).unwrap();
        match res {
            OutObject::Response(Response { result, .. }) => panic!(format!("{:?}", result)),
            OutObject::Error(ErrObject{error: ComError {code,.. }, ..  }) =>
                { assert_eq!(code, $expect);  }
        }
    };
}
#[macro_export]
macro_rules! macro_call_meth_map {
    ($model:ident, $meth:expr, $expect:expr) => {
        let res = $model.request($meth).unwrap();
        dbg!(&res);
        match res {
            OutObject::Response(Response { result, .. }) => {
//                         print!("result:{:?}", &result) ;
                    $expect(result) ;
              }
            OutObject::Error(e) => panic!(format!("{:?}", e)),
        }
    };
}

#[macro_export]
macro_rules! macro_call_meth_return {
    ($model:ident, $meth:expr) => {{
        let res = $model.request($meth).unwrap();
        match res {
            OutObject::Response(Response { result, .. }) =>  result ,
            OutObject::Error(e) => panic!(format!("{:?}", e)),
        }
    }};
}

#[macro_export]
macro_rules! last_rowid {
    ($model:ident) => {{
        let db = $model.db_connection();
        Payload::Id(RowId(db.last_insert_rowid()))
    }};
}
#[macro_export]
macro_rules! new_group {
    ( $name:expr ) => {
        Group {
            id: 0,
            name: $name.to_string(),
            parent: None
        }
    };
}

#[macro_export]
macro_rules! new_group_w_parent {
    ( $name:expr, $parent:expr ) => {
        Group {
            id: 0,
            name: $name.to_string(),
            parent: Some( $parent)
        }
    };
}

#[macro_export]
macro_rules! find_method {
    ($ent:tt, $num_vec:expr,  $model:ident) => {

        let find = method!( find, (TableKind::$ent,  Finder::new()) );
        let r: Vec<_> = $num_vec.into_iter().map(|i| RowId(i)).collect();
        macro_call_meth!($model, find, Payload::Finded(r));

    };
}
#[macro_export]
macro_rules! find_wf_method {
    ($ent:tt, $num_vec:expr, $finder:expr,  $model:ident) => {
        let find = method!( find, (TableKind::$ent,  $finder) );
        let r: Vec<_> = $num_vec.into_iter().map(|i| RowId(i) ).collect();
        macro_call_meth!($model, find, Payload::Finded(r));

    };
}



#[macro_export]
macro_rules! fetch_method {
    ($ent:tt, $num_vec:expr, $expect:expr, $model:ident) => {
        let r: Vec<_> = $num_vec.into_iter().map(|i| RowId(i) ).collect();
        let fetch = method!( fetch_data, (TableKind::$ent,  r) );
        macro_call_meth_map!($model, fetch, |result| {
            match result {
                Payload::Rows(x) => {  println!("{:?}", &x);
                           println!("{}",  serde_json::to_string(&x).unwrap() );
                        assert_eq!(x.len(), $expect);
               },
                _ => panic!("Not Rows variants"),
            }
        });
    };
}



#[macro_export]
macro_rules! perm_column_compare {
    ($model:ident, $meth:expr, $expect:expr) => {
        let res = $model.request($meth).unwrap();
        dbg!(&res);
        match res {
            OutObject::Response(Response { Payload::Rows(result), .. }) => {
//                         print!("result:{:?}", &result) ;
                    assert_eq!(result) ;
              }
            OutObject::Error(e) => panic!(format!("{:?}", e)),
        }
    };
}


#[macro_export]
macro_rules! fetch_method_perm_check {
    ($ent:tt, $num_vec:expr, $expect:expr, $model:ident) => {
        let r: Vec<_> = $num_vec.into_iter().map(|i| RowId(i) ).collect();
        let fetch = method!( fetch_data, (TableKind::$ent,  r) );
        macro_call_meth_map!($model, fetch, |result| {
            match result {
                Payload::Rows(x) => {  println!("{:?}", &x);
                           println!("{}",  serde_json::to_string(&x).unwrap() );
                        assert_eq!(x.len(), $expect);
               },
                _ => panic!("Not Rows variants"),
            }
        });
    };
}

#[macro_export]
macro_rules! fetch_first_row {
    ( $ent:tt,  $model:ident) => {{
          let find = method!( find, (TableKind::$ent,  Finder::new()) );
        macro_call_meth_return!($model, find);
        let meth = method!( fetch_data, (TableKind::$ent, vec![RowId(1)]) );
        match  macro_call_meth_return!($model, meth) {
           Payload::Rows(x) => x,
            _ => panic!("Not Rows varioan of Payload"),
        }.pop().expect("Vec zero lenght")
    }};
}

#[macro_export]
macro_rules! fetch_num_row {
    ( $ent:tt,  $num:expr, $model:ident) => {{
          let find = method!( find, (TableKind::$ent,  Finder::new()) );
        macro_call_meth_return!($model, find);
        let meth = method!( fetch_data, (TableKind::$ent, vec![RowId($num)]) );
        match  macro_call_meth_return!($model, meth) {
           Payload::Rows(x) => x,
            _ => panic!("Not Rows varioan of Payload"),
        }.pop().expect("Vec zero lenght")
    }};
}


#[macro_export]
macro_rules! column_type_method {
    ($ent:tt, $rowid:expr, $num:expr, $ctype:expr,  $model:ident) => {
        let meth = method!( column_type, (TableKind::$ent, RowId( $rowid), $num) );
        macro_call_meth!($model, meth, Payload::ColumnType($ctype));

    };
}
#[macro_export]
macro_rules! column_variants_method {
    ($ent:tt, $column:expr, $num_expect:expr,  $model:ident) => {
        let meth = method!( column_variants, (TableKind::$ent, $column) );
        macro_call_meth_map!($model, meth, | result | {
        match result {
            Payload::Rows(x) => {  println!("{:?}", &x); assert_eq!(x.len(), $num_expect);
           },
            _ => panic!("Not Rows variant of Payload"),
        }
});

    };
}
#[macro_export]
macro_rules! update_str_method {
    ($ent:tt, $id:expr, $column:expr, $value:expr,  $model:ident) => {
        let meth = method!( update_str, (TableKind::$ent, RowId($id), $column, $value.to_string()) );
        let mut row_src = fetch_first_row!($ent, $model);
        row_src.columns[$column as usize] = $value.to_string();
        macro_call_meth!($model, meth, Payload::Ok);
        let row = fetch_first_row!($ent, $model);
//        print_map_table(&mut $model);
        assert_eq!(row, row_src);

    };
}


#[macro_export]
macro_rules! update_int_method {
    ($ent:tt, $id:expr, $column:expr, $value:expr,  $expect:expr, $model:ident) => {
        let meth = method!( update_int, (TableKind::$ent, RowId($id), $column, RowId($value)) );

        let mut row_src = fetch_first_row!($ent, $model);
        row_src.columns[$column] =  $expect.to_string();
        macro_call_meth!($model, meth, Payload::Ok);
        let row = fetch_first_row!($ent, $model);
        assert_eq!(row, row_src);
    };

    ($ent:tt, $id:expr, $column:expr, $value:expr, err $expect:expr, $model:ident) => {
        let meth = method!( update_int, (TableKind::$ent, RowId($id), $column, RowId($value)) );

        let mut row_src = fetch_first_row!($ent, $model);
        macro_call_meth!($model, meth, err $expect);
        let row = fetch_first_row!($ent, $model);
        assert_eq!(row, row_src);
    };

    ($ent:tt, $id:expr, $column:expr, $value:expr, err $expect:expr, id= $row:expr, $model:ident) => {
        let meth = method!( update_int, (TableKind::$ent, RowId($id), $column, RowId($value)) );

        let mut row_src = fetch_num_row!($ent, $row,  $model);
        macro_call_meth!($model, meth, err $expect);
        let row = fetch_num_row!($ent, $row, $model);
        assert_eq!(row, row_src);
    };

}

#[macro_export]
macro_rules! update_int_perm_group_method {
    ($ent:tt, $id:expr, $column:expr, $value:expr,  $expect:expr, $model:ident) => {
        let meth = method!( update_int, (TableKind::$ent, RowId($id), $column, RowId($value)) );

        let mut row_src = fetch_first_group_row!($ent, $model);
        row_src.columns[$column] =  $expect.to_string();
        macro_call_meth!($model, meth, Payload::Ok);
        let row = fetch_first_group_row!($ent, $model);
        assert_eq!(row, row_src);

    };
}

#[macro_export]
macro_rules! task_perm_vec {
    ($id:expr,  $model:ident) => {{

        let meth = method!( fetch_data, (TableKind::Task, vec![RowId($id)]) );
        match  macro_call_meth_return!($model, meth) {
           Payload::Rows(x) =>  x[0].meta.c.clone() ,
            _ => panic!("Not Rows varioan of Payload"),
        }
    }};
}

#[macro_export]
macro_rules! update_int_perm_group_and_task_check_method {
    ($id:expr, $task_id:expr,  $column:expr, $value:expr,  $expect:expr, $model:ident) => {
        let meth = method!( update_int, (TableKind::PermGroup, RowId($id), $column+1, RowId($value)) );
        let mut src_perm_vec = task_perm_vec!($task_id, $model)
        let mut row_src = fetch_first_group_row!(PermGroup, $model);
        row_src.columns[$column+1] =  $expect.to_string();
        src_perm_vec[$column].permission = Permission::from($value);
        macro_call_meth!($model, meth, Payload::Ok);
        let row = fetch_first_group_row!(PermGroup, $model);
        let perm_vec = task_perm_vec!($task_id, $model)
        assert_eq!(row, row_src);
        assert_eq!(perm_vec, src_perm_vec);

    };
}

#[macro_export]
macro_rules! fetch_first_group_row {
    ( $ent:tt, $model:ident) => {{
        let meth = method!( fetch_data, (TableKind::$ent, vec![RowId(1)]) );
        match  macro_call_meth_return!($model, meth) {
           Payload::Rows(x) => x,
            _ => panic!("Not Rows varioan of Payload"),
        }.pop().expect("Vec zero lenght")
    }};
}


#[macro_export]
macro_rules! update_performer_column_task_method {
    ($id:expr,  $performer_id:expr,  $model:ident) => {
        let meth = method!( update_int, (TableKind::Task, RowId($id), 1, RowId($performer_id)) );

        let last_exe_id = exe_id_from_task_id($id, &mut   $model  );

        macro_call_meth!($model, meth, Payload::Ok);

        let ps = prio_seq(last_exe_id, &mut $model  );
//        println!("{:?}", &ps);
        check_prio_seq( &ps );

        let ps = prio_seq($performer_id, &mut $model  );
//        println!("{:?}", &ps);
        check_prio_seq( &ps );

        print_task::<Task, PermissiveFilters>(&mut $model);
    };
}

#[macro_export]
macro_rules! update_status_column_task_method {
    ($id:expr,  $status_id:expr,  $model:ident) => {
        let meth = method!( update_int, (TableKind::Task, RowId($id), 2, RowId($status_id)) );

        let last_exe_id = exe_id_from_task_id($id, &mut   $model  );

        macro_call_meth!($model, meth, Payload::Ok);

        let ps = prio_seq(last_exe_id, &mut $model  );
//        println!("{:?}", &ps);
        check_prio_seq( &ps );

        let exe_id = exe_id_from_task_id($id, &mut   $model  );
        let ps = prio_seq(exe_id, &mut $model  );
//        println!("{:?}", &ps);
        check_prio_seq( &ps );

        print_task::<Task, PermissiveFilters>(&mut $model);
    };
}

#[macro_export]
macro_rules! add_columns_method {
    ($ent:tt,  $model:ident, $($column:expr),+) => {
       let count =  vec![ $($column.to_string()),+].len();
        let meth = method!( add_columns, (TableKind::$ent, vec![ $($column.to_string()),+]) );

        let columns_count_before = text_column_count(&mut $model).len();
        let ecolun_row_count_before = ecolumn_row_count(&mut $model);
        macro_call_meth!($model, meth, Payload::Ok);
        let columns_count_after = text_column_count(&mut $model).len();
        let ecolun_row_count_after = ecolumn_row_count(&mut $model);
        assert_eq!(columns_count_before + count, columns_count_after);
        assert_eq!(ecolun_row_count_before + count as i64, ecolun_row_count_after);

    };
}

#[macro_export]
macro_rules! delete_column_method {
    ($ent:tt,  $model:ident, $idx:expr) => {
        let meth = method!( delete_column, (TableKind::$ent, $idx) );
        let columns_count_before = text_column_count(&mut $model).len();
        let ecolun_row_count_before = ecolumn_row_count(&mut $model);
        macro_call_meth!($model, meth, Payload::Ok);
        let columns_count_after = text_column_count(&mut $model).len();
        let ecolun_row_count_after = ecolumn_row_count(&mut $model);
        assert_eq!(columns_count_before -1, columns_count_after );
        assert_eq!(ecolun_row_count_before -1 , ecolun_row_count_after);

    };
}

#[macro_export]
macro_rules! prio_for_executor_method {
    ( $exe_id:expr, $expect:expr, $model:ident) => {
        let meth = method!(prio_for_executor, (RowId($exe_id),));

        let sample: Vec<i32> = if let Some(num) = $expect {
            let num : i32 = num;
            (0..(num + 1)).collect()
        } else {
            vec![]
        };
        macro_call_meth!($model, meth, Payload::Priority(sample));
    };
}

#[macro_export]
macro_rules! update_prio_method {
    ( $task_rowid:expr, $prio:expr, $model:ident) => {
        let meth = method!( update_prio, (RowId( $task_rowid) , $prio) );

        macro_call_meth!($model, meth, Payload::Ok );

        let exe_id = exe_id_from_task_id($task_rowid, &mut   $model  );
        let ps = prio_seq(exe_id, &mut $model  );
//        println!("{:?}", &ps);
        check_prio_seq_and_prio($task_rowid, $prio, &ps );

    };
}

#[macro_export]
macro_rules! authenticate_method {
    ( $login:expr, $hashsum:expr, $id_expect:expr, $perm_expect:expr, $model:ident) => {
        let meth = method!( authenticate, ( $login.to_string() , $hashsum.to_string() ) );
        macro_call_meth!($model, meth, Payload::Permission((RowId( $id_expect), RowId($perm_expect) )) );
    };
}

#[macro_export]
macro_rules! set_password_method {
    ( $login:expr, $hashsum:expr,  $model:ident) => {
        let meth = method!( set_password, ( $login.to_string() , $hashsum.to_string()) );
        macro_call_meth!($model, meth, Payload::Ok );
    };
}

#[macro_export]
macro_rules! change_password_method {
    ( $login:expr, $old_hashsum:expr, $new_hashsum:expr,  $model:ident) => {
        let meth = method!( change_password, ( $login.to_string() , $old_hashsum.to_string(),
        $new_hashsum.to_string()) );
        macro_call_meth!($model, meth, Payload::Ok );
    };
}

#[macro_export]
macro_rules! admin_count_method {
    ( $count:expr,  $model:ident) => {
        let meth = method!( admin_count, () );
        println!("{:?}", &meth);
        macro_call_meth!($model, meth, Payload::Id( RowId($count) ) );
    };
}

#[macro_export]
macro_rules! add_task_method {
    ( $owner_id:expr, $performer_id:expr,  $status_id:expr, $start_time:expr,
    $deadline:expr, $prio:expr, $pmask:expr, $expect:expr, $model:ident) => {

        let task = new_task_wprio!( $owner_id, $performer_id,  $status_id, $start_time,
                $deadline, $prio, $pmask );

        let meth = method!( add_task, (  RowId($performer_id), RowId($status_id),
      RowId($start_time),  RowId($deadline), $prio,  RowId( $pmask) ) );
        macro_call_meth!($model, meth,  Payload::Id( RowId( $expect) ) );
        let ps = prio_seq($performer_id, &mut $model  );
//        println!("{:?}", &ps);
        check_prio_seq_and_prio($expect, $prio, &ps );
    };
}

#[macro_export]
macro_rules! add_perm_group_method {
    ( $pmask_id:expr,  $group_id:expr, $expect:expr,  $model:ident) => {

        let meth = method!( add_perm_group, (  RowId($pmask_id), RowId($group_id)) );
        macro_call_meth!($model, meth,  Payload::Id( RowId( $expect ) ) );
    };
}




#[macro_export]
macro_rules! new_person {
    ( $fname:expr,  $lname:expr,  $mname:expr,  $gr_base:expr) => {
        model::entity::Person {
            id: 0,
            login: $fname.to_string(),
            pass_hashsum: Some("111".to_string()),
            fname: $fname.to_string(),
            lname: $lname.to_string(),
            mname: $mname.to_string(),
            gr_base: $gr_base,
            gr_1: None,
            gr_2: None,
            perm: Privileges::Ordinary.into(),
        }
    };
}

#[macro_export]
macro_rules! new_person_perm {
    ( $fname:expr,  $lname:expr,  $mname:expr, $perm:expr,  $gr_base:expr) => {
        model::entity::Person {
            id: 0,
            login: $fname.to_string(),
            pass_hashsum: Some("111".to_string()),
            fname: $fname.to_string(),
            lname: $lname.to_string(),
            mname: $mname.to_string(),
            gr_base: $gr_base,
            gr_1: None,
            gr_2: None,
            perm: $perm,
        }
    };
}

#[macro_export]
macro_rules! new_status {
    ($name:expr) => {
        model::entity::Status {
            id: 0,
            name: $name.to_string(),
            prio_flag: 0,
        }
    };
}

#[macro_export]
macro_rules! new_task {
    ($owner_id:expr, $performer_id:expr,  $status_id:expr, $start_time:expr, $deadline:expr, $pmask:expr) => {
        model::entity::Task {
            id: 0,
            owner_id: $owner_id,
            performer_id: $performer_id,
            status_id: $status_id,
            start_time:  $start_time,
            create_time: 55555,
            deadline: $deadline,
            prio: 0,
            pmask_id: $pmask,
        }
    };
}

#[macro_export]
macro_rules! new_status_wprio {
    ($name:expr, $prio:expr) => {
        model::entity::Status {
            id: 0,
            name: $name.to_string(),
            prio_flag: $prio,
        }
    };
}

#[macro_export]
macro_rules! new_task_wprio {
    ($owner_id:expr, $performer_id:expr,  $status_id:expr, $start_time:expr,
        $deadline:expr, $prio:expr, $pmask:expr) => {
        model::entity::Task {
            id: 0,
            owner_id: $owner_id,
            performer_id: $performer_id,
            status_id: $status_id,
            start_time:  $start_time,
            create_time: 55555,
            deadline: $deadline,
            prio: $prio,
            pmask_id: $pmask,
        }
    };
}

#[macro_export]
macro_rules! new_perm_mask {
    ($name:expr, $owner_id:expr, $group_id:expr) => {
        model::entity::PermMask {
            id: 0,
            name: $name.to_string(),
            owner_id: $owner_id,
            group_id: $group_id,
        }
    };
}

#[macro_export]
macro_rules! new_perm_group {
    ($name:expr, $owner_id:expr, $group_id:expr, $perm:expr) => {
        model::entity::PermGroup {
            id: 0,
            pmask_id: $pmask_id,
            column: $column,
            group_id: $group_id,
            perm: $perm,
        }
    };
}

#[macro_export]
macro_rules! new_ecolumn {
    ($name:expr) => {
        model::entity::EColumn {
            id: 0,
            name: $name.to_string(),
        }
    };
}
