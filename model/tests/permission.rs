mod common;
use common::*;


#[test]
fn task_column_perm_test() {
init_db!(
    tnmod,
    new_group!("group 1"),
    new_group!("group 2"),
    new_status!("stat 1")
);
insert!(tnmod, new_person!("Dow", "John", "AA", 1));
insert!(tnmod, new_person!("Dow2", "John2", "AA2", 1));

insert!(tnmod, new_perm_mask!("mask1", 1, 1));
add_perm_group_method!(1, 1, 1, tnmod);
insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

update_int_perm_group_method!(PermGroup, 1, 1, Permission::RO as i64, "RO", tnmod);

update_int_method!(Task, 1, 0, 2, err CodeError::WriteAccessForbid, tnmod);
update_int_perm_group_method!(PermGroup, 1, 1, Permission::RW as i64, "RW", tnmod);
update_int_method!(Task, 1, 0, 2, "John2 D. A.", tnmod);

update_int_perm_group_method!(PermGroup, 1, 3, Permission::RO as i64, "RO", tnmod);
update_int_method!(Task, 1, 2, 2, err CodeError::WriteAccessForbid, tnmod);


}

//#[test]
//fn task_column_perm_admin_test() {
//init_db_admin!(
//    tnmod,
//    new_group!("group 1"),
//    new_group!("group 2"),
//    new_status!("stat 1")
//);
//insert!(tnmod, new_person_perm!("Dow", "John", "AA", 0b111, 1));
//insert!(tnmod, new_person_perm!("Dow2", "John2", "AA2",0b111,  1));

//insert!(tnmod, new_perm_mask!("mask1", 1, 1));
//add_perm_group_method!(1, 1, 1, tnmod);
//insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));

//update_int_perm_group_method!(PermGroup, 1, 1, Permission::RO as i64, "RO", tnmod);

//update_int_method!(Task, 1, 0, 2, "John2 D. A.", tnmod);
//}


#[test]
fn task_column_perm_case_1_test() {
init_db!(
    tnmod,
    new_group!("group 1"),
    new_group!("group 2"),
    new_status!("stat 1")
);
insert!(tnmod, new_person_perm!("Dow", "John", "AA", 1, 1));
insert!(tnmod, new_person_perm!("Dow2", "John2", "AA2",1,  1));

insert!(tnmod, new_perm_mask!("mask1", 1, 1));
add_perm_group_method!(1, 1, 1, tnmod);
insert!(tnmod, new_task!(1, 1, 1, 41242, 235235, 1));


update_int_method!(Task, 1, 0, 2, "John2 D. A.", tnmod);

}
