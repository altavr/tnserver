use std::iter::Iterator;
use std::marker::PhantomData;
use std::sync::{Arc, Mutex, MutexGuard};
use std::ptr;
use std::{fmt,  ptr::NonNull};

pub struct Arena<T> {
    inner: Arc<Mutex<ArenaInner<T>>>,
}

impl<T> Arena<T>
where
    T: Clone + Passthrough + Reset,
{
    pub fn new(v: T, size: usize) -> Self {
        Arena {
            inner: Arc::new(Mutex::new(ArenaInner::new(v, size))),
        }
    }
    pub fn from_arena(arena: &Arena<T>) -> Self {
        Arena {inner: Arc::clone(&arena.inner)}
}

    pub fn chunk(&mut self) -> Option<Chunk<T>> {
        let mut ai = self.inner.lock().unwrap();
        if let Some(mut c) = ai.pull_chunk() {
            c.arena = Some(Arc::clone(&self.inner));
            Some(c)
        } else {
            None
        }
    }

    pub fn iter(&mut self) -> ArenaIter<T> {
        let ai = self.inner.lock().unwrap();
        ArenaIter {
            arena: ai,
            arc: Arc::clone(&self.inner),
        }
    }
}

struct ArenaInner<T> {
    rchunks: Vec<T>,
    free_idx: Vec<usize>,
}

impl<T> ArenaInner<T>
where
    T: Clone + Passthrough + Reset,
{
    fn new(v: T, size: usize) -> Self {
        //        assert!(size % 32usize == 0);
        let rchunks = vec![v; size];
        let free_idx = (0..size).into_iter().collect();
        ArenaInner { rchunks, free_idx }
    }

    fn push_chunk_idx(&mut self, idx: usize) {
        self.free_idx.push(idx);
    }

    fn pull_chunk(&mut self) -> Option<Chunk<T>> {
        if let Some(idx) = self.free_idx.pop() {
            Some(self.pull_chunk_idx(idx))
        } else {
            None
        }
    }

    fn pull_chunk_idx(&mut self, idx: usize) -> Chunk<T> {
        let mut ptr = NonNull::new(&mut self.rchunks[idx]).unwrap();
        unsafe { ptr.as_mut().reset() };
        //Fill zeros
//        unsafe {ptr::write_bytes(ptr.as_ptr(), 0, 1) };
        Chunk::new(ptr, idx)
    }
}

pub struct ArenaIter<'a, T> {
    arena: MutexGuard<'a, ArenaInner<T>>,
    arc: Arc<Mutex<ArenaInner<T>>>,
}

impl<'a, T> Iterator for ArenaIter<'a, T>
where
    T: Clone + Passthrough + Reset,
{
    type Item = Chunk<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(mut c) = self.arena.pull_chunk() {
            c.arena = Some(Arc::clone(&self.arc));
            Some(c)
        } else {
            None
        }
    }
}

impl<T> fmt::Debug for Chunk<T>
where
    T: Clone + fmt::Debug + Passthrough + Reset,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Chunk: {:?}", &self.inner_ref())
    }
}

pub trait Reset {
    fn reset(&mut self);
}

pub trait Passthrough {
    type Output: ?Sized;
    fn passthrough_mut(&mut self) -> &mut Self::Output;
    fn passthrough_ref(&self) -> &Self::Output;
}

impl<T> Passthrough for T
where
    T: AsMut<[u8]> + AsRef<[u8]>,
{
    type Output = [u8];
    fn passthrough_mut(&mut self) -> &mut Self::Output {
        self.as_mut()
    }
    fn passthrough_ref(&self) -> &Self::Output {
        self.as_ref()
    }
}

unsafe impl<T> Send for Chunk<T> where
T: Clone + Passthrough  + Reset, {}

pub struct Chunk<T>
where
    T: Clone + Passthrough + Reset,
{
    inner: NonNull<T>,
    bitidx: usize,
    arena: Option<Arc<Mutex<ArenaInner<T>>>>,
}

impl<T> Chunk<T>
where
    T: Clone + Passthrough + Reset,
{
    fn new(inner: NonNull<T>, bitidx: usize) -> Self {
        Chunk {
            inner,
            bitidx,
            arena: None,
        }
    }

    pub fn inner_mut(&mut self) -> &mut T {
        unsafe { self.inner.as_mut() }
    }
    pub fn inner_ref(&self) -> &T {
        unsafe { self.inner.as_ref() }
    }
}

impl<T> Drop for Chunk<T>
where
    T: Clone + Passthrough + Reset ,
{
    fn drop(&mut self) {
        if let Some(arena) = &self.arena {
            let mut ai = arena.lock().unwrap();
            ai.push_chunk_idx(self.bitidx);
        }
    }
}

impl<T> AsRef<T::Output> for Chunk<T>
where
    T: Clone + Passthrough + Reset,
{
    fn as_ref(&self) -> &T::Output {
        self.inner_ref().passthrough_ref()
    }
}

impl<T> AsMut<T::Output> for Chunk<T>
where
    T: Clone + Passthrough + Reset,
{
    fn as_mut(&mut self) -> &mut T::Output {
        self.inner_mut().passthrough_mut()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Clone, Debug)]
    struct SS {
        buf: Seq,
        a: u32,
    }

    impl Reset for SS {
        fn reset(&mut self) { self.a = 0}
}

    type Seq = [u8; 32];

    impl Passthrough for SS {
        type Output = [u8];
        fn passthrough_mut(&mut self) -> &mut Self::Output {
            &mut self.buf[..]
        }
        fn passthrough_ref(&self) -> &Self::Output {
            &self.buf[..]
        }
    }

    #[test]
    fn arena() {
        let ec = SS { buf: [0; 32], a: 0 };

        let mut arena = Arena::new(ec, 64);
        let mut chunks = arena.iter().collect::<Vec<_>>();
        for (i, c) in chunks.iter_mut().enumerate() {
            c.as_mut().copy_from_slice(&[i as u8; 32]);
            dbg!("cycle");
        }
        println!("chunks: {:?}", &chunks);
    }
}
