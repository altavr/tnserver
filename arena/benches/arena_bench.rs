use arena::{Arena, Chunk, Reset};
use criterion::{black_box, criterion_group, criterion_main, Criterion};

const LEN:usize = 4096;

#[derive(Clone)]
struct Arr( [u8; LEN]);

impl Arr {
    fn new() -> Self { Arr([0;LEN]) }
}

impl AsRef<[u8]> for Arr {
    fn as_ref(&self) -> &[u8] {
        &self.0[..]
}
}

impl AsMut<[u8]> for Arr {
    fn as_mut(&mut self) -> &mut [u8] {
        &mut self.0[..]
}
}

impl Reset for Arr {
    fn reset(&mut self) { self.0[0] = 0}
}

//pub fn criterion_benchmark(c: &mut Criterion) {
//    c.bench_function("fib 20", |b| b.iter(|| fibonacci(black_box(20))));
//}

pub fn criterion_benchmark(c: &mut Criterion) {
    let ec = Arr::new();
    let mut arena = Arena::new(ec, 64);
    //    let mut chunks = arena.into_iter().unwrap().collect::<Vec<_>>();
    //    for (i, c) in chunks.iter_mut().enumerate() {
    //        c.copy_from_slice(&[i as u8; 4]);
    //    }

    c.bench_function("arena alloc", |b| {
        b.iter(|| {
            arena.chunk()
        } )
    });
}

pub fn criterion_benchmark_vec(c: &mut Criterion) {
    //    let ec: Seq = [0; 4];

    c.bench_function("vec alloc",
    |b| b.iter(||
//    vec![0; LEN].into_iter().cycle().take(64).collect::<Vec<_>>()
    vec![0u8; LEN]
//    for _ in 0..64 { vec![0u8; 40960]; }
));
}

criterion_group!(benches, criterion_benchmark, criterion_benchmark_vec);
criterion_main!(benches);
