//#![allow(unused)]

use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use std::marker::PhantomData;

#[derive(Deserialize)]
pub struct FullTextSearch {}

pub trait ToColumnName: Clone {
    fn to_column_name<'a>(self, columns: &'a [&str]) -> &'a str;
}

impl ToColumnName for &'static str {
    fn to_column_name<'a>(self, _: &'a [&str]) -> &'a str {
        self
    }
}

impl ToColumnName for usize {
    fn to_column_name<'a>(self, columns: &'a [&str]) -> &'a str {
        if self == 0 {
            "id"
        } else {
            columns[self - 1]
        }
    }
}

#[derive(Deserialize)]
enum SortOrder {
    Asc,
    Desc,
}

impl SortOrder {
    pub fn to_expr(&self) -> &str {
        use SortOrder::*;
        match self {
            Asc => "",
            Desc => "DESC",
        }
    }
}
#[derive(Deserialize)]
pub struct Order {
    idx: usize,
    dir: SortOrder,
}

impl Order {
    pub fn new(idx: usize) -> Self {
        Order {
            idx,
            dir: SortOrder::Asc,
        }
    }

    pub fn reverse(idx: usize) -> Self {
        Order {
            idx,
            dir: SortOrder::Desc,
        }
    }
}

impl ToExpr for Order {
    fn to_expr(&self, columns: &[&str]) -> String {
        if self.idx == 0 {
            format!(" ORDER BY id {}", self.dir.to_expr())
        } else {
            format!(" ORDER BY {} {}", columns[self.idx - 1], self.dir.to_expr())
        }
    }
}

#[derive(Deserialize)]
pub struct NOrder<'a> {
    column: &'a str,
    dir: SortOrder,
}

impl<'a> NOrder<'a> {
    pub fn new(column: &'a str) -> Self {
        NOrder {
            column,
            dir: SortOrder::Asc,
        }
    }

    pub fn reverse(column: &'a str) -> Self {
        NOrder {
            column,
            dir: SortOrder::Desc,
        }
    }
}

impl<'a> ToExpr for NOrder<'a> {
    fn to_expr(&self, _: &[&str]) -> String {
        format!(" ORDER BY {} {}", self.column, self.dir.to_expr())
    }
}

#[derive(Deserialize)]
pub enum Condition {
    Equal,
    Less,
    More,
}

impl Condition {
    pub fn to_expr(&self) -> &str {
        use Condition::*;
        match self {
            Equal => "=",
            Less => "<",
            More => ">",
        }
    }
}
#[derive(Deserialize)]
pub struct ColumnValue<T, I> {
    idx: I,
    value: T,
}

impl<T, I> ColumnValue<T, I> {
    pub fn new(idx: I, value: T) -> Self {
        ColumnValue { idx, value }
    }
}
#[derive(Deserialize)]
struct WhereClause<T> {
    cond: Condition,
    column: ColumnValue<T, usize>,
}

impl<T> WhereClause<T> {
    pub fn new(idx: usize, value: T, cond: Condition) -> Self {
        WhereClause {
            cond,
            column: ColumnValue::new(idx, value),
        }
    }
}

impl<T> ToExpr for WhereClause<T> {
    fn to_expr(&self, columns: &[&str]) -> String {
        if self.column.idx == 0 {
            format!("(id{}?)", self.cond.to_expr())
        } else {
            format!("({}{}?)", columns[self.column.idx - 1], self.cond.to_expr())
        }
    }
}
struct NWhereClause<'a, T> {
    cond: Condition,
    column: ColumnValue<T, &'a str>,
}

impl<'a, T> NWhereClause<'a, T> {
    pub fn new(idx: &'a str, value: T, cond: Condition) -> Self {
        NWhereClause {
            cond,
            column: ColumnValue::new(idx, value),
        }
    }

    pub fn to_expr(&self, _: &[&str]) -> String {
        format!("({}{}?)", self.column.idx, self.cond.to_expr())
    }
}

pub trait ToExpr {
    fn to_expr(&self, columns: &[&str]) -> String;
}

pub trait ToParams<'a, T: 'a, R> {
    fn to_params(&'a self) -> Vec<R>
    where
        R: From<&'a T>;
}

impl<'a, T> ToExpr for NFinder<'a, T> {
    fn to_expr(&self, columns: &[&str]) -> String {
        if self.args.len() > 0 {
            "WHERE ".to_string()
                + &self
                    .args
                    .iter()
                    .map(|c| c.to_expr(columns))
                    .collect::<Vec<_>>()
                    .join(" AND ")
        } else {
            String::new()
        }
    }
}

impl<'a, T: 'a, R> ToParams<'a, T, R> for NFinder<'a, T> {
    fn to_params(&'a self) -> Vec<R>
    where
        R: From<&'a T>,
    {
        self.args
            .iter()
            .map(|c| From::from(&c.column.value))
            .collect()
    }
}
pub struct NFinder<'a, T> {
    args: Vec<NWhereClause<'a, T>>,
}

impl<'a, T> NFinder<'a, T> {
    pub fn new() -> Self {
        NFinder { args: Vec::new() }
    }
    pub fn where_<U>(mut self, idx: &'a str, value: U, cond: Condition) -> Self
    where
        T: From<U>,
    {
        self.args
            .push(NWhereClause::new(idx, From::from(value), cond));
        self
    }
    pub fn and<U>(mut self, idx: &'a str, value: U, cond: Condition) -> Self
    where
        T: From<U>,
    {
        self.args
            .push(NWhereClause::new(idx, From::from(value), cond));
        self
    }
}

impl<'a, T> ToExpr for Finder<T> {
    fn to_expr(&self, columns: &[&str]) -> String {
        if self.args.len() > 0 {
            "WHERE ".to_string()
                + &self
                    .args
                    .iter()
                    .map(|c| c.to_expr(columns))
                    .collect::<Vec<_>>()
                    .join(" AND ")
        } else {
            String::new()
        }
    }
}

impl<'a, T: 'a, R> ToParams<'a, T, R> for Finder<T> {
    fn to_params(&'a self) -> Vec<R>
    where
        R: From<&'a T>,
    {
        self.args
            .iter()
            .map(|c| From::from(&c.column.value))
            .collect()
    }
}
#[derive(Deserialize)]
pub struct Finder<T> {
    fts: Option<FullTextSearch>,
    args: Vec<WhereClause<T>>,
}

impl<T> Finder<T> {
    pub fn new() -> Self {
        Finder {
            fts: None,
            args: Vec::new(),
        }
    }
    pub fn where_<U>(mut self, idx: usize, value: U, cond: Condition) -> Self
    where
        T: From<U>,
    {
        self.args
            .push(WhereClause::new(idx, From::from(value), cond));
        self
    }
    pub fn and<U>(mut self, idx: usize, value: U, cond: Condition) -> Self
    where
        T: From<U>,
    {
        self.args
            .push(WhereClause::new(idx, From::from(value), cond));
        self
    }
}

#[derive(Default)]
pub struct Table {
    name: &'static str,
    columns: &'static [&'static str],
    insert: String,
    select: String,
    update: String,
    delete: String,
}

impl Table {
    pub fn new(name: &'static str, columns: &'static [&'static str]) -> Self {
        Table {
            name,
            columns,
            insert: gen_insert_stmt(name, columns),
            select: gen_select_stmt(name, columns),
            update: gen_update_stmt(name),
            delete: gen_delete_stmt(name),
        }
    }
}

impl Table {
    pub fn name(&self) -> &str {
        self.name
    }
    pub fn expr_insert(&self) -> &str {
        &self.insert
    }
    pub fn expr_select<F>(&self, f: &F) -> String
    where
        F: ToExpr,
    {
        vec![self.select.as_str(), &f.to_expr(self.columns)].join("")
    }
    pub fn expr_select_ord<F, S>(&self, f: &F, order: S) -> String
    where
        F: ToExpr,
        S: ToExpr,
    {
        vec![
            self.select.as_str(),
            &f.to_expr(self.columns),
            &order.to_expr(self.columns),
        ]
        .join("")
    }

    pub fn expr_update<F, C>(&self, column: C, f: &F) -> String
    where
        F: ToExpr,
        C: ToColumnName,
    {
        vec![
            self.update.as_str(),
            &format!("{}=? ", column.to_column_name(self.columns)),
            &f.to_expr(self.columns),
        ]
        .join("")
    }
    pub fn expr_delete<F>(&self, f: &F) -> String
    where
        F: ToExpr,
    {
        vec![self.delete.as_str(), &f.to_expr(self.columns)].join("")
    }

    pub fn columns(&self) -> &[&str] {
        self.columns
    }
}

fn gen_placeholders(n: usize) -> String {
    ["?"]
        .iter()
        .cycle()
        .take(n)
        .copied()
        .collect::<Vec<&str>>()
        .join(",")
}

fn gen_insert_stmt(t: &str, columns: &[&str]) -> String {
    format!(
        "INSERT INTO {} ({}) VALUES ({}) ",
        t,
        columns.join(", "),
        &gen_placeholders(columns.len())
    )
}

fn gen_select_stmt(t: &str, columns: &[&str]) -> String {
    format!("SELECT id, {} FROM {} ", columns.join(", "), t)
}

fn gen_update_stmt(t: &str) -> String {
    format!("UPDATE {} SET ", t)
}

fn gen_delete_stmt(t: &str) -> String {
    format!("DELETE FROM {} ", t)
}

pub trait Entity {
    fn values<'a, T: 'a>(&'a self) -> Vec<T>
    where
        T: From<&'a i64> + From<&'a String> + From<&'a Option<String>> + From<&'a Option<i64>>;

    fn len(&self) -> usize;
}


pub trait SelectExpr: IntoTable {
    fn select_expr<F, S>(&self, f: &F, order: S) -> String
    where
        F: ToExpr,
        S: ToExpr,
    {
        let table = <Self as IntoTable>::into_table();
        vec![
            table.select.as_str(),
            &f.to_expr(table.columns),
            &order.to_expr(table.columns),
        ]
        .join("")
    }
}


pub trait IntoTable {
    fn into_table() -> &'static Table;
}

pub trait Identify {
fn identify(&self) -> i64;

}



#[cfg(test)]
mod tests {

    //    use super::*;

    //    static STR_1: &str = "one";
    //    static STR_2: &str = "2";

    //    #[test]
    //    fn fixed_table_test() {}
}
