use std::{error, fmt, io, num, string};
use serde_json;
use serde::{Deserialize, Deserializer, Serialize, Serializer, de};
use crate::threadpool;
use tntransition;
use model;


#[derive(Debug)]
pub struct Error {
    pub kind: ErrorKind,
    desc: String,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Error {
        Error { kind, desc: String::from("") }
    }

    pub fn with_desc(kind: ErrorKind, desc: String) -> Error {
         Error { kind, desc }
}
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error {:?}", self)
    }
}

impl error::Error for Error {}

#[derive(Debug)]
pub enum ErrorKind {
    ParseMessage,
    CacheOverflow,
    EventHandlerNotFound,
    QueryHandlerNotFound,
    ArenaFull,
    TooLongIncomeMessage,
    ReadZeroBytes,
    ThreadPool(threadpool::Error),
    IO(io::Error),
    Transition(tntransition::Error),
    Model(model::Error),
//    Utf8Convert(string::FromUtf8Error),
//    Serde(serde_json::Error)
}


impl From<ErrorKind> for Error {
        fn from(v: ErrorKind) -> Error {
            Error::new(v)
        }
}


macro_rules! impl_from_error_to_tnerror {
    ($t:ty, $kind:ident) => {
        impl From<$t> for Error {
            fn from(v: $t) -> Error {
                Error::new(ErrorKind::$kind)
            }
        }
    };
}

impl_from_error_to_tnerror!(num::ParseIntError, ParseMessage);


macro_rules! impl_from_error {
    ($t:ty, $var:ident) => {
        impl From<$t> for Error {
            fn from(v: $t) -> Error {
                Error::new( ErrorKind::$var(v))
            }
        }
    };
}

impl_from_error!(io::Error, IO);
//impl_from_error!(string::FromUtf8Error, Utf8Convert);
//impl_from_error!(serde_json::Error, Serde);
impl_from_error!(threadpool::Error, ThreadPool);
impl_from_error!(tntransition::Error, Transition);
impl_from_error!(model::Error, Model);
