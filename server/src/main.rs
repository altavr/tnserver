#![warn(clippy::all)]

//#[macro_use]
//extern crate lazy_static;

use mio::{ Events, Poll, Ready, Registration,};
use std::sync::mpsc::{ Sender};
use std::net::{SocketAddr, ToSocketAddrs};
use std::path::Path;
use std::str::FromStr;
use tntransition::{JsonQuery,};
use model:: {TNModel, Filters, PermissiveFilters, FilterKit , create_tables};
use tnsqlite3::{Connection, OpenFlags, self};
use arena::Arena;
use crate::threadpool::ThreadPool;
//mod server;
//mod errors;
//mod threadpool;
//mod ichunk;


mod server;
mod threadpool;
mod ichunk;
mod errors;
mod thread;

use server::{Dispatcher, event_loop, ListenConnection, DBConnection, Transport};
use ichunk::IOC;

pub(crate) const CONNCAPACITY: usize = 100;
const WORKING_THREADS: usize = 4;



fn gen_db_conn(path: &str) -> Result<Connection, tnsqlite3::Error> {
   let mut conn =  Connection::with_flags(path,
     OpenFlags::READWRITE | OpenFlags::CREATE | OpenFlags::URI | OpenFlags::SHAREDCACHE )?;
     conn.register_cdatetime_func()?;
     Ok(conn)
}

fn main() {
    let db_file=  Path::new( "/home/alex/test_server.db");
    let db_file_path = db_file.to_str().unwrap();
    if !db_file.exists() {
        let mut conn = gen_db_conn(db_file_path).unwrap();
        create_tables(&mut conn).unwrap();
        }

    let addr = SocketAddr::from_str("127.0.0.1:8088").unwrap();
    let mut model = TNModel::<Filters>::new(db_file_path, gen_db_conn).unwrap();
//    model.create_tables().unwrap();
//    model.db_connection().execute("PRAGMA journal_mode=WAL", &[]);
    let qh = Box::new(JsonQuery::from_model(model));


let (registration, set_readiness) = Registration::new2();
let handler_db_conn = Box::new(DBConnection::new(registration, set_readiness.clone()));


let action_gen_old = move || {
    let st = set_readiness.clone();
    move |task: Box<dyn FnOnce() -> Transport + Send + 'static>,
          sender: &mut Sender<Transport>| {
        let mut tr = task();
        let res = tr.qh.request(tr.chunks.take());
        tr.message = res;
//        tr.message = Ok(Some("Hello".into()));
        sender.send(tr).unwrap();
        st.set_readiness(Ready::readable()).unwrap();
    }
};

//        let threadpool = ThreadPool::new(WORKING_THREADS, set_readiness_2.clone());
let threadpool = ThreadPool::new_ext(WORKING_THREADS, action_gen_old);



    let arena = Arena::new(IOC::default(), 128);
    let listen_conn = Box::new( ListenConnection::new(&addr, qh).unwrap());
    let poll = Poll::new().unwrap();
    let mut events = Events::with_capacity(CONNCAPACITY);
    let mut  dispatcher = Dispatcher::new(listen_conn, handler_db_conn,
    &poll, arena, threadpool, CONNCAPACITY).unwrap();

    event_loop(&poll, &mut events, &mut dispatcher).unwrap();
}
