use mio::{Ready, Registration, SetReadiness};
use std::sync::atomic::{self, AtomicBool, AtomicI32};
use std::sync::mpsc::Sender;
use std::sync::{Arc, Condvar, Mutex};

use crate::threadpool::ThreadPool;

pub enum ExeResult {
    Success,
    NeedRetry,
}

pub trait Execute {
    type Error;
    fn exe(&mut self) -> Result<ExeResult, Self::Error>;
    fn exe_retry(&mut self) -> Result<ExeResult, Self::Error>;
}

impl<T> ThreadPool<T>
where
    T: Send + Execute,
{
    pub fn with_concurrent(
        num_threads: usize,
        set_readiness: SetReadiness,
    ) -> ThreadPool<T> {
        use atomic::Ordering::*;

        let reader_lock_flag = Arc::new(AtomicBool::new(false));
        let conn_count = Arc::new(AtomicI32::new(0));
        let reader_lock = Arc::new((Mutex::new(false), Condvar::new()));
        let writer_lock = Arc::new((Mutex::new(false), Condvar::new()));

        let action_gen = || {
            //делаем копии примитивов блокировки для каждого треда
            let reader_lock_flag = reader_lock_flag.clone();
            let conn_count = conn_count.clone();
            let reader_lock = reader_lock.clone();
            let writer_lock = writer_lock.clone();
            let st = set_readiness.clone();

            //
            // Для увеличения скорости чтения из БД (несколько читателей одновременно), соединения
            //создаются с опцией NOMUTEX (Multi-thread mode), разрешение конфликтов  доступа к
            //ДБ происходит на уровне приложения (параметр action_gen пула тредов ThreadPool).
            //При нормальной работе соединения БД не происходит захват мьютексов. Если какой-то запрос к
            // БД вернулся с ошибкой BDBusy, он будет повторно выполнен в режиме монопольного доступа
            //в одном из тредов, после того как остальные треды будут заблокированы УСЛОВНОЙ ПЕРЕМЕННОЙ и
            // мьютексом

            move |task: Box<dyn FnOnce() -> T + Send + 'static>, sender: &mut Sender<T>| {
                let mut tr = task();
                let mut count = 0;

                //Подсчет количества активных тредов
                conn_count.fetch_add(1, SeqCst);
                loop {
                    if count == 2 {
                        break;
                    };
                    //Одна дополнительная попытка записи в ДБ, если первая не удалась с ошибкой DBBusy
                    if count > 0 {
                        count += 1;
                        //устанавливам мьютекс в false для блокировки остальных тредов
                        let (read_lock, _) = &*reader_lock;
                        let mut read_started = read_lock.lock().unwrap();
                        *read_started = false;
                        // устанавливаем флаг
                        reader_lock_flag.store(true, SeqCst);
                        //блокируем этот тред, сигнал при блокировке последнего активного треда
                        //разблокирует этот тред
                        let (write_lock, write_cvar) = &*writer_lock;
                        let mut write_started = write_lock.lock().unwrap();
                        while !*write_started {
                            write_started = write_cvar.wait(write_started).unwrap();
                        }
                        //очередной запрос к БД
                        let _ = tr.exe_retry();
                        //снимаем флаг
                        reader_lock_flag.store(false, SeqCst);
                        //возвращаем мьютекс в исходное состояние
                        let (write_lock, _) = &*writer_lock;
                        let mut write_started = write_lock.lock().unwrap();
                        *write_started = false;
                        //разлочка всех тредов
                        let (read_lock, read_cvar) = &*reader_lock;
                        let mut read_started = read_lock.lock().unwrap();
                        *read_started = true;
                        read_cvar.notify_all();
                        break;
                    }



                    if reader_lock_flag.load(SeqCst) {
                        conn_count.fetch_sub(1, SeqCst);
                        if conn_count.load(SeqCst) == 1 {
                            //последний тред помимо писателя блокируется, ДБ обеспечен монопольный
                            //доступ.
                            //тред писателя нужно разблокировать
                            let (write_lock, write_cvar) = &*writer_lock;
                            let mut write_started = write_lock.lock().unwrap();
                            *write_started = true;
                            write_cvar.notify_one();
                        }
                        //блокируем тред до окончания работы писателя
                        let (read_lock, read_cvar) = &*reader_lock;
                        let mut read_started = read_lock.lock().unwrap();
                        while !*read_started {
                            read_started = read_cvar.wait(read_started).unwrap();
                        }
                        conn_count.fetch_add(1, SeqCst);
                    } else {
                        count += 1;
                        match tr.exe() {
                            Ok(r) => match r {
                                ExeResult::Success => break,
                                ExeResult::NeedRetry => (),
                            },
                            Err(_) => panic!(),
                        };
                    }
                }
                conn_count.fetch_sub(1, SeqCst);
                if reader_lock_flag.load(SeqCst) && conn_count.load(SeqCst) == 1 {
                    //последний тред помимо писателя блокируется, ДБ обеспечен монопольный
                    //доступ для писателя.
                    //тред писателя нужно разблокировать
                    let (write_lock, write_cvar) = &*writer_lock;
                    let mut write_started = write_lock.lock().unwrap();
                    *write_started = true;
                    write_cvar.notify_one();
                }

                sender.send(tr).unwrap();
                st.set_readiness(Ready::readable()).unwrap();
            }
        };
        ThreadPool::new_ext(num_threads, action_gen)
    }
}
