use crate::errors::{Error, ErrorKind};
use arena::{Chunk, Reset};
use std::cmp::Ordering;
use std::io::{self, Read};
use std::fmt;
use iovec::IoVec;
use mio::net::{TcpListener, TcpStream};

const CHUNK_SIZE: usize = 4096;
type IOCArr = [u8; CHUNK_SIZE];

#[derive(Clone)]
pub struct IOC {
    buf: IOCArr,
    pos_idx: usize,
}

impl IOC {
    fn new(buf: IOCArr) -> Self {
        IOC { buf, pos_idx: 0 }
    }
    fn pos(&self) -> usize {
        self.pos_idx
    }
    fn set_pos(&mut self, pos: usize) {
        self.pos_idx = pos
    }
    fn raw_slice_mut(&mut self) -> &mut [u8] {
        &mut self.buf
    }
    fn raw_slice(&self) -> &[u8] {
        &self.buf
    }

}

impl Default for IOC {
    fn default() -> Self {   IOC {
    buf: [0; CHUNK_SIZE],
    pos_idx: 0,
}     }
}

impl AsRef<[u8]> for IOC {
    fn as_ref(&self) -> &[u8] {
       & self.raw_slice()[..self.pos()]
    }
}

impl AsMut<[u8]> for IOC {
    fn as_mut(&mut self) -> &mut [u8] {
     let pos = self.pos();
        &mut self.raw_slice_mut()[..pos]
    }
}

impl Reset for IOC {
    fn reset(&mut self) { self.pos_idx = 0}
}

impl IOChunked for Chunk<IOC> {
    fn set_pos(&mut self, pos: usize) {
        self.inner_mut().set_pos(pos)
    }
    fn pos(&self) -> usize {
        self.inner_ref().pos()
    }
    fn raw_slice(& self) -> & [u8]{
         &self.inner_ref().buf
    }
    fn raw_slice_mut(&mut self) -> &mut [u8]{
         &mut self.inner_mut().buf
    }

}



pub trait IOChunked {
    fn set_pos(&mut self, pos: usize);
    fn pos(&self) -> usize;
    fn raw_slice(&self) -> & [u8];
    fn raw_slice_mut(&mut self) -> &mut [u8];

    fn capacity(&self) -> usize {
        self.raw_slice().len()
    }
    fn remain_bytes(&self) -> usize {
        self.capacity() - self.pos()
    }
    fn slice_for_write(&mut self) -> &mut [u8] {
        let pos = self.pos();
        &mut self.raw_slice_mut()[pos..]
    }
}

impl<T> fmt::Debug for VChunks<T>

{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "VChunks<T>: {:?}", &self.idx)
    }
}

pub struct VChunks<T> {
    v: Vec<T>,
    idx: usize,
}

impl<T> VChunks<T>
where
    T: IOChunked,
{
    pub(crate) fn new(chunks: Vec<T>) -> Self {
        VChunks { v: chunks, idx: 0 }
    }

    pub(crate) fn write(&mut self, socket: &mut TcpStream) -> Result<(), Error>
    {
        if self.idx == self.v.len() {
            dbg!(self.idx );
            return Err(Error::new(ErrorKind::TooLongIncomeMessage));
        }

        let mut io_vec = self.v[self.idx..]
            .iter_mut()
            .map(|c|  IoVec::from_bytes_mut(c.slice_for_write()).expect("Chunk length is 0") )
            .collect::<Vec<_>>();
        let mut writed_bytes = socket.read_bufs(&mut io_vec)?;
        if writed_bytes == 0 {return Err(Error::new(ErrorKind::ReadZeroBytes)) }
//        dbg!(writed_bytes);
        for c in &mut self.v {
            let remain_len = c.remain_bytes();
//            dbg!(remain_len);
            match writed_bytes.cmp(&remain_len) {
                Ordering::Less => {
                    c.set_pos(c.pos() + writed_bytes);
                    break;
                }
                Ordering::Equal => {
                    c.set_pos(c.capacity());
                }
                Ordering::Greater => {
                    c.set_pos(c.capacity());
                    writed_bytes -= remain_len;
                }
            }
            self.idx += 1;
        }
        Ok(())
    }

    pub(crate) fn add_chunks(&mut self, chunk: Vec<T>) {
        self.v.extend(chunk)
    }

    pub(crate) fn take_chunks(self) -> Vec<T> { self.v}
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;

    impl IOChunked for IOC {
        fn set_pos(&mut self, pos: usize) {
            self.set_pos(pos)
        }
        fn pos(&self) -> usize {
            self.pos()
        }
        fn raw_slice(& self) -> & [u8]{
             &self.buf
        }
        fn raw_slice_mut(&mut self) -> &mut [u8]{
             &mut self.buf
        }
    }

    #[test]
    fn iochunk_test() {
        let chunks = vec![IOC::new([0; CHUNK_SIZE]); 4];
        let vc = VChunks::new(chunks);
    }
}

mod stubs {

use super::CHUNK_SIZE;
use mio::net::{TcpStream};
use crate::errors::{Error, ErrorKind};
use std::marker::PhantomData;
use std::io::Read;

    pub(crate) struct VChunks<T> {
        v: Vec<u8>,
        t: PhantomData<T>,
        offset: usize,
    }

impl<T> VChunks<T>
{
    pub(crate) fn new(chunks: Vec<T>) -> Self {
        VChunks { v: Vec::new(), t: PhantomData, offset: 0}
    }

    pub(crate) fn write(&mut self, socket: &mut TcpStream) -> Result<(), Error>
    {
        let mut c = vec![0;CHUNK_SIZE*10];
        let len = socket.read(&mut c);
        Ok(())
    }

//    pub(crate) fn add_chunks(&mut self, chunk: Vec<T>) {
//        self.v.extend(chunk)
//    }

//    pub(crate) fn take_chunks(self) -> Vec<T> { self.v}
}

}
