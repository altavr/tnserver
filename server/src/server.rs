//#![allow(unused)]
#![warn(clippy::all)]

use crate::errors::{Error, ErrorKind};
use mio;
use mio::net::{TcpListener, TcpStream};
use mio::{Event, Evented, Events, Poll, PollOpt, Ready, Registration, SetReadiness, Token};
use slab::Slab;
use std::collections::VecDeque;
use std::io::Write;
use std::net::SocketAddr;
use std::sync::atomic::{self, AtomicBool, AtomicI32};
use std::sync::mpsc::Sender;
use std::sync::{Arc, Condvar, Mutex};
use std::{fmt, io};
use crate::thread::Execute;
use crate::ichunk::{VChunks, IOC};
use crate::threadpool::ThreadPool;
use arena::{Arena, Chunk};
use tntransition::QueryHandled;

const MESSAGE_QUEUE_SIZE: usize = 10;


type EventHandler = Box<dyn EventHandled + 'static>;
pub(crate) type QueryHandler = Box<dyn QueryHandled<IOChunk> + Send + 'static>;
type IOChunk = Chunk<IOC>;

pub struct Transport {
    pub chunks: Option<Vec<IOChunk>>,
    pub qh: QueryHandler,
    pub message: Result<Option<Vec<u8>>, tntransition::Error>,
    pub token: Token,
}

#[derive(Debug)]
pub enum HandlerKind {
    Events(Vec< (TcpStream, QueryHandler)>),
    Query(VChunks<IOChunk>),
    ConnClosed(Token),
    ResponseReady,
}

pub trait EventHandled: Evented + fmt::Debug {
    fn handle(&mut self, readiness: Ready) -> Result<Option<HandlerKind>, Error>;
    fn send_response(&mut self, message: Vec<u8>) -> Result<(), Error> {
        unreachable!()
    }
}


pub fn event_loop(
    poll: &Poll,
    events: &mut Events,
    dispatcher: &mut Dispatcher,
) -> Result<(), Error> {
    loop {
        poll.poll(events, None)?;
        for event in &*events {
            dispatcher.dispatch(event, poll)?
        }
    }
}

pub(crate) struct HandlerStuff {
    event_handler: EventHandler,
    queue: Option<VecDeque<IOChunk>>,
    query_handler: Option<QueryHandler>,
    need_destroy: bool,
}

impl HandlerStuff {
    fn new(event_handler: EventHandler) -> Self {
        HandlerStuff {
            event_handler,
            queue: None,
            query_handler: None,
            need_destroy: false,
        }
    }
    fn client_handler(event_handler: EventHandler, query_handler: QueryHandler) -> Self {
        HandlerStuff {
            event_handler,
            queue: Some(VecDeque::with_capacity(MESSAGE_QUEUE_SIZE)),
            query_handler: Some(query_handler),
            need_destroy: false,
        }
    }
}

fn add_and_register(
    handler: EventHandler,
    handler_store: &mut Slab<HandlerStuff>,
    poll: &Poll,
) -> Result<(), Error> {
    let hs = HandlerStuff::new(handler);
    let token = handler_store.insert(hs);
    poll.register(
        &handler_store
            .get(token)
            .ok_or_else(|| Error::new(ErrorKind::EventHandlerNotFound))?
            .event_handler,
        Token(token),
        Ready::readable() ,
        PollOpt::edge()
    )?;
    Ok(())
}

pub struct Dispatcher {
    threadpool: ThreadPool<Transport>,
    hs: Slab<HandlerStuff>,
    arena: Arena<IOC>,
}

impl Dispatcher {
    pub fn new(
        handler: EventHandler,
        handler_db_conn: EventHandler,
        poll: &Poll,
        arena: Arena<IOC>,
        threadpool: ThreadPool<Transport>,
        capacity: usize,
    ) -> Result<Self, Error> {
        let mut handler_store = Slab::with_capacity(capacity);
        add_and_register(handler, &mut handler_store, poll)?;

        add_and_register(handler_db_conn, &mut handler_store, poll)?;


        Ok(Dispatcher {
            threadpool,
            hs: handler_store,
            arena,
        })
    }

    fn handler_stuff(&mut self, token: Token) -> Result<&mut HandlerStuff, Error> {
        self.hs
            .get_mut(token.0)
            .ok_or_else(|| Error::new(ErrorKind::EventHandlerNotFound))
    }

    fn dispatch(&mut self, event: Event, poll: &Poll) -> Result<(), Error> {
        let handler = &mut self.handler_stuff(event.token())?.event_handler;
        if let Some(ev_handler) = handler.handle(event.readiness())? {
//                        poll.reregister(
//                            handler,
//                            event.token(),
//                            Ready::readable() | Ready::writable(),
//                            PollOpt::edge() | PollOpt::oneshot(),
//                        )?;
            match ev_handler {
                HandlerKind::Events(handlers) => {
                    self.add_event_handlers(handlers, poll)
                }
                HandlerKind::Query(chunks) => self.send_to_thread_pool(chunks, event.token()),
                HandlerKind::ConnClosed(token) => self.mark_or_destroy(token, poll),
                HandlerKind::ResponseReady => self.get_responces(poll),
            }
        } else {
            Ok(())
        }
    }

    fn mark_or_destroy(&mut self, token: Token, poll: &Poll) -> Result<(), Error> {
        if self.handler_stuff(token)?.query_handler.is_some() {
            self.remove_event_handler(token, poll)?;
        } else {
            self.handler_stuff(token)?.need_destroy = true
        }
        Ok(())
    }

    fn remove_event_handler(&mut self, token: Token, poll: &Poll) -> Result<(), Error> {
        let hs = self.hs.remove(token.0);
        poll.deregister(&hs.event_handler)?;
        drop(hs);
        Ok(())
    }

    fn get_responces(&mut self, poll: &Poll) -> Result<(), Error> {
        while let Some(transport) = self.threadpool.try_recv()? {
            let Transport {
                qh: query_handler,
                message,
                token,
                ..
            } = transport;
            let hs = self.handler_stuff(token)?;
            if !hs.need_destroy {
                hs.query_handler = Some(query_handler);
                if let Some(message) = message.unwrap() {
                    hs.event_handler.send_response(message)?;
                }
            } else {
                self.remove_event_handler(token, poll)?;
                drop(query_handler)
            }
        }
        Ok(())
    }

    fn send_to_thread_pool(&mut self, chunks: VChunks<IOChunk>, token: Token) -> Result<(), Error> {
        let hs = self.handler_stuff(token)?;
        let queue = hs.queue.as_mut().unwrap();
        chunks
            .take_chunks()
            .into_iter()
            .for_each(|c| queue.push_back(c));
        if let Some(query_handler) = hs.query_handler.take() {
            let chunks = queue.drain(0..).collect();
            let transport = Transport {
                qh: query_handler,
                chunks: Some(chunks),
                message: Ok(None),
                token,
            };
            self.threadpool.execute(Box::new(move || transport))?
        }

        Ok(())
    }

    fn add_event_handlers(&mut self, handlers: Vec< (TcpStream, QueryHandler)>, poll: &Poll) -> Result<(), Error> {
         for (socket, qh) in handlers {
            self.add_event_handler(socket, qh, poll)?;
}
    Ok(())
}

    fn add_event_handler(
        &mut self,
        socket: TcpStream,
        qh: QueryHandler,
        poll: &Poll,
    ) -> Result<(), Error> {
        let vacant_entry = self.hs.vacant_entry();
        let event_handler = ClientConnection::new(
            socket,
            Token(vacant_entry.key()),
            Arena::from_arena(&self.arena),
        );
        let hs = HandlerStuff::client_handler(Box::new(event_handler), qh);

        let token = Token::from(self.hs.insert(hs));
        let eh = &self.handler_stuff(token)?.event_handler;
        poll.register(
            eh,
            token,
            Ready::readable() | Ready::writable(),
            PollOpt::edge()
        )?;
        Ok(())
    }
}

impl Evented for EventHandler {
    fn register(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        self.as_ref().register(poll, token, interest, opts)
    }

    fn reregister(
        &self,
        poll: &Poll,
        token: Token,
        interest: Ready,
        opts: PollOpt,
    ) -> io::Result<()> {
        self.as_ref().reregister(poll, token, interest, opts)
    }

    fn deregister(&self, poll: &Poll) -> io::Result<()> {
        self.as_ref().deregister(poll)
    }
}

impl fmt::Debug for ListenConnection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ListenConnection")
    }
}

pub struct ListenConnection {
    socket: TcpListener,
    client_qh: QueryHandler,
}

impl ListenConnection {
    pub fn new(addr: &SocketAddr, query_handler: QueryHandler) -> Result<ListenConnection, Error> {
        Ok(ListenConnection {
            socket: TcpListener::bind(addr)?,
            client_qh: query_handler,
        })
    }
}

impl EventHandled for ListenConnection {
    fn handle(&mut self, _: Ready) -> Result<Option<HandlerKind>, Error> {
        let mut handlers = Vec::new();

        loop {
            match self.socket.accept() {
                Ok((socket, addr)) => {
                    handlers.push((socket, self.client_qh.try_clone()?))
                }
                Err(e) if e.kind() == io::ErrorKind::WouldBlock =>
                {  break;},
                Err(e) => return Err(Error::new(ErrorKind::IO(e))),
            }
        }
        Ok(Some(HandlerKind::Events(handlers)))
    }
}


macro_rules! impl_evented {
    ($t:ty, $object:ident) => {
        impl Evented for $t {
            fn register(
                &self,
                poll: &Poll,
                token: Token,
                interest: Ready,
                opts: PollOpt,
            ) -> io::Result<()> {
                // Delegate the `register` call to `socket`
                self.$object.register(poll, token, interest, opts)
            }

            fn reregister(
                &self,
                poll: &Poll,
                token: Token,
                interest: Ready,
                opts: PollOpt,
            ) -> io::Result<()> {
                // Delegate the `reregister` call to `socket`
                self.$object.reregister(poll, token, interest, opts)
            }

            fn deregister(&self, poll: &Poll) -> io::Result<()> {
                // Delegate the `deregister` call to `socket`
                // self.$object.deregister(poll)
                poll.deregister(&self.$object)
            }
        }
    };
}

impl_evented!(ListenConnection, socket);
impl_evented!(ClientConnection, socket);

struct ClientConnection {
    socket: TcpStream,
    arena: Arena<IOC>,
    out_queue: VecDeque<Vec<u8>>,
    write_count: usize,
    token: Token,
}

impl ClientConnection {
    fn new(socket: TcpStream, token: Token, arena: Arena<IOC>) -> ClientConnection {
        ClientConnection {
            socket,
            arena,
            out_queue: VecDeque::with_capacity(MESSAGE_QUEUE_SIZE),
            write_count: 0,
            token,
        }
    }

    fn read(&mut self) -> Result<HandlerKind, Error> {
        use io::ErrorKind::*;

        let chunks: Vec<_> = self.arena.iter().collect();
        let mut chunks = VChunks::new(chunks);
        loop {
            match chunks.write(&mut self.socket) {
                Ok(_) => (),
                Err(Error { kind: ek, .. }) => match ek {
                    ErrorKind::IO(e) => match e.kind() {
                        WouldBlock => {
                            break;
                        }
                        ConnectionReset => return Ok(HandlerKind::ConnClosed(self.token)),
                        _ => return Err(Error::new(ErrorKind::IO(e))),
                    },

                    ErrorKind::ReadZeroBytes => return Ok(HandlerKind::ConnClosed(self.token)),
                    e => return Err(Error::new(e)),
                },
            }
        }

        Ok(HandlerKind::Query(chunks))
    }

    fn write(&mut self) -> Result<(), Error> {
        while let Some(message) = self.out_queue.pop_front() {
            if message.len() == self.write_count {
                self.write_count = 0;
                continue;
            }

            loop {
                match self.socket.write(&message[self.write_count..]) {
                    Ok(len) => {
                        self.write_count += len;
                        if self.write_count == message.len() {
                            break;
                        }
                    }
                    Err(e) if e.kind() == io::ErrorKind::WouldBlock => break,
                    Err(e) => return Err(Error::new(ErrorKind::IO(e))),
                }
            }
            self.out_queue.push_front(message);
        }
        Ok(())
    }
}

impl EventHandled for ClientConnection {
    fn handle(&mut self, readiness: Ready) -> Result<Option<HandlerKind>, Error> {
        let result = if readiness.is_readable() {
            Some(self.read()?)
        } else {
            None
        };
        if readiness.is_writable() {
            self.write()?;
        }
        Ok(result)
    }

    fn send_response(&mut self, message: Vec<u8>) -> Result<(), Error> {
        self.out_queue.push_back(message);
        self.write()
    }
}

impl fmt::Debug for ClientConnection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "CientConnection: token: {}", self.token.0)
    }
}

pub struct DBConnection {
    registration: Registration,
    set_readiness: SetReadiness,
}

impl DBConnection {
    pub fn new(registration: Registration, set_readiness: SetReadiness) -> DBConnection {
        DBConnection {
            registration,
            set_readiness,
        }
    }
}

impl_evented!(DBConnection, registration);

impl fmt::Debug for DBConnection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "DBConnection")
    }
}

impl EventHandled for DBConnection {
    fn handle(&mut self, _event: Ready) -> Result<Option<HandlerKind>, Error> {
        self.set_readiness.set_readiness(Ready::empty())?;
        Ok(Some(HandlerKind::ResponseReady))
    }
}
