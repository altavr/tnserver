#![allow(unused)]

#[path = "../tests/stubs.rs"]
pub(crate) mod stubs;

use mio::Ready;
use std::marker::PhantomData;
use std::sync::mpsc::{
    channel, Iter, Receiver, RecvError, SendError, Sender, TryIter, TryRecvError,
};
use std::sync::{Arc, Mutex};
use std::{error, fmt, thread};
use stubs::SetReadiness;

type PReceiver<T> = Arc<Mutex<Receiver<T>>>;
type Task<T> = Box<dyn FnOnce() -> T + Send + 'static>;

pub enum Message<T> {
    Job(T),
    Finish,
}

#[derive(Debug)]
pub enum Error {
    Send(Box<dyn error::Error>),
    Receive(RecvError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for Error {}


pub struct ThreadPool<T: Send + 'static> {
    workers: Vec<ExtendedWorker>,
    task_sender: Sender<Message<Task<T>>>,
    receiver: Receiver<T>,
}

impl<T: Send + 'static> ThreadPool<T> {
    pub fn new_ext<G, C>(size: usize, action_gen: G) -> ThreadPool<T>
    where
        C: Fn(Task<T>, &mut Sender<T>) + Send + 'static,
        G: Fn() -> C,
    {
        assert!(size < 8);
        let (task_sender, task_receiver) = channel();
        let (sender, receiver) = channel();

        let task_receiver = Arc::new(Mutex::new(task_receiver));
        let mut workers = Vec::with_capacity(size);

        for i in 0..size {
            let worker =
                ExtendedWorker::new(i, Arc::clone(&task_receiver), sender.clone(), action_gen());
            workers.push(worker);
        }
        ThreadPool {
            workers,
            task_sender,
            receiver,
        }
    }

    pub fn new(size: usize, set_readiness: SetReadiness) -> ThreadPool<T> {
        let action_gen = move || {
            let st = set_readiness.clone();
            move |task: Task<T>, sender: &mut Sender<T>| {
                let res = task();
                sender.send(res).unwrap();
                st.set_readiness(Ready::readable()).unwrap();
            }
        };
        ThreadPool::new_ext(size, action_gen)
    }

    pub(crate) fn execute(&self, task: Task<T>) -> Result<(), Error> {
        self.task_sender
            .send(Message::Job(task))
            .map_err(|e| Error::Send(Box::new(e)))
    }
    fn finished(&self) -> TryIter<T> {
        self.receiver.try_iter()
    }
    pub(crate) fn recv(&self) -> Result<T, Error> {
        self.receiver.recv().map_err(Error::Receive)
    }

    pub(crate) fn try_recv(&self) -> Result<Option<T>, Error> {
        match self.receiver.try_recv() {
            Ok(v) => Ok(Some(v)),
            Err(e) => match e {
                TryRecvError::Empty => Ok(None),
                TryRecvError::Disconnected => Err(Error::Receive(RecvError)),
            },
        }
    }
}

impl<T: Send + 'static> Drop for ThreadPool<T> {
    fn drop(&mut self) {
        for _ in &self.workers {
            self.task_sender.send(Message::Finish).unwrap();
        }
        for worker in &mut self.workers {
            worker.thread.take().unwrap().join().unwrap();
        }
    }
}

struct ExtendedWorker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl ExtendedWorker {
    fn new<T, C>(
        id: usize,
        receiver: PReceiver<Message<Task<T>>>,
        mut sender: Sender<T>,
        action: C,
    ) -> Self
    where
        T: Send + 'static,
        C: Fn(Task<T>, &mut Sender<T>) + Send + 'static,
    {
        let thread = thread::spawn(move || loop {
            let message = receiver.lock().unwrap().recv().unwrap();
            match message {
                Message::Job(task) => {action(task, &mut sender)},
                Message::Finish => break,
            };
        });
        ExtendedWorker {
            id,
            thread: Some(thread),
        }
    }
}
