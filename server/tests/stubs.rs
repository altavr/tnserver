#[cfg(not(feature = "fake"))]
pub(crate) use mio::SetReadiness;
#[cfg(feature = "fake")]
pub(crate) use stubs_::FakeSetReadiness as SetReadiness;

#[cfg(not(feature = "fake"))]
pub(crate) use mio::Registration;
#[cfg(feature = "fake")]
pub(crate) use stubs_::FakeRegistration as Registration;

pub mod stubs_ {
    use mio::Ready;
    use std::io;

    #[derive(Clone)]
    pub(crate) struct FakeSetReadiness(i32);

    impl FakeSetReadiness {
        pub(crate) fn new() -> FakeSetReadiness {
            FakeSetReadiness(0)
        }

        pub(crate) fn set_readiness(&self, _r: Ready) -> io::Result<()> {
            Ok(())
        }
    }

    pub(crate) struct FakeRegistration(i32);
    impl FakeRegistration {
        pub(crate) fn new2() -> (FakeRegistration, FakeSetReadiness) {
            (FakeRegistration(0), FakeSetReadiness(0))
}
    }

}
