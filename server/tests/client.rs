use serde_json;
use std::io::prelude::*;
use std::net::{Ipv4Addr, SocketAddrV4, TcpStream};
use std::thread::{sleep, spawn};
use std::time::Duration;
use model::{Method, Where_, Condition, TableKind, method, JSON_RPC_VER, RowId, find, Finder};
//use std::

fn main() {
    let addr: SocketAddrV4 = "127.0.0.1:8088".parse().unwrap();


fn proc_mess( message: &str, mut m: &mut [u8], i:  u64,  streamc: & mut TcpStream ) {
    streamc.write(message.as_bytes()).unwrap();
    println!("write {}", i);
    sleep(Duration::from_millis(100));
    let len = streamc.read(&mut m).unwrap();
    let buff = m[..len].to_vec();
    println!("{}", String::from_utf8(buff).unwrap());
    println!("read {}", i);
    //                if count==10 {break};

}



    let w = find!(  (2, vec![1,2,3 ] ), (2, vec![1,2,3 ] ) ) ;
   let meth = method!(add_group, ( "def gr".to_string(), RowId( 0)) );

    let meth_find = method!(find, (TableKind::Group, w));
//    let meth_fetch = Method::new(MethodKind::fetch_data, ParamsKind::TableArg(( TableKind::Group,  )));

   let message_org = serde_json::to_string(&meth).unwrap();
//    let message_fetch_data = serde_json::to_string(&meth_fetch).unwrap();
    let message_find  = serde_json::to_string(&meth_find).unwrap();


    let thread_handlers = (0..1)
        .into_iter()
        .map(|i| {
//        sleep(Duration::from_millis(5));
            let mut streamc = TcpStream::connect(addr).unwrap();
            streamc.set_nonblocking(false).unwrap();
            let mut m = vec![0u8; 100000];
//            let message = message_org.clone();
//            let message_fetch_data = message_fetch_data.clone();
            let message_find = message_find.clone();
            let message_org = message_org.clone();

            spawn(move || {
                  println!("connected {}", i);
//                  streamc.write(message.as_bytes()).unwrap();
                  sleep(Duration::from_millis(10 + i * 10));
                    let mut count = 0;
//                  let r = streamc.read(&mut m).unwrap();
                loop {
//                proc_mess(&message_org, &mut m, i, &mut streamc);
            proc_mess(&message_find, &mut m, i, &mut streamc);
//            proc_mess(&message_fetch_data, &mut m, i, &mut streamc);
            }})
        })
        .collect::<Vec<_>>();

    thread_handlers.into_iter().for_each(|h| h.join().unwrap());
    //loop {
    //    stream.write(message.as_bytes()).unwrap();
    //    println!("sent");
    //    sleep(dur);
    //    let r = stream.read(&mut m).unwrap();
    //    println!("read {}", r);
    //}
}
