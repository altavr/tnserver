
use crate::error::{Error, ErrorKind, Result};
use std::{convert::TryFrom, fmt, result, time};



pub trait Procedure {
    fn name(&self) -> &str;
    fn params(&self) -> &str;
    fn id(&self) -> u64;
}

pub trait TryClone<T> {
    fn try_clone(&self) -> result::Result<T, Error>;
}

pub trait Model {
    type Connection;
    type Output;
    type Proc;
    fn request(&mut self, method: Self::Proc) -> result::Result<Self::Output, Error>;
    fn init_db(&mut self) -> Result<()>;
    fn db_connection(&mut self) -> &mut Self::Connection;
}


pub(crate) trait EditTable {
    type Connection;
    type Stringinizer;
    fn set_int_field(
        &self,
        value: i64,
        row_id: i64,
        column: usize,
        conn: &mut Self::Connection,
    ) -> Result<()>;
    fn set_str_field(
        &self,
        value: &str,
        row_id: i64,
        column: usize,
        conn: &mut Self::Connection,
    ) -> Result<()>;

    fn column_type(&self, column: u64, conn: &mut Self::Connection) -> Result<ColumnTypeVar>;

    fn column_var_content(
        &self,
        column_idx: usize,
        conn: &mut Self::Connection,
        s: &Self::Stringinizer,
    ) -> Result<Vec<RowRepresent>>;
}

pub(crate) trait FetchData {
    type Connection;
    type Stringinizer;
    fn fetch_data(&self, s:& Self::Stringinizer, conn: &mut Self::Connection) -> Result<Vec<RowRepresent>>;
    fn find(&self, conn: &mut Self::Connection) -> Result<()>;

}


pub(crate) trait Header {
    type Connection;
    fn header(&self, conn: &mut Self::Connection) -> Result<Vec<String>>;

}




pub(crate) enum Data {
    Content(Vec<RowRepresent>),
    Header(Vec<String>),
    Id(i64),
    Null
}

pub(crate) struct RowRepresent {
  pub(crate)  id: i64,
  pub(crate)  columns: Vec<String>,
}


pub(crate) trait ViewRow {
    type Stringinizer;
    fn view_row(self,  s: &Self::Stringinizer) -> RowRepresent;
}



pub trait Stringinize {
    type Stringinizer;
    fn stringinize(self, row: &mut Vec<String>, r: &Self::Stringinizer);
}


pub trait Stringinized {
    type Stringinizer;
    fn from_stringinized(self, r: &Self::Stringinizer) -> String;
}


pub trait StringinizedIter {
    type Stringinizer;
    fn from_stringinized<'a>(self, r: &'a Self::Stringinizer)
        -> Box<dyn Iterator<Item=String> + 'a >;
}


pub(crate) trait ComplexVar: FetchData {
    type Stringinizer;

    fn column_var_content(
        &self,
        conn: &mut <Self as FetchData>::Connection,
        s: &  <Self as FetchData>::Stringinizer ,
    ) -> Result<Vec<RowRepresent>>
    {
        self.find(conn)?;
        self.fetch_data( s, conn)
    }
}


pub(crate) trait SetColumn {
    fn set_string(&mut self, value: &str, row_id: u64) -> Result<()> {
        Err(ErrorKind::UnsupportedType.into())
    }
    fn set_integer(&mut self, value: i64, row_id: u64) -> Result<()> {
        Err(ErrorKind::UnsupportedType.into())
    }
}

pub(crate) enum ColumnTypeVar {
    View = 0,
    String,
    Integer,
}



pub(crate) trait EditView<C, S>: FetchData<Connection=C,
                                Stringinizer=S>  {
    type Connection;
    type Stringinizer;
}


