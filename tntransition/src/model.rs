//#![allow(unused)]
use crate::error::{Error, ErrorKind, Result};
use std::marker::PhantomData;
use std::{convert::TryFrom, fmt, result, time};
// use serde_json::{de, ser };
use crate::sqlite3_scheme::*;
use serde::de::{self, SeqAccess, Visitor};
use serde::ser::SerializeSeq;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use tn_orm::*;
use tn_proc::{Entity, GenRow, IntoTable};
use tnsqlite3::{self, Connection, GenRow, Mapping, Row, Sqlite3Ref, Sqlite3Value};
use crate::absmodel::{Model, TryClone, Procedure};

pub const JSON_RPC_VER: &str = "2.0";

lazy_static! {
    pub static ref PERSON: Table = Table::new(
        "person",
        &["fname", "lname", "mname", "gr_main", "gr_1", "gr_2"]
    );
    pub static ref GROUP: Table = Table::new("group", &["name"]);
    pub static ref TASK: Table = Table::new("task", &["own_id", "performer_id", "status_id"]);
    pub static ref STATUS: Table = Table::new("status", &["name"]);
    pub static ref TASK_TIME_MOD: Table = Table::new("task_time_mod", &["task_id", "time"]);
    pub static ref TYPE_FIELD: Table = Table::new("type_field", &["type_id", "name", "in_use"]);
    pub static ref TASK_DATA: Table = Table::new(
        "task_data",
        &["modification_id", "type_field_id", "data_text", "data_int"]
    );
}

#[derive(Clone, Deserialize, Serialize, Debug, PartialEq)]
pub enum TNT {
    NULL,
    INTEGER(i64),
    DOUBLE(f64),
    BLOB(Vec<u8>),
    TEXT(String),
}

impl<'a> From<&'a TNT> for Sqlite3Ref<'a> {
    fn from(v: &'a TNT) -> Sqlite3Ref<'a> {
        use TNT::*;
        match *v {
            NULL => Sqlite3Ref::NULL,
            INTEGER(x) => Sqlite3Ref::INTEGER(x),
            DOUBLE(x) => Sqlite3Ref::DOUBLE(x),
            BLOB(ref x) => Sqlite3Ref::BLOB(x.as_slice()),
            TEXT(ref x) => Sqlite3Ref::TEXT(x.as_str()),
        }
    }
}

macro_rules! impl_from_for_TNT {
    ($t:ty, $var:ident) => {
        impl<'a> From<$t> for TNT {
            fn from(v: $t) -> TNT {
                TNT::$var(v)
            }
        }
    };
}

impl_from_for_TNT!(i64, INTEGER);
impl_from_for_TNT!(f64, DOUBLE);
impl_from_for_TNT!(String, TEXT);
impl_from_for_TNT!(Vec<u8>, BLOB);

pub enum TNTRef<'a> {
    NULL,
    INTEGER(i64),
    DOUBLE(f64),
    BLOB(&'a u8),
    TEXT(&'a str),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Types {
    Null = 1,
    Integer,
    Double,
    Blob,
    Text,
}

impl From<Sqlite3Value> for Types {
    fn from(v: Sqlite3Value) -> Types {
        use Types::*;
        match v {
            Sqlite3Value::INTEGER(x) => match x {
                1 => Null,
                2 => Integer,
                3 => Double,
                4 => Blob,
                5 => Text,
                _ => Null,
            },
            _ => Null,
        }
    }
}

impl TryFrom<i64> for Types {
    type Error = Error;
    fn try_from(v: i64) -> result::Result<Self, Self::Error> {
        use Types::*;
        match v {
            1 => Ok(Null),
            2 => Ok(Integer),
            3 => Ok(Double),
            4 => Ok(Blob),
            5 => Ok(Text),
            _ => Err(Error::new(ErrorKind::TypeError)),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub enum Data {
    Id(i64),
    Tasks(Vec<XRow<Task>>),
    Persons(Vec<Person>),
    Statuses(Vec<Status>),
    SchemaExt(Vec<ExtColProp>),
    Null,
}

impl Response {
    pub fn new(data: Data, id: Option<u64>) -> Self {
        Response {
            jsonrpc: JSON_RPC_VER.to_string(),
            result: data,
            id,
        }
    }
}

#[serde(rename = "")]
#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct ComError {
    code: i64,
    message: String,
}

#[serde(rename = "")]
#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct ErrObject {
    pub jsonrpc: String,
    pub error: ComError,
    pub id: Option<u64>,
}

impl ErrObject {
    fn new(code: i64, message: String, id: Option<u64>) -> Self {
        let error = ComError { code, message };
        ErrObject {
            jsonrpc: JSON_RPC_VER.to_string(),
            error,
            id,
        }
    }
}

#[serde(rename = "")]
#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct Response {
    pub jsonrpc: String,
    pub result: Data,
    pub id: Option<u64>,
}

//#[serde(rename = "")]
#[serde(untagged)]
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum OutObject {
    Response(Response),
    Error(ErrObject),
}

impl OutObject {
    pub fn response(d: Data, id: Option<u64>) -> Self {
        OutObject::Response(Response::new(d, id))
    }
    pub fn error(code: i64, message: String, id: Option<u64>) -> Self {
        OutObject::Error(ErrObject::new(code, message, id))
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct Method {
    jsonrpc: String,
    method: String,
    params: String,
    id: u64,
}
impl Method {
    pub fn new(name: &str, params: &str) -> Self {
        Method {
            jsonrpc: JSON_RPC_VER.to_string(),
            method: name.to_string(),
            params: params.to_string(),
            id: 42,
        }
    }
}

impl Procedure for Method {
    fn name(&self) -> &str {
        &self.method
    }
    fn params(&self) -> &str {
        &self.params
    }
    fn id(&self) -> u64 {
        self.id
    }
}


impl TryClone<Self> for DefModel {
    fn try_clone(&self) -> result::Result<Self, Error> {
        DefModel::new(self.gen_db_conn)
    }
}

impl Model for DefModel {
    type Connection = tnsqlite3::Connection;
    type Output = OutObject;
    type Proc = Method;

    fn request(&mut self, method: Self::Proc) -> result::Result<Self::Output, Error> {
        match self.request_inner(method.name(), method.params(), Some(method.id())) {
            Ok(r) => Ok(OutObject::Response(r)),
            Err(e) => Err(e),
        }
    }

    fn init_db(&mut self) -> Result<()> {
        self.create_tables()
    }

    fn db_connection(&mut self) -> &mut Connection {
        &mut self.db
    }
}

pub struct DefModel {
    db: Connection,
    task_table_schema: SchemaExt,
    gen_db_conn: fn() -> result::Result<Connection, tnsqlite3::Error>,
}

impl DefModel {
    pub fn new(gen_db_conn: fn() -> result::Result<Connection, tnsqlite3::Error>) -> Result<Self> {
        Ok(DefModel {
            db: gen_db_conn()?,
            task_table_schema: SchemaExt::new(),
            gen_db_conn,
        })
    }

    fn request_inner(&mut self, method: &str, args: &str, id: Option<u64>) -> Result<Response> {
        dbg!(method, args, id);
        let data = match method {
            "add_person" => {
                let args: (Person,) = serde_json::from_str(args)?;
                self.add_person(args.0)?
            }
            "add_column" => {
                let args: (i64, String) = serde_json::from_str(args)?;
                let t = TryFrom::try_from(args.0)?;
                self.add_column(t, args.1)?
            }
            "add_status" => {
                let args: (String,) = serde_json::from_str(args)?;
                self.add_status(args.0)?
            }
            "add_task" => {
                let args: (Task, ExtRow) = serde_json::from_str(args)?;
                self.add_task(args.0, args.1)?
            }
            "find_task" => {
                let args: (Finder<TNT>,) = serde_json::from_str(args)?;
                self.find_task(&args.0)?
            }
            "find_person" => {
                let args: (Finder<TNT>, Order) = serde_json::from_str(args)?;
                self.find_person(&args.0, args.1)?
            }

            _ => unreachable!(),
        };
        Ok(Response::new(data, id))
    }

    pub fn create_tables(&mut self) -> Result<()> {
        for (tablename, table) in CREATE_DB_EXPR {
            self.db.execute(table, &[])?;
        }
        Ok(())
    }

    fn add_column(&mut self, type_: Types, name: String) -> Result<Data> {
        self.task_table_schema
            .add_column(&mut self.db, name, type_)?;
        Ok(Data::Id(self.db.last_insert_rowid()))
    }

    fn add_status(&mut self, name: String) -> Result<Data> {
        let status = Status { id: 0, name };
        insert(&mut self.db, &status)?;
        Ok(Data::Id(self.db.last_insert_rowid()))
    }

    fn add_person(&mut self, person: Person) -> Result<Data> {
        self.db
            .execute((&*PERSON).expr_insert(), &person.values())?;
        Ok(Data::Id(self.db.last_insert_rowid()))
    }

    fn add_ext_row(&mut self, task_id: i64, row: ExtRow, time_f_id: Option<i64>) -> Result<()> {
        let modification_id = if let Some(modification_id) = time_f_id {
            modification_id
        } else {
            let sec_since_epoch = time::SystemTime::now()
                .duration_since(time::UNIX_EPOCH)
                .map_err(|_| Error::new(ErrorKind::SystemTimeError))?
                .as_secs() as i64;
            insert(
                &mut self.db,
                &TaskTimeMod {
                    id: 0,
                    task_id,
                    time: sec_since_epoch,
                },
            )?;
            self.db.last_insert_rowid()
        };
        let mut cols = Vec::new();
        for c in row.0 {
            self.task_table_schema.id2column_idx(c.0)?;
            cols.push(match c.1 {
                //TODO Add type check
                TNT::INTEGER(x) => TaskData {
                    id: 0,
                    modification_id,
                    type_field_id: c.0,
                    data_text: None,
                    data_int: Some(x),
                },
                TNT::TEXT(x) => TaskData {
                    id: 0,
                    modification_id,
                    type_field_id: c.0,
                    data_text: Some(x),
                    data_int: None,
                },
                _ => return Err(ErrorKind::UnsupportedType.into()),
            })
        }
        dbg!(&cols);
        insert_many(&mut self.db, &cols)?;
        Ok(())
    }

    fn add_task(&mut self, task: Task, ext_row: ExtRow) -> Result<Data> {
        insert(&mut self.db, &task)?;
        let sec_since_epoch = time::SystemTime::now()
            .duration_since(time::UNIX_EPOCH)
            .map_err(|_| Error::new(ErrorKind::SystemTimeError))?
            .as_secs() as i64;
        let task_id = self.db.last_insert_rowid();
        insert(
            &mut self.db,
            &TaskTimeMod {
                id: 0,
                task_id,
                time: sec_since_epoch,
            },
        )?;
        let modification_id = self.db.last_insert_rowid();
        self.add_ext_row(task.id, ext_row, Some(modification_id))?;
        Ok(Data::Null)
    }

    fn find_task<'a, F>(&mut self, f: &'a F) -> Result<Data>
    where
        F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    {
        self.db.execute(SELECT_TASK_EXT_STMT, &[])?;
        let mut qm = self.db.query_map(TaskExt::mapping)?;
        let mut res: Vec<XRow<Task>> = Vec::new();

        while let Some(ext) = qm.next() {
            let mut ext_store = Some(ext?);
            while let Some(ext) = ext_store.take() {
                match form_ext_row(&mut qm, ext, &self.task_table_schema)? {
                    (task_with_ext, Some(new_ext)) => {
                        res.push(task_with_ext);
                        ext_store = Some(new_ext)
                    }
                    (task_with_ext, None) => {
                        res.push(task_with_ext);
                    }
                }
            }
        }
        Ok(Data::Tasks(res))
    }

    fn find_person<'a, F: 'a>(&mut self, f: &'a F, order: Order) -> Result<Data>
    where
        F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    {
        Ok(Data::Persons(find_entity::<Person, F, Order>(
            &mut self.db,
            &f,
            order,
        )?))
    }
}

fn form_ext_row<I>(
    querymap: &mut I,
    ext: TaskExt,
    task_table_schema: &SchemaExt,
) -> Result<(XRow<Task>, Option<TaskExt>)>
where
    I: Iterator<Item = result::Result<TaskExt, tnsqlite3::Error>>,
{
    use Types::*;
    let row_len = task_table_schema.ids.len();
    let mut extrow: Vec<TNT> = vec![TNT::NULL; row_len];

    let task = Task {
        id: ext.task_id,
        owner_id: ext.owner_id,
        performer_id: ext.task_performer,
        status: ext.task_status,
    };

    let mut ext_store = Some(ext);
    while let Some(ext) = ext_store.take() {
        if task.id == ext.task_id {
            dbg!("in ifff");
            let t: Types = TryFrom::try_from(ext.type_id).unwrap_or(Types::Null);
            let idx = task_table_schema.id2column_idx(ext.type_field_id)?;
            extrow[idx] = match t {
                Text => TNT::TEXT(
                    ext.text
                        .unwrap_or_else(|| "ERROR UNWRAPPING: MUST BE TEXT".to_string()),
                ),
                Integer => TNT::INTEGER(ext.integer.unwrap_or(0)),
                _ => TNT::NULL,
            }
        } else {
            ext_store.replace(ext);
            break;
        }
        if let Some(ext) = querymap.next() {
            ext_store.replace(ext?);
        }
    }

    dbg!((&task, &extrow), &ext_store);
    Ok((
        XRow {
            fixed: task,
            ext: extrow,
        },
        ext_store,
    ))
}

#[derive(Debug, PartialEq, Clone, GenRow)]
struct TaskExt {
    task_id: i64,
    owner_id: i64,
    task_performer: i64,
    task_status: i64,
    time: i64,
    type_field_id: i64,
    type_id: i64,
    text: Option<String>,
    integer: Option<i64>,
}

pub(crate) fn insert<T>(conn: &mut Connection, entity: &T) -> Result<()>
where
    T: Entity + IntoTable + 'static,
{
    conn.insert_expr(T::into_table().expr_insert())?;
    Ok(conn.insert(&entity.values())?)
}

pub(crate) fn insert_many<T>(conn: &mut Connection, entities: &[T]) -> Result<()>
where
    T: Entity + IntoTable + 'static,
{
    conn.insert_expr(T::into_table().expr_insert())?;
    for e in entities {
        conn.insert(&e.values())?
    }
    Ok(())
}

pub(crate) fn select_entity<T>(
    conn: &mut Connection,
    query: &str,
    params: &[Sqlite3Ref],
) -> Result<Vec<T>>
where
    T: Mapping<T> + fmt::Debug + 'static,
{
    conn.execute(query, params)?;
    Ok(conn.query_map(T::mapping)?.fallible_collect()?)
}

pub(crate) fn request_entity<'a, F, S>(conn: &mut Connection, f: &'a F, order: S, table: &'static Table)
    -> Result<()>
where
//    T: Mapping<T> + IntoTable + 'static + fmt::Debug,
    F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    S: ToExpr,
{
//    let table = T::into_table();
    conn.execute(&table.expr_select_ord(f, order), &f.to_params())?;
    Ok(())
}

pub(crate) fn fetch_entity<T>(conn: &mut Connection) -> Result<Vec<T>>
where
    T: Mapping<T> + 'static + fmt::Debug,
{
    Ok(conn.query_map(T::mapping)?.fallible_collect()?)
}


pub(crate) fn find_entity<'a, T, F, S>(conn: &mut Connection, f: &'a F, order: S) -> Result<Vec<T>>
where
    T: Mapping<T> + IntoTable + 'static + fmt::Debug,
    F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    S: ToExpr,
{
    let table = T::into_table();
    conn.execute(&table.expr_select_ord(f, order), &f.to_params())?;
    Ok(conn.query_map(T::mapping)?.fallible_collect()?)
}

pub(crate) fn find_entity_and_transform<'a, T, F, S, TR, R>(
    conn: &mut Connection,
    f: &'a F,
    order: S,
    transform: TR,
) -> Result<Vec<R>>
where
    T: Mapping<T> + IntoTable + 'static + fmt::Debug,
    F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    S: ToExpr,
    TR: Fn(T) -> R,
{
    let table = T::into_table();
    conn.execute(&table.expr_select_ord(f, order), &f.to_params())?;
    Ok(conn.query_map(T::mapping)?.fallible_fn_collect(transform)?)
}

pub(crate) fn delete<'a, F>(conn: &mut Connection, kind: EntityKind, f: &'a F) -> Result<()>
where
    F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
{
    let table = kind.table();
    conn.execute(&table.expr_delete(f), &f.to_params())?;
    Ok(())
}

pub(crate) fn update<'a, 'b, R: 'b, F, C>(
    conn: &mut Connection,
    kind: EntityKind,
    column: C,
    value: R,
    f: &'a F,
) -> Result<()>
where
    R: Into<Sqlite3Ref<'b>>,
    F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    C: ToColumnName,
{
    let table = kind.table();
    let stmt = table.expr_update(column, f);
    let mut args: Vec<Sqlite3Ref> = vec![value.into()];
    args.extend_from_slice(&f.to_params());
    conn.execute(&stmt, &args)?;
    Ok(())
}

pub(crate) fn multi_update<'a, 'b, R: 'b, F, C>(
    conn: &mut Connection,
    kind: EntityKind,
    pairs: &'b [(C, R)],
    f: &Finder<TNT>,
) -> Result<()>
where
    &'b R: Into<Sqlite3Ref<'b>>,
    F: ToExpr + ToParams<'a, TNT, Sqlite3Ref<'a>>,
    C: ToColumnName,
{
    for p in pairs {
        update(conn, kind.clone(), p.0.clone(), &p.1, f)?;
    }
    Ok(())
}

#[derive(GenRow)]
struct IMap {
    id: i64,
}

#[derive(Clone, Deserialize, Serialize, Debug)]
pub struct ExtRow(Vec<(i64, TNT)>);

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct SchemaExt {
    ids: Vec<i64>,
}

impl SchemaExt {
    fn new() -> Self {
        SchemaExt { ids: Vec::new() }
    }

    fn load(&mut self, db: &mut Connection) -> Result<()> {
        //        db.execute(SELECT_LAST_TYPE_FIELDS, &[])?;
        let mut ids = Vec::new();

        for r in db.query_map(IMap::mapping)? {
            ids.push(r?.id);
        }
        self.ids = ids;
        Ok(())
    }

    fn id2column_idx(&self, id: i64) -> Result<usize> {
        self.ids
            .binary_search(&id)
            .map_err(|_| ErrorKind::NoColumnIdx.into())
    }

    fn add_column(&mut self, db: &mut Connection, name: String, t: Types) -> Result<()> {
        let nc = ExtColProp {
            id: 0,
            t: t as i64,
            name,
            in_use: 1,
        };
        insert(db, &nc)?;
        self.load(db)?;
        Ok(())
    }

    fn del_column(&mut self, db: &mut Connection, id: i64) -> Result<()> {
        update(
            db,
            EntityKind::TypeField,
            "in_use",
            0,
            &NFinder::new().where_("id", id, Condition::Equal),
        )?;
        self.load(db)?;
        Ok(())
    }

    fn rename_column(&mut self, db: &mut Connection, id: i64, name: String) -> Result<()> {
        update(
            db,
            EntityKind::TypeField,
            "name",
            &name,
            &NFinder::new().where_("id", id, Condition::Equal),
        )?;
        self.load(db)?;
        Ok(())
    }
}

macro_rules! ser_field {
    ($seq: ident, $self: ident, $($field: tt),+) => {
        $( ($seq.serialize_element(&$self.$field)?);)+
    };
}

impl Serialize for Person {
    fn serialize<S>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.len()))?;
        ser_field!(seq, self, id, fname, lname, mname);
        seq.end()
    }
}

impl<'de> Deserialize<'de> for Person {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct PersonVisitor;

        impl<'de> Visitor<'de> for PersonVisitor {
            type Value = Person;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct Person")
            }
            fn visit_seq<V>(self, mut seq: V) -> result::Result<Person, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let id = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let fname = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                let lname = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(2, &self))?;
                let mname = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(3, &self))?;

                Ok(Person {
                    id,
                    fname,
                    lname,
                    mname,
                })
            }
        }

        deserializer.deserialize_seq(PersonVisitor)
    }
}

#[derive(Clone)]
pub(crate) enum EntityKind {
    Person,
    Task,
    Status,
    TaskTimeMod,
    TypeField,
    TaskData,
}

impl EntityKind {
    fn table(&self) -> &'static Table {
        use EntityKind::*;
        match &self {
            Person => &*PERSON,
            Task => &*TASK,
            Status => &*STATUS,
            TaskTimeMod => &*TASK_TIME_MOD,
            TypeField => &*TYPE_FIELD,
            TaskData => &*TASK_DATA,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct ExtRowData(Vec<TNT>);

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct XRow<T> {
    fixed: T,
    ext: Vec<TNT>,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, GenRow, Entity, IntoTable, Clone)]
#[table = "TYPE_FIELD"]
pub struct ExtColProp {
    #[row_id]
    id: i64,
    t: i64,
    name: String,
    in_use: i64,
}

#[derive(Debug, PartialEq, Clone, GenRow, Entity, IntoTable)]
#[table = "PERSON"]
pub struct Person {
    #[row_id]
    pub id: i64,
    pub fname: String,
    pub lname: String,
    pub mname: String,
}

#[derive(Clone, Deserialize, Serialize, Debug, PartialEq, Entity, IntoTable, GenRow)]
#[table = "TASK"]
pub struct Task {
    #[row_id]
    pub id: i64,
    owner_id: i64,
    performer_id: i64,
    status: i64,
}

#[derive(Debug, PartialEq, GenRow, Entity, IntoTable)]
#[table = "TASK_DATA"]
pub(crate) struct TaskData {
    #[row_id]
    id: i64,
    modification_id: i64,
    type_field_id: i64,
    data_text: Option<String>,
    data_int: Option<i64>,
}

#[derive(Debug, PartialEq, GenRow, Entity, IntoTable)]
#[table = "TASK_TIME_MOD"]
pub(crate) struct TaskTimeMod {
    #[row_id]
    id: i64,
    task_id: i64,
    time: i64,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, GenRow, Entity, IntoTable, Clone)]
#[table = "STATUS"]
pub struct Status {
    #[row_id]
    id: i64,
    name: String,
}

pub(crate) struct Group {
    id: i64,
    name: String,
}

#[cfg(test)]
mod tests {

    use super::*;
    use crate::sqlite3_scheme::CREATE_DB_EXPR;
    use serde_json::{self};
    use tnsqlite3::Connection;
//    use crate::absmodel::*;

    fn create_conn() -> result::Result<Connection, tnsqlite3::Error> {
        Connection::new(":memory:")
    }
#[ignore]
    #[test]
    fn create_db_file() {
        let mut db = Connection::new(":memory:").unwrap();

        for (tablename, table) in CREATE_DB_EXPR {
            let _ = db.execute(table, &[]).unwrap();
        }
    }

    fn get_id(response: Data) -> i64 {
        match response {
            Data::Id(x) => x,
            _ => panic!(),
        }
    }
#[ignore]
    #[test]
    fn add_load_extcols_test() {
        let mut model = DefModel::new(create_conn).unwrap();
        model.create_tables().unwrap();
        let db = model.db_connection();

        let mut schema = SchemaExt::new();
        schema
            .add_column(db, "one string".to_string(), Types::Text)
            .expect("add_column(db, one string");
        schema
            .add_column(db, "two string".to_string(), Types::Text)
            .expect("add_column(db, two string");
        schema
            .add_column(db, "three int".to_string(), Types::Integer)
            .expect("add_column(db, three int");
        schema
            .add_column(db, "foth string".to_string(), Types::Text)
            .expect("add_column(db, foth string");
        let t_str = Types::Text as i64;
        let t_int = Types::Integer as i64;

        let src_schema = SchemaExt {
            ids: vec![1, 2, 3, 4],
        };
        assert_eq!(&schema, &src_schema);
        let mut schema = SchemaExt::new();
        schema.load(db).unwrap();
        assert_eq!(&schema, &src_schema);
    }
#[ignore]
    #[test]
    fn add_del_rename_extcols_test() {
        let mut model = DefModel::new(create_conn).unwrap();
        model.create_tables().unwrap();
        let db = model.db_connection();
        let t_str = Types::Text as i64;
        let t_int = Types::Integer as i64;
        let mut src_schema = SchemaExt {
            ids: vec![1, 2, 3, 4],
        };

        let mut schema = SchemaExt::new();
        schema
            .add_column(db, "one string".to_string(), Types::Text)
            .expect("add_column(db, one string");
        schema
            .add_column(db, "two string".to_string(), Types::Text)
            .expect("add_column(db, two string");
        schema
            .add_column(db, "three int".to_string(), Types::Integer)
            .expect("add_column(db, three int");
        schema
            .add_column(db, "foth string".to_string(), Types::Text)
            .expect("add_column(db, foth string");
        assert_eq!(&schema, &src_schema);
        //        let mut schema = src_schema.clone();
        schema.del_column(db, 2).expect("del_column(db, 2");
        src_schema.ids.remove(1);
        assert_eq!(&schema, &src_schema);

        schema.del_column(db, 4).expect("del_column(db, 4");
        src_schema.ids.remove(2);
        assert_eq!(&schema, &src_schema);

        schema
            .add_column(db, "fifth string".to_string(), Types::Text)
            .expect("add_column(db, fifth string");
        src_schema.ids.push(5);
        assert_eq!(&schema, &src_schema);

        let mut schema = SchemaExt::new();
        schema.load(db).unwrap();
        assert_eq!(&schema, &src_schema);
    }
#[ignore]
    #[test]
    fn add_tasks_test() {
        let mut p = Person {
            id: 1,
            fname: "Jone".to_string(),
            lname: "Dow".to_string(),
            mname: "ya".to_string(),
        };

        let mut model = DefModel::new(create_conn).unwrap();
        model.create_tables().unwrap();
        let person_id = get_id(model.add_person(p).expect("add_person"));
        let status_id = get_id(
            model
                .add_status("good status".to_string())
                .expect("add_status"),
        );
        let task = Task {
            id: 0,
            owner_id: person_id,
            performer_id: person_id,
            status: status_id,
        };
        let col_0_id = get_id(
            model
                .add_column(Types::Text, "col 0".to_string())
                .expect("add_column col 0"),
        );
        let col_1_id = get_id(
            model
                .add_column(Types::Text, "col 1".to_string())
                .expect("add_column col 1"),
        );
        let col_2_id = get_id(
            model
                .add_column(Types::Integer, "col 2".to_string())
                .expect("add_column col 2"),
        );

        let extrow = ExtRow(vec![
            (col_0_id, TNT::TEXT("two string".to_string())),
            (col_1_id, TNT::TEXT("ont string".to_string())),
            (col_2_id, TNT::INTEGER(44)),
        ]);

        model
            .add_task(task.clone(), extrow.clone())
            .expect("model.add_task(task");

        let extrow = ExtRow(vec![
            (col_1_id, TNT::TEXT("ont string 2".to_string())),
            (col_2_id, TNT::INTEGER(42)),
            (col_0_id, TNT::TEXT("two string 2".to_string())),
        ]);

        model
            .add_task(task.clone(), extrow.clone())
            .expect("model.add_task(task");
        let t_list = model.find_task(&NFinder::new()).unwrap();

        let t_list_src = Data::Tasks(vec![
            XRow {
                fixed: Task {
                    id: 1,
                    owner_id: 1,
                    performer_id: 1,
                    status: 1,
                },
                ext: vec![
                    TNT::TEXT("two string".to_string()),
                    TNT::TEXT("ont string".to_string()),
                    TNT::INTEGER(44),
                ],
            },
            XRow {
                fixed: Task {
                    id: 2,
                    owner_id: 1,
                    performer_id: 1,
                    status: 1,
                },
                ext: vec![
                    TNT::TEXT("two string 2".to_string()),
                    TNT::TEXT("ont string 2".to_string()),
                    TNT::INTEGER(42),
                ],
            },
        ]);

        assert_eq!(&t_list, &t_list_src);
    }
#[ignore]
    #[test]
    fn insert_and_find_test() {
        let mut model = DefModel::new(create_conn).unwrap();
        model.create_tables().unwrap();
        let db = model.db_connection();

        let mut p = Person {
            id: 1,
            fname: "Jone".to_string(),
            lname: "Dow".to_string(),
            mname: "ya".to_string(),
        };

        insert(db, &p.clone()).unwrap();
        let mut p1 = p.clone();
        p1.fname = "Jane".to_string();
        insert(db, &p1).unwrap();

        let f = NFinder::new().where_("fname", "Jone".to_string(), Condition::Equal);
        let res: Vec<Person> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res[0], &p);
        assert_eq!(res.len(), 1);

        let f = NFinder::new().where_("id", 1, Condition::Equal);
        let res: Vec<Person> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res[0], &p);
        assert_eq!(res.len(), 1);

        let f = Finder::new()
            .where_(1, "Jone".to_string(), Condition::Equal)
            .and(2, "Dow".to_string(), Condition::Equal);
        let res: Vec<Person> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res[0], &p);
        assert_eq!(res.len(), 1);

        let f = NFinder::new();
        let mut status = Status {
            id: 1,
            name: "status".to_string(),
        };
        insert(db, &status.clone()).unwrap();
        let res_stat: Vec<Status> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res_stat[0], &status);

        let prop = ExtColProp {
            id: 1,
            t: 1,
            name: "field 1".to_string(),
            in_use: 1,
        };
        insert(db, &prop.clone()).unwrap();
        let res_prop: Vec<ExtColProp> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res_prop[0], &prop);
    }
#[ignore]
    #[test]
    fn update_and_delete_test() {
        let mut model = DefModel::new(create_conn).unwrap();
        model.create_tables().unwrap();
        let db = model.db_connection();

        let mut p = Person {
            id: 1,
            fname: "Jone".to_string(),
            lname: "Dow".to_string(),
            mname: "ya".to_string(),
        };

        insert(db, &p.clone()).unwrap();
        let mut p1 = p.clone();
        p1.fname = "Jane".to_string();
        insert(db, &p1).unwrap();

        let f = Finder::new().where_(1, "Jone".to_string(), Condition::Equal);
        update(db, EntityKind::Person, 3, "yoyo", &f).unwrap();
        let res: Vec<Person> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res[0].mname, "yoyo");

        update(db, EntityKind::Person, "mname", "yaya", &f).unwrap();
        let res: Vec<Person> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert_eq!(&res[0].mname, "yaya");

        delete(db, EntityKind::Person, &f).unwrap();
        let res: Vec<Person> = find_entity(db, &f, NOrder::new("id")).unwrap();
        assert!(res.len() == 0);
    }
#[ignore]
    #[test]
    fn ser_person() {
        let person = Person {
            id: 88,
            fname: "Jone".to_string(),
            lname: "Dow".to_string(),
            mname: "ya".to_string(),
        };
        assert_eq!(
            "[88,\"Jone\",\"Dow\",\"ya\"]",
            &serde_json::to_string(&person).unwrap()
        )
    }
    #[ignore]
    #[test]
    fn de_person() {
        let s = "[88,\"Jone\",\"Dow\",\"ya\"]";
        let p: Person = serde_json::from_str(s).expect("Not succesfull trying deserialize");

        let ref_p = Person {
            id: 88,
            fname: "Jone".to_string(),
            lname: "Dow".to_string(),
            mname: "ya".to_string(),
        };
        assert_eq!(ref_p, p);
    }
}
