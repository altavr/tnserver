//#[macro_use]
//extern crate lazy_static;

mod error;
pub mod message;
//pub mod model;
//mod sqlite3_scheme;
//mod index_model;
//mod absmodel;

pub use error::{Error, ErrorKind};
pub use message::{JsonQuery, QueryHandled};
pub use model::{TNModel};
pub use model::{Model, TryClone};

#[cfg(test)]
mod tests {}
