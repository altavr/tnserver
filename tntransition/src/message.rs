//#![allow(unused)]

use crate::error::{Error, ErrorKind};
use model::{Model,  TNModel, TryClone, JSON_RPC_VER};
use serde::{Deserialize, Serialize};
use serde_json::{self, de::Deserializer, de::SliceRead, de::StreamDeserializer};
use std::marker::PhantomData;
use std::{fmt, io};

type QueryHandler<T> = Box<dyn QueryHandled<T>  + Send + 'static>;
static JSONRPC: Protocol = Protocol{header: r#"{"jsonrpc":"2.0""#  };

struct Protocol{
    header: &'static str
}

impl Protocol {
    fn with_header<T>(&self, chunk: &T) -> bool where
    T: AsRef<[u8]>,  {
    &chunk.as_ref()[..self.header.len()] == self.header.as_bytes()
}
}



pub trait QueryHandled<T>
where
    T: AsRef<[u8]> + 'static + Send,
{
    fn request(&mut self, chunks: Option<Vec<T>>) -> Result<Option<Vec<u8>>, Error>;
    fn try_clone(&self) -> Result<QueryHandler<T>, Error> ;
}


#[derive(Default)]
struct ProcDeSer {
    buf: Vec<u8>,
    offset: usize,
}

impl ProcDeSer {
    fn update<'de, T, C>(&'de mut self, chunks: Vec<C>, head_type: &Protocol)
    where
        C: AsRef<[u8]>,
        T: Deserialize<'de>,
    {
        if self.offset != 0 {
            self.buf = self.buf[self.offset..].to_vec();
            self.offset = 0;
        }
        if chunks.len() == 0 {
            return;
        }
        println!("{:?}", String::from_utf8( chunks[0].as_ref().into() ));
        let len_chunks = chunks.iter().fold(0, |a, f| a + f.as_ref().len());
        let len_buf = self.buf.len() - self.offset;
        let mut new_buf = if len_buf > 0 && !head_type.with_header(&chunks[0]) {
            let mut new_buf = Vec::with_capacity(len_buf + len_chunks);
            new_buf.extend_from_slice(&self.buf[self.offset..]);
            new_buf
        } else {
            Vec::with_capacity(len_chunks)
        };
        chunks
            .iter()
            .for_each(|c| new_buf.extend_from_slice(c.as_ref()));
        self.buf = new_buf;
    }

    fn iter<'de, T>(&'de mut self) -> ProcIter<'de, T>
    where
        T: Deserialize<'de>,
    {
        let de = serde_json::Deserializer::from_slice(&self.buf[self.offset..]);
        let stream = de.into_iter::<T>();
        ProcIter {
            sde: stream,
            offset: &mut self.offset,
        }
    }
}

struct ProcIter<'de, T, R = SliceRead<'de>> {
    sde: StreamDeserializer<'de, R, T>,
    offset: &'de mut usize,
}

impl<'de, T> Iterator for ProcIter<'de, T>
where
    T: Deserialize<'de>,
{
    type Item = Result<T, serde_json::error::Error>;
    fn next(&mut self) -> Option<Self::Item> {
        let item = self.sde.next();
        *self.offset = self.sde.byte_offset();
        match &item {
            Some(Ok(_)) => item,
            None => None,
            Some(Err(e)) if e.is_eof() => None,
            Some(Err(_)) => item,
        }
    }
}


impl<T> fmt::Debug for dyn QueryHandled<T> + Send + 'static {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "QueryHandled")
    }
}


pub struct JsonQuery<T, M> {
    model: M,
    t: PhantomData<T>,
    deser: ProcDeSer,
}

impl<T, M> JsonQuery<T, M>
where
    T: AsRef<[u8]>,
    M: Model+ TryClone<M, model::Error>,
{
    pub fn new(model: M) -> Self {
        JsonQuery {
            model,
            t: PhantomData,
            deser: ProcDeSer::default(),
        }
    }
    pub fn from_model(model: M) -> Self {
        JsonQuery {
            model,
            t: PhantomData,
            deser: ProcDeSer::default(),
        }
    }

}

impl<T, M> QueryHandled<T> for JsonQuery<T, M>
where
    T: AsRef<[u8]> + Send + 'static,
    M: Model,
    M::Output: Serialize,
    for<'de> M::Proc: Deserialize<'de> + fmt::Debug,
    Self: TryClone<QueryHandler<T>, Error>,
{
    fn request(&mut self, chunks: Option<Vec<T>>) -> Result<Option<Vec<u8>>, Error> {
        let mut responses = Vec::new();
        if let Some(chunks) = chunks {
            self.deser.update::<M::Proc, T>(chunks, &JSONRPC);
        }
        for i in self.deser.iter::<M::Proc>() {
            match i {
                Ok(method) => {
                    match self.model.request(method) {
                        Ok(resp) => {
                            let message =
                                serde_json::to_vec(&resp).expect("serde_json::to_vec(&resp)");
                            responses.extend_from_slice(&message);
                        }
                        Err(e) => return Err(e.into()),
                    }
                }
                Err(e) => {
                    panic!()
                }
            };
        }
        if responses.is_empty() {
            Ok(None)
        } else {
            Ok(Some(responses))
        }
    }

    fn try_clone(&self) -> Result<QueryHandler<T>, Error> {
        let qh = TryClone::try_clone(self)?;
        Ok(qh)
    }
}


impl<T, M> TryClone<QueryHandler<T>, Error> for JsonQuery<T, M>
where
    T: AsRef<[u8]> + Send + 'static,
    M: TryClone<M, model::Error> + Model+ Send+ 'static,
    M::Output: Serialize,
    for<'de> M::Proc: Deserialize<'de> + fmt::Debug,

{
    fn try_clone(&self) -> Result<QueryHandler<T>, Error> {
        let model = self.model.try_clone()?;
        Ok(Box::new(JsonQuery::new(model)))
}
}

#[cfg(test)]
mod tests {
    use super::*;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Default, Debug, PartialEq)]
    struct Method {
    a: String,
    b: String,
    c: String,
}

impl Method {
    fn new(a: &str, b: &str) -> Self {
        Method {
            a: a.to_string(),
            b: b.to_string(),
            c: "blahblahblahblahblahblahblahblahblahblahblahblahblahblahblah".to_string(),
        }
    }
}


    #[test]
    fn jsonrpc_header_detect_true_test() {
        let s = "{\"jsonrpc\":\"2.0\",\"result\":{\"Persons\":[[88,\"Jone\",\"Dow\",\"ya\"]]},\"id\":11}";
        assert!(JSONRPC.with_header(&s));
    }

    #[test]
    fn jsonrpc_header_detect_false_test() {
        let s = "{\"json rpc\":\"2.0\",\"result\":{\"Persons\":[[88,\"Jone\",\"Dow\",\"ya\"]]},\"id\":11}";
        assert!(!JSONRPC.with_header(&s));
    }



    #[test]
    fn proc_de_ser_test() {
        let args = serde_json::to_string(& ( "def".to_string(), )  ).unwrap();
        let method = Method::new("add_group", &args);
        let meth = serde_json::to_string(&method).unwrap();
        let mut procdeser = ProcDeSer::default();
        procdeser.update::<Method, String>( vec![meth], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let met = it.next().unwrap();
        assert!(met.is_ok());
        assert!( it.next().is_none());
        println!("{:?}", met);
    }

    #[test]
    fn proc_de_ser_test1() {
        let args = serde_json::to_string(& ( "def".to_string(), )  ).unwrap();
        let method = Method::new("add_group", &args);
        let mut meth = serde_json::to_string(&method).unwrap();
        let meth1 = meth.split_off(10);
        let mut procdeser = ProcDeSer::default();
        procdeser.update::<Method, String>( vec![meth, meth1], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let met = it.next().unwrap();
        assert!(met.is_ok());
        assert!( it.next().is_none());
        println!("{:?}", met);
    }

    #[test]
    fn proc_de_ser_test2() {
        let args = serde_json::to_string(& ( "def".to_string(), )  ).unwrap();
        let method = Method::new("add_group", &args);
        let mut meth = serde_json::to_string(&method).unwrap();
        let _ = meth.split_off(10);
        let mut procdeser = ProcDeSer::default();
        procdeser.update::<Method, String>( vec![meth], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let met = it.next();
        assert!(met.is_none());
        println!("{:?}", met);
    }

    #[test]
    fn proc_de_ser_test3() {
        let args = serde_json::to_string(& ( "def".to_string(), )  ).expect("1");
        let method = Method::new("add_group", &args);

        let method2 = Method::new("add_user", &args);
        let mut meth = serde_json::to_string(&method).expect("2");
        let ref_meth_len = meth.len();
        let  meth_2 = serde_json::to_string(&method2).expect("3");
        let ref_meth2_len = meth_2.len();
        let meth_0_1 = meth.split_off(12);
        let mut procdeser = ProcDeSer::default();
        procdeser.update::<Method, String>( vec![meth], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let met = it.next();
        assert!(met.is_none());
        procdeser.update::<Method, String>( vec![meth_0_1, meth_2], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let m = it.next().unwrap().expect("4 --");

        assert_eq!(&m, &method);

        assert_eq!(it.next().unwrap().unwrap(), method2);
        assert!(it.next().is_none());
        assert_eq!(ref_meth2_len + ref_meth_len, procdeser.buf.len() );
    }

    #[test]
    fn proc_de_ser_test4() {
        let args = serde_json::to_string(& ( "def".to_string(), )  ).expect("1");
        let method = Method::new("add_group", &args);
        let mut meth = serde_json::to_string(&method).expect("2");
        let ref_meth_len = meth.len();
        let meth1 = meth.split_off(12);
        let mut procdeser = ProcDeSer::default();
        procdeser.update::<Method, String>( vec![meth], &JSONRPC);

        let mut it = procdeser.iter::<Method>();
        assert!( it.next().is_none());

        procdeser.update::<Method, String>( vec![meth1], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let met = it.next().expect("3");
        assert!(met.is_ok());
        assert_eq!(ref_meth_len,  procdeser.buf.len());

        let  meth = serde_json::to_string(&method).expect("4");
        procdeser.update::<Method, String>( vec![meth], &JSONRPC);
        let mut it = procdeser.iter::<Method>();
        let met = it.next().expect("1");
        assert!(met.is_ok());
        assert_eq!(ref_meth_len,  procdeser.buf.len());

    }

    #[test]
    fn proc_de_ser_test5() {
    let args = serde_json::to_string(& ( "def".to_string(), )  ).expect("1");
    let method = Method::new("add_group", &args);
    let method2 = Method::new("add_user", &args);
    let  meth2 = serde_json::to_string(&method2).expect("2");
    let  meth = serde_json::to_string(&method).expect("3");
    let ref_meth2_len = meth2.len();
    let mut procdeser = ProcDeSer::default();
    procdeser.update::<Method, String>( vec![meth, meth2], &JSONRPC);
    let mut it = procdeser.iter::<Method>();
    assert!(it.next().expect("5").is_ok());
    assert!(it.next().expect("6").is_ok());
    assert!( it.next().is_none());
    procdeser.update::<Method, String>( vec![], &JSONRPC);
    assert_eq!(0, procdeser.buf.len() );

    }

    #[ignore]
    #[test]
    fn call_add_person_test() {}

}
