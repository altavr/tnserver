#[allow(dead_code)]
pub(crate) const CREATE_DB_EXPR: &[(&str, &str)] = &[
    (
        "task",
        "CREATE TABLE task (
           id               INTEGER PRIMARY KEY,
           own_id           INTEGER NOT NULL,
           performer_id     INTEGER NOT NULL,
           status_id        INTEGER NOT NULL ,
           FOREIGN KEY (own_id) REFERENCES person(id),
           FOREIGN KEY (performer_id) REFERENCES person(id),
           FOREIGN KEY (status_id) REFERENCES status(id))",
    ),
    (
        "status",
        "CREATE TABLE status (
            id  INTEGER PRIMARY KEY,
            name TEXT NOT NULL)",
    ),
    (
        "group",
        "CREATE TABLE group (
                 id              INTEGER PRIMARY KEY,
                 name            TEXT NOT NULL)",
    ),
    (
        "person",
        "CREATE TABLE person (
                 id              INTEGER PRIMARY KEY,
                 fname            TEXT NOT NULL,
                 lname            TEXT NOT NULL,
                 mname            TEXT NOT NULL,
                 gr_main          INTEGER NOT NULL,
                 gr_1             INTEGER,
                 gr_2             INTEGER,
                 FOREIGN KEY (gr_main) REFERENCES group(id),
                 FOREIGN KEY (gr_1 ) REFERENCES group(id),
                 FOREIGN KEY (gr_2) REFERENCES group(id)) ",
    ),
    (
        "task_time_mod",
        "CREATE TABLE task_time_mod (
           id          INTEGER PRIMARY KEY,
           task_id     INTEGER NOT NULL,
           time        INTEGER NOT NULL,
           FOREIGN KEY (task_id) REFERENCES task(id)
           )",
    ),
    (
        "ecolumn",
        "CREATE TABLE ecolumn (
           id           INTEGER  PRIMARY KEY AUTOINCREMENT,
           name         TEXT NOT NULL,
           in_use       INTEGER NOT NULL
       )",
    ),
    (
        "ecolumn_filtred",
        "CREATE TABLE type_field_filtred (
            ecolumn_id       INTEGER
          )",
    ),
    (
        "ecolumn_trigger",
        "CREATE TRIGGER ecolumn_trigger
        AFTER  INSERT ON ecolumn
        BEGIN
          DELETE FROM ecolumn_filtred;
          INSERT INTO ecolumn_filtred
          SELECT id
              FROM ecolumn
              WHERE (in_use = 1)
              ORDER BY id;
        END",
    ),
    (
        "task_data",
        "CREATE TABLE task_data (
           id                  INTEGER PRIMARY KEY,
           modification_id     INTEGER  NOT NULL,
           ecolumn_id           INTEGER NOT NULL,
           data_text            TEXT,
           FOREIGN KEY (modification_id)   REFERENCES task_time_mod(id),
           FOREIGN KEY (ecolumn_id)           REFERENCES ecolumn(id),
           UNIQUE (modification_id, ecolumn_id)
       )",
    ),
//    (
//        "task_last_mod",
//        "CREATE VIEW task_ext (
//        id,
//        task_id,
//        mod_time,
//        type_field_id,
//        data_text,
//        data_int
//    )
//    AS SELECT   td.id,
//                tm.task_id,
//                tm.time,
//                tf.id,
//                td.data_text,
//                td.data_int
//    FROM task_data as td
//    INNER JOIN task_time_mod as tm ON td.modification_id = tm.id
//    INNER JOIN type_field as tf ON td.type_id = tf.id
//    GROUP BY tf.idx
//    HAVING max(tm.time)
//    ORDER BY tm.task_id",
//    ),
];



pub(crate) const SELECT_TASK_EXT_STMT: &str =
    "SELECT task.id, task.own_id, task.performer_id, task.status_id, ttm.time,
    ec.ecolumn_id,  type_field.type_id, td.data_text
    FROM ecolumn_filtred as ec
     JOIN ecolumn ON ecolumn.id = ec.ecolumn_id
     JOIN task_data AS td ON ec.ecolumn_id = td.ecolumn_id
     JOIN task_time_mod as ttm ON td.modification_id = ttm.id
    JOIN task ON task.id = ttm.task_id";

//pub(crate) const SELECT_TASK_EXT_STMT: &str =
//    "SELECT task.id, task.own_id, task.performer_id, task.status_id, ttm.time,
//    type_field.id,  type_field.type_id, td.data_text, td.data_int
//    FROM type_field_filtred as tf
//    INNER JOIN type_field ON type_field.id = tf.type_field_id
//    INNER JOIN task_data AS td ON tf.type_field_id = td.type_field_id
//    INNER JOIN task_time_mod as ttm ON td.modification_id = ttm.id
//    INNER JOIN task ON task.id = ttm.task_id
//    GROUP BY ttm.time
//    HAVING max(ttm.time)
//    ORDER BY task.id";



