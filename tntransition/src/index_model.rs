#![allow(unused)]

use crate::absmodel::*;
use crate::error::{Error, ErrorKind, Result};
use crate::model::{
    delete, find_entity, insert, insert_many, multi_update, select_entity, update, Group, Person,
    Response, Status, Task, TaskData, TaskTimeMod, TNT, OutObject, Method, fetch_entity, request_entity
};
use crate::sqlite3_scheme::*;
use std::sync::RwLock;
use std::{marker::PhantomData, result, time};
use tn_orm::*;
use tn_proc::{Entity, GenRow, Identify, IntoTable, Stringinize};
use tnsqlite3::{self, Connection, GenRow, Mapping, Sqlite3Ref, Sqlite3Value};

lazy_static! {
    pub static ref TASK_PERSON_VIEW: Table = Table::new("person", &["fname", "lname", "mname"]);
    pub static ref TASK_STATUS: Table = Table::new("person", &["fname", "lname", "mname"]);
}

//type EditableTable = Box<dyn TabularModel>;


//impl<T> EditView<tnsqlite3::Connection, Stringinizer> for T
//where
//    T: FetchData<Connection = tnsqlite3::Connection, Stringinizer = Stringinizer>,
//{
//    type Connection = tnsqlite3::Connection;
//    type Stringinizer = Stringinizer;
//}

impl<T> ViewRow for T
where
    T: Stringinize + Identify,
{
    type Stringinizer= T::Stringinizer;

    fn view_row(self, s: &Self::Stringinizer) -> RowRepresent {
        let id = self.identify();
        let mut columns = Vec::new();
        self.stringinize(&mut columns, s);
        RowRepresent { id, columns }
    }
}

pub struct TNModel {
    db: Connection,
    gen_db_conn: fn() -> result::Result<Connection, tnsqlite3::Error>,

//    views: Vec<EditableTable>,
}

impl TNModel {

    pub fn new(gen_db_conn: fn() -> result::Result<Connection, tnsqlite3::Error>) -> Result<Self> {
        Ok(TNModel {
            db: gen_db_conn()?,
            gen_db_conn,
//            views: Vec::new(),
        })
    }

    pub fn create_tables(&mut self) -> Result<()> {
        for (tablename, table) in CREATE_DB_EXPR {
            self.db.execute(table, &[])?;
        }
        Ok(())
    }

    fn request_inner(&mut self, method: &str, args: &str, id: Option<u64>) -> Result<Response> {
        dbg!(method, args, id);
        let data = match method {

            _ => unreachable!(),
        };
        Ok(Response::new(data, id))
    }


}


impl Model for TNModel {

    type Connection = tnsqlite3::Connection;
    type Output = OutObject;
    type Proc = Method;

    fn request(&mut self, method: Self::Proc) -> result::Result<Self::Output, Error> {
        unimplemented!()
}
    fn init_db(&mut self) -> Result<()> {
         self.create_tables()
}
    fn db_connection(&mut self) -> &mut Self::Connection {
        &mut self.db
}
}

//impl TabularModel for TNModel {

//    type ViewType = ViewType;

//    fn add_task_view_column(&self, name: &str) -> Result<Data> {
//        unimplemented!()
//    }
//    fn del_task_view_column(&self, column: u64) -> Result<Data> {
//        unimplemented!()
//    }

//    fn remain_data(&self) -> Result<Data> {
//        unimplemented!()
//    }
//    fn find(&self, view: ViewType) -> Result<Data> {
//        unimplemented!()
//    }
//    fn header(&self, view: ViewType) -> Result<Data> {
//        unimplemented!()
//    }
//    fn set_int_field(
//        &self,
//        view: ViewType,
//        value: i64,
//        row_id: i64,
//        column: usize,
//    ) -> Result<Data> {
//        unimplemented!()
//    }
//    fn set_str_field(
//        &self,
//        view: ViewType,
//        value: &str,
//        row_id: i64,
//        column: usize,
//    ) -> Result<Data> {
//        unimplemented!()
//    }

//    fn column_type(&self, view: ViewType, column: u64) -> Result<Data> {
//        unimplemented!()
//    }
//    fn column_var_header(&self, view: ViewType, column_idx: usize) -> Result<Data> {
//        unimplemented!()
//    }
//    fn column_var_content(&self, view: ViewType, column_idx: usize) -> Result<Data> {
//        unimplemented!()
//    }
//}


macro_rules! impl_fetch_data {
    ($target_type: ty, $row_type: ty) => {
        impl FetchData for $target_type {
            type Connection = tnsqlite3::Connection;
            type Stringinizer = Stringinizer;

            fn fetch_data(
                &self,
                s: &Self::Stringinizer,
                conn: &mut Self::Connection,
            ) -> Result<Vec<RowRepresent>> {
                let tasks: Vec<$row_type> = fetch_entity(conn)?;
                Ok(tasks.into_iter().map(|r| r.view_row(s)).collect())
            }

            fn find(&self, conn: &mut Self::Connection) -> Result<()> {
                request_entity(conn, &NFinder::new(), Order::new(0), <$row_type as IntoTable>::into_table())
            }
        }
    };
}

//impl_fetch_data!(TaskTable, TaskRow);
impl_fetch_data!(TaskPersonTable, TaskPerson);
impl_fetch_data!(TaskStatusTable, TaskStatus);

pub(crate) enum ViewType {
    Task = 0,
    Person,
}




struct TaskTable {
    select_stmt: String,
    column_count: u64,
    task_person_table: TaskPersonTable,
    task_status_table: TaskStatusTable,
}


struct TaskComplexColumn {


}

struct TaskPersonTable {}

struct TaskStatusTable {}




impl Header for TaskTable {
    type Connection = tnsqlite3::Connection;
    fn header(&self, conn: &mut Self::Connection) -> Result<Vec<String>> {
        unimplemented!();
    }
}

impl EditTable for TaskTable {
    type Connection = tnsqlite3::Connection;
    type Stringinizer = Stringinizer;

    fn set_int_field(
        &self,
        value: i64,
        row_id: i64,
        column: usize,
        conn: &mut Self::Connection,
    ) -> Result<()> {
        Err(ErrorKind::UnsupportedType.into())
    }
    fn set_str_field(
        &self,
        value: &str,
        row_id: i64,
        column: usize,
        conn: &mut Self::Connection,
    ) -> Result<()> {
        unimplemented!();
    }

    fn column_type(&self, column: u64, conn: &mut Self::Connection) -> Result<ColumnTypeVar> {
        Ok(match column {
            0 => ColumnTypeVar::View,
            1 => ColumnTypeVar::View,
            2 => ColumnTypeVar::View,
            x if x < self.column_count => ColumnTypeVar::String,
            _ => Err(ErrorKind::InvalidIndex)?,
        })
    }

    fn column_var_content(
        &self,
        column_idx: usize,
        conn: &mut Self::Connection,
        s: &Stringinizer,
    ) -> Result<Vec<RowRepresent>> {

        Ok(match column_idx {
            0 => self.task_person_table.column_var_content( conn, s)? ,
            1 => self.task_person_table.column_var_content( conn, s)?,
            2 => self.task_status_table.column_var_content( conn, s)?,
            _ => Err(ErrorKind::InvalidIndex)?,
        })
    }
}


impl ComplexVar for TaskPersonTable {
    type Stringinizer = <Self as FetchData>::Stringinizer;
}

impl ComplexVar for TaskStatusTable {
    type Stringinizer = <Self as FetchData>::Stringinizer;
}


impl Stringinized for String {
    type Stringinizer = Stringinizer;
    fn from_stringinized(self, _: &Self::Stringinizer) -> String {
        self
    }
}


#[derive(GenRow, Identify, Stringinize, Debug)]
#[stringinizer = "Stringinizer"]
struct TaskRow {
    #[row_id]
    #[stringinize_ignore]
    id: i64,
    owner: TaskPerson,
    performer: TaskPerson,
    status: TaskStatus,
    #[stringinize_iter]
    column_ext: ColumnExt,
}


#[derive(Debug)]
struct ColumnExt {
    d: Vec<String>,
}

impl GenRow<ColumnExt> for ColumnExt {
    fn gen_row(row: &mut tnsqlite3::Row) -> result::Result<Option<ColumnExt>, tnsqlite3::Error> {
        let mut v = Vec::with_capacity((row.len() - row.count()) as usize);
        for i in row.count()..row.len() {
            v.push(row.get(i)?.unwrap_or_default())
        }
        Ok(Some(ColumnExt { d: v }))
    }
}

impl StringinizedIter for ColumnExt {
    type Stringinizer = Stringinizer;
    fn from_stringinized<'a>(self, r: &'a Self::Stringinizer)
        -> Box<dyn Iterator<Item=String> + 'a >  {
     Box::new(  self.d.into_iter()  )
}
}

#[derive(GenRow, Identify, IntoTable, Debug, Stringinize)]
#[stringinizer = "Stringinizer"]
#[table = "TASK_STATUS"]
struct TaskStatus {
    #[row_id]
    #[stringinize_ignore]
    id: i64,
    name: String
}

impl Stringinized for TaskStatus {
    type Stringinizer =Stringinizer;
    fn from_stringinized(self, _: &Self::Stringinizer) -> String {
        self.name
    }
}


#[derive(GenRow, Identify, IntoTable, Debug, Stringinize)]
#[table = "TASK_PERSON_VIEW"]
#[stringinizer = "Stringinizer"]
struct TaskPerson {
    #[row_id]
    #[stringinize_ignore]
    id: i64,
    fname: String,
    lname: String,
    mname: String,
}







impl Stringinized for TaskPerson {
    type Stringinizer =Stringinizer;
    fn from_stringinized(self, s: &Self::Stringinizer) -> String {
        s.from_person(self)
    }
}


struct Date(i64);

pub struct Stringinizer {}

impl Stringinizer {
    fn new() -> Self {
        Stringinizer {}
    }

    fn from_date<I>(&self, d: Date) -> String {
        unimplemented!()
    }
    fn from_person(&self, p: TaskPerson) -> String {
        format!("{} {} {}", &p.fname, &p.lname, &p.mname)
    }


    fn from_status<I>(&self, p: Status) -> String {
        unimplemented!()
    }
    fn from_string<I>(&self, s: String) -> String {
        unimplemented!()
    }
}



#[cfg(test)]
mod tests {

    use super::*;

    struct AA {
        a: String,
        b: String,
    }

    fn destruct_test(a: AA) {}
}
