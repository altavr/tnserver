use serde_json::error::Error as SerdeErr;
//use tnsqlite3;
use std::{result, io, error, fmt};
use model;

pub(crate) type Result<T> = result::Result<T, Error>;


#[derive(Debug)]
pub struct Error {
    pub kind: ErrorKind,
}

impl Error {
    pub fn new(kind: ErrorKind) -> Self {
        Error { kind }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}


impl error::Error for Error { }


impl From<ErrorKind> for Error {
    fn from(v: ErrorKind) -> Error {
        Error{ kind: v }
}

}

#[derive(Debug)]
pub enum ErrorKind {
    Model(model::Error),
    Serde(SerdeErr),
    IO(io::Error),
}

macro_rules! impl_from_error {
    ($t:ty, $var:ident) => {
        impl From<$t> for Error {
            fn from(v: $t) -> Error {
              Error::new(  ErrorKind::$var(v))
            }
        }
    };
}


impl_from_error!(model::Error, Model);
impl_from_error!(SerdeErr, Serde);
impl_from_error!(io::Error, IO);


macro_rules! impl_from_errorkind {
    ($t:ty, $var:ident) => {
        impl From<$t> for ErrorKind {
            fn from(v: $t) -> ErrorKind {
                ErrorKind::$var(v)
            }
        }
    };
}


impl_from_errorkind!(model::Error, Model);
impl_from_errorkind!(SerdeErr, Serde);
impl_from_errorkind!(io::Error, IO);

