#![allow(unused)]
extern crate proc_macro;

use proc_macro2::{Span, TokenStream};
use quote::{quote, quote_spanned};
use syn::parse;
use syn::spanned::Spanned;
use syn::{
    parse_quote, Attribute, Data, DeriveInput, Fields, GenericParam, Generics, Ident, Index, Lit,
    Path, PathArguments, Type, LitStr,
};

#[proc_macro_derive(Entity, attributes(sql_ignore))]
pub fn derive_entity(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let (values, len) = add_values(&input.data);
    //    let table_var = get_table_var(&input.attrs).expect("Not define attibute \"table\"");

    let expanded = quote! {
        impl tn_orm::Entity for #name
         {
            fn values<'a, T:'a>(&'a self) -> Vec<T>
                where T: From<&'a i64> +
                    From<&'a String> +
                    From<&'a Option<String>> +
                    From<&'a Option<i64>>  {
                #values
            }
            fn len(&self) -> usize {#len}
        }
    };
    //    println!("______________{:?}", row_id_field_name);

    proc_macro::TokenStream::from(expanded)
}

fn row_id_field(data: &Data) -> Option<TokenStream> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                for f in fields.named.iter() {
                    if f.attrs.iter().any(|a| a.path.is_ident("row_id")) {
                        let i = &f.ident;
                        return Some(quote!(#i));
                    }
                }
            }
            _ => panic!("Need field attribute \"row_id\""),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }

    None
}

fn add_values(data: &Data) -> (TokenStream, usize) {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let mut num_fields = 0;
                let num_ref = &mut num_fields;
                let recurse = fields
                    .named
                    .iter()
                    .filter(|f| {
                        !f.attrs
                            .iter()
                            .any(|a| a.path.is_ident("row_id") | a.path.is_ident("sql_ignore"))
                    })
                    .map(|f| {
                        *num_ref += 1;
                        let name = &f.ident;
                        quote_spanned! {f.span()=>
                            (&self.#name).into()
                        }
                    });
                (
                    quote! {
                        vec![#(#recurse, )*]
                    },
                    num_fields,
                )
            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}
fn get_table_var(attrs: &Vec<Attribute>) -> Option<TokenStream> {
    for attr in attrs {
        if attr.path.is_ident("table") {
            match attr.parse_meta().unwrap() {
                syn::Meta::NameValue(x) => {
                    let span = Span::call_site();
                    match x.lit {
                        Lit::Str(s) => {
                            let name = Ident::new(&s.value(), span);
                            return Some(quote!(#name));
                        }
                        _ => panic!(),
                    }
                }
                _ => return None,
            }
        }
    }
    None
}

#[proc_macro_derive(Identify, attributes(row_id))]
pub fn derive_identify(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let row_id_field_name = row_id_field(&input.data).expect("Not define attibute \"row_id\"");

    let expanded = quote! {
        impl tn_orm::Identify for #name
         {
            fn identify(&self) -> i64 {
                self.#row_id_field_name
            }
        }
    };
    //    println!("______________{:?}", row_id_field_name);

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(IntoTable, attributes(row_id, table))]
pub fn derive_to_table(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let table_var = get_table_var(&input.attrs).expect("Not define attibute \"table\"");

    let expanded = quote! {
        impl tn_orm::IntoTable for #name
         {
            fn into_table() -> &'static Table {
                &*#table_var
            }
        }
    };
    //    println!("______________{:?}", row_id_field_name);

    proc_macro::TokenStream::from(expanded)
}

#[proc_macro_derive(GenRow, attributes(mapping_ignore))]
pub fn derive_mapping(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let (fields, defs) = add_fields(&input.data);

    //        println!("______________{:?}", &fields);

    let expanded = quote! {
        impl GenRow<Self> for #name {
            fn gen_row(row: &mut tnsqlite3::Row) -> std::result::Result<Option<Self>, tnsqlite3::Error> {
                #defs
               Ok( Some( #name {
                    #fields
                }))
            }

        }
    };
    //    let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}

fn add_fields(data: &Data) -> (TokenStream, TokenStream) {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields.named.iter().enumerate().map(|(i, f)| {
                    let name = &f.ident;
                    let ty = match &f.ty {
                        Type::Path(x) => x.path.segments.last(),
                        _ => panic!(),
                    };
                    let i = i as i32;
                    let tname = &ty.unwrap().ident;
                    if !f.attrs.iter().any(|a| a.path.is_ident("mapping_ignore")) {
                        if tname == "Option" {
                            let args = match &ty.unwrap().arguments {
                                PathArguments::AngleBracketed(x) => x,
                                _ => unimplemented!(),
                            };
                            quote_spanned! {f.span()=> let #name = #args::gen_row(row)? }
                        } else {
                            quote_spanned! {f.span()=> let #name =  #ty::gen_row(row)?
                            .ok_or_else(|| tnsqlite3::Error::InnerError(
                                tnsqlite3::InnerError::new(tnsqlite3::IRR::MappingError(#i))) )? }
                        }
                    } else {
                        quote_spanned! {f.span()=> let #name =  #tname::default()}
                    }
                });

                let recurse_names = fields.named.iter().enumerate().map(|(i, f)| {
                    let name = &f.ident;
                    quote_spanned! {f.span()=> #name }
                });
                (
                    quote! {
                        #(#recurse_names, )*
                    },
                    quote! {
                        #(#recurse; )*
                    },
                )
            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

#[proc_macro_derive(
    Stringinize,
    attributes(stringinize_ignore, stringinizer, stringinize_iter)
)]
pub fn derive_stringinize(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let vec = add_to_iterator(&input.data);
    let kinds = get_colums_kind(&input.data);
//    panic!( kinds.to_string() );
    let associated_attr = get_stringinizer_var(&input.attrs)
        .expect("associated attibute \"stringinizer \" not  present");
    //    println!("______________{:?}", &vec);

    let expanded = quote! {
        impl Stringinize<#name> for #name {
        type Stringinizer = #associated_attr;
        const COLUMN_KIND: &'static [ColumnKind] = &[#kinds];
            fn stringinize(src: #name, row: &mut Vec<String>, s: &Self::Stringinizer)
            {
                #vec
            }
        }
    };
    //        let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}

fn add_to_iterator(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields
                    .named
                    .iter()
                    .filter(|f| {
                        !f.attrs
                            .iter()
                            .any(|a| a.path.is_ident("stringinize_ignore"))
                    })
                    .map(|f| {
                        let name = &f.ident;
                        if !f.attrs
                        .iter()
                        .any(|a| a.path.is_ident("stringinize_iter") ) {
                        quote_spanned! {f.span() => row.push( src.#name.stringinized(s) ) } }
                        else {
          quote_spanned! {f.span() => src.#name.stringinized(s).for_each(|s| row.push( s) ); }
                            }
                    });
                quote! {
                    #(#recurse;)*
                }
            }
            _ => panic!("This is not named struct"),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}


fn get_colums_kind(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields
                    .named
                    .iter()
                    .filter(|f| {
                        !f.attrs
                            .iter()
                            .any(|a| a.path.is_ident("stringinize_ignore"))
                    })
                    .map(|f| {
                        let name = &f.ident;
                        let ty = match &f.ty {
                            Type::Path(x) => x.path.segments.last(),
                            _ => panic!(),
                        };
                        let typ = &f.ty;
                        quote_spanned! {ty.span() => <#typ as ColumnKinded>::COLUMN_KINDED}
                        }
                    );
                quote! {
                    #(#recurse,)*
                }
            }
            _ => panic!("This is not named struct"),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}


fn get_stringinizer_var(attrs: &Vec<Attribute>) -> Option<TokenStream> {
    for attr in attrs {
        if attr.path.is_ident("stringinizer") {
            match attr.parse_meta().unwrap() {
                syn::Meta::NameValue(x) => {
                    let span = Span::call_site();
                    match x.lit {
                        Lit::Str(s) => {
                            let name = Ident::new(&s.value(), span);
                            return Some(quote!(#name));
                        }
                        _ => panic!(),
                    }
                }
                _ => return None,
            }
        }
    }
    None
}

#[proc_macro_derive(Values, attributes(no_value))]
pub fn derive_values(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let vec = values(&input.data);
    //    println!("______________{:?}", &vec);

    let expanded = quote! {
        impl Values for #name {
            fn values(&self) -> Vec<Sqlite3Ref>
            {
                #vec
            }

        }
    };
    //        let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}


fn values(data: &Data) -> TokenStream {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                let recurse = fields
                    .named
                    .iter()
                    .filter(|f| !f.attrs.iter().any(|a| a.path.is_ident("no_value")))
                    .map(|f| {
                        let name = &f.ident;
                        quote_spanned! {f.span()=>
                            (&self.#name).into()
                        }
                    });

                quote! {
                    vec![#(#recurse, )*]
                }
            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}

fn gen_placeholders(n: usize) -> String {
    ["?"]
        .iter()
        .cycle()
        .take(n)
        .copied()
        .collect::<Vec<&str>>()
        .join(",")
}



#[proc_macro_derive(Insert, attributes(no_insert_column, table))]
pub fn derive_insert(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    //    println!("{:?}", input);
    let input: DeriveInput = parse(input).unwrap();

    let name = input.ident;
    let table = get_table_var(&input.attrs).expect("Not define attibute \"table\"");
    let col_vec = add_insert(&input.data);
    let columns = col_vec.join(", ");
    let stmt = format!("INSERT INTO {} ({}) VALUES ({})", table, columns,

        gen_placeholders(col_vec.len() ));
        let lit = LitStr::new( &stmt, Span::call_site() );
    //    println!("______________{:?}", &vec);

    let expanded = quote! {
        impl Insert for #name {
            fn stmt() -> &'static str
            {
                #lit
            }

        }
    };
    //        let expanded = quote!{};
    proc_macro::TokenStream::from(expanded)
}


fn add_insert(data: &Data) -> Vec<String> {
    match *data {
        Data::Struct(ref data) => match data.fields {
            Fields::Named(ref fields) => {
                 fields
                    .named
                    .iter()
                    .filter(|f| !f.attrs.iter().any(|a| a.path.is_ident("no_insert_column")))
                    .map(|f|
                        f.ident.as_ref().unwrap().to_string()

                    ).collect()


            }
            _ => panic!(),
        },
        Data::Enum(_) | Data::Union(_) => unimplemented!(),
    }
}
