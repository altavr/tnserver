use tn_proc::Stringinize;
//use tntransition::absmodel::{Stringinize, Stringinized};


pub trait Stringinize {
    type Stringinizer;
    fn stringinize(self, row: &mut Vec<String>, s: &Self::Stringinizer);
}


pub trait Stringinized {
    type Stringinizer;

    fn from_stringinized(self, r: &Self::Stringinizer) -> String;
}

pub trait StringinizedIter {
    type Stringinizer;
    fn from_stringinized<'a>(self, r: &'a Self::Stringinizer)
        -> Box<dyn Iterator<Item=String> + 'a >;
}

struct Stringinizer {
    a: String,
}
impl StringinizedIter for IT {
    type Stringinizer = Stringinizer;
    fn from_stringinized<'a>(self, r: &'a Self::Stringinizer)
        -> Box<dyn Iterator<Item=String> + 'a >
    {
      Box::new( self.0.into_iter().map( move |i| r.it(i)) )
    }
}

impl Stringinizer {
    fn ss(&self, t: SS) -> String {
         t.0.join("+ ss +")   }

    fn ss2(&self, t: SS2) -> String {
          t.0.join("+ ss2 +")
}
    fn it(&self, t: String) -> String {
        t + "+ it"
}}

struct IT (Vec<String>);


#[derive(Stringinize)]
#[stringinizer = "Stringinizer"]
struct AA {
    #[stringinize_ignore]
    id: i64,
    ff: SS,
    f2: SS2,
    #[stringinize_iter]
    it: IT
}

struct SS(Vec<String>);

impl Stringinized  for SS {
    type Stringinizer=Stringinizer;
    fn from_stringinized(self,  s: &Self::Stringinizer) -> String {
        s.ss(self)
}
}


struct SS2(Vec<String>);

impl Stringinized  for SS2 {
    type Stringinizer=Stringinizer;
    fn from_stringinized(self, s: &Self::Stringinizer) -> String {
        s.ss2(self)
}
}

//impl Stringinize  for AA {
//    type Stringinizer=Stringinizer;
//    fn stringinize(self, row: &mut Vec<String>, s: &Self::Stringinizer)  {
//      row.push(  self.ff.from_stringinized(s)  );
//       row.push(  self.f2.from_stringinized(s)  );
//       self.it.from_stringinized(s).for_each(|s| row.push( s) );
//}
//}

#[test]
fn derive_stringinazed_test() {

    let it = IT  ( vec!["1".to_string(), "2".to_string(), "3".to_string()]);
    let ss2 = SS2  ( vec!["ttt".to_string(), "uuu".to_string()]);
    let aa = AA {id : 1, ff: SS( vec!["one".to_string(), "two".to_string()] ), f2: ss2, it} ;

    let s  = Stringinizer{a: "Stringenazer".to_string() };
    let mut row = Vec::new();
    aa.stringinize(&mut row, &s);
    assert_eq!(row[0], "one+ ss +two" );
    assert_eq!(row[1],  "ttt+ ss2 +uuu");
    assert_eq!(row[2],  "1+ it");
    assert_eq!(row[3],  "2+ it");
    assert_eq!(row[4],  "3+ it");
    assert_eq!(row.len(),  5);
}
