use tn_proc::Insert;

pub(crate) trait Insert {
    fn stmt() -> &'static str;

}
#[derive(Insert)]
#[table="task"]
struct Task {
    #[no_insert_column]
    id: i64,
    owner_id: i64,
    performer_id: i64,
    status: i64,
}

#[test]
fn derive_insert_test() {
    assert_eq!(Task::stmt(),  "INSERT INTO task (owner_id, performer_id, status) VALUES (?,?,?)");

}
