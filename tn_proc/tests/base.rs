//use tn_proc::{Entity};

//static TT: &[u32] =  &[1,2];
//static TEST_TABLE: &&[u32] = &TT;



//#[derive(Debug)]
//pub enum Sqlite3Ref<'a> {
//    NULL,
//    INTEGER(i64),
//    DOUBLE(f64),
//    BLOB(&'a [u8]),
//    TEXT(&'a str),
//}

//macro_rules! impl_from_for_Sqlite3Ref {
//    ($t:ty, $var:ident) => {
//        impl<'a> From<$t> for Sqlite3Ref<'a> {
//            fn from(v: $t) -> Sqlite3Ref<'a> {
//                Sqlite3Ref::$var(v)
//            }
//        }
//    };
//}


//impl_from_for_Sqlite3Ref!(i64, INTEGER);
//impl_from_for_Sqlite3Ref!(f64, DOUBLE);
//impl_from_for_Sqlite3Ref!(&'a str, TEXT);
//impl_from_for_Sqlite3Ref!(&'a String, TEXT);
//impl_from_for_Sqlite3Ref!(&'a [u8], BLOB);

//macro_rules! impl_from_ref_for_Sqlite3Ref {
//    ($t:ty, $var:ident) => {
//        impl<'a> From<$t> for Sqlite3Ref<'a> {
//            fn from(v: $t) -> Sqlite3Ref<'a> {
//                Sqlite3Ref::$var(*v)
//            }
//        }
//    };
//}

//impl_from_ref_for_Sqlite3Ref!(&'a i64, INTEGER);
//impl_from_ref_for_Sqlite3Ref!(&'a f64, DOUBLE);

//impl<'a, T> From<&'a Option<T>> for Sqlite3Ref<'a>
//where
//    &'a T: Into<Sqlite3Ref<'a>>,
//{
//    fn from(v: &'a Option<T>) -> Sqlite3Ref<'a> {
//        match v {
//            Some(x) => x.into(),
//            None => Sqlite3Ref::NULL,
//        }
//    }
//}

//pub(crate) trait Query {
//    fn value(&self) -> i32 {42 }
//}

//impl Query for &'static [u32] {}

//pub(crate) trait Entity {
//    fn query(&self) -> &'static dyn Query;
//    fn values(&self) -> Vec<Sqlite3Ref>;
//    fn get_id(&self) -> i64;
//    fn len(&self) -> usize;
//}






//#[derive(Entity, Default)]
//#[table = "TEST_TABLE"]
//struct TaskData {
//    #[row_id]
//    id: i64,
//    modification_id: i64,
//    type_id: i64,
//    data_text: Option<String>,
//    data_int: Option<i64>,
//}


//#[test]
//fn derive_entity_test() {
//    let mut  td = TaskData::default();
//    td.id = 49;
//    assert_eq!(td.get_id(), 49);
//    assert_eq!(td.len(), 4);
//    assert_eq!(td.query().value(), 42 );
//}
