use tn_proc::GenRow;
//use tntransition;
//use tnsqlite3;

pub mod tnsqlite3 {

    #[derive(Debug)]
    pub enum Error {
        InnerError(InnerError),
    }

    #[derive(Debug)]
    pub struct InnerError {}

    impl InnerError {
        pub fn new(e: IRR) -> Self {
            InnerError {}
        }
    }

    #[derive(Debug)]
    pub enum IRR {
        MappingError(i32),
    }

    pub struct Row {
        count: i32,
    }

    impl Row {
      pub  fn new() -> Self {
            Row { count: -1 }
        }

        fn get<T>(&mut self, idx: i32) -> Result<Option<T>, Error>
        where
            T: From<String>
        {
            match idx {
                0 => Ok(Some(From::from("first string".to_string()))),
                1 => Ok(Some(From::from("second string".to_string()))),
                2 => Ok(Some(From::from("third string".to_string()))),
                _ => unreachable!(),
            }
        }

        fn get_inc<T>(&mut self) -> Result<Option<T>, Error>
        where
                T: From<String>
 {
            self.count += 1;
            self.get(self.count)
        }
    }

    pub trait GenRow<T> {
        fn gen_row(row: &mut Row) -> Result<Option<T>, Error>;
    }

    impl<T> Mapping<T> for T
    where
        T: GenRow<T>,
    {
        fn mapping(row: &mut Row) -> Result<T, Error> {
            Ok(T::gen_row(row)?.unwrap())
        }
    }

    pub trait Mapping<T> {
        fn mapping(row: &mut Row) -> Result<T, Error>;
    }

    macro_rules! impl_gen_row {
        ($t:ty) => {
            impl GenRow<$t> for $t {
                fn gen_row(row: &mut Row) -> Result<Option<$t>, Error> {
                    row.get_inc()
                }
            }
        };
    }

    impl_gen_row!(String);
}

use tnsqlite3::{GenRow, Mapping, Row};

#[derive(GenRow, Debug)]
struct AA {
    #[mapping_ignore]
    id: i64,
    a: String,
}

#[derive(GenRow, Debug)]
struct BB {
    a: AA,
    b: String,
    c: Option<String>,
}

#[test]
fn derive_mapping_test() {
    let mut row = Row::new();
    let a = AA::mapping(&mut row).unwrap();
    assert_eq!(a.a, "first string".to_string());


    let mut row = Row::new();
    let b = BB::mapping(&mut row).unwrap();
    assert_eq!(b.a.a, "first string".to_string());
    assert_eq!(b.b, "second string".to_string());
    assert_eq!(b.c, Some("third string".to_string()));
    println!("{:?}",b);
}
